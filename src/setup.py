# -*- coding: utf-8  -*-
import os
import logging, logging.config
import datetime as dt

if not os.path.exists('logs'):
    os.mkdir('logs')
logging.config
logging.basicConfig(filename='logs/test.log',
                    filemode='w',
                    level=logging.DEBUG,
                    # format = '[%(asctime)s]# %(levelname)-8s %(threadName)-13s'
                    # ' %(name)-40s %(message)-100s [%(funcName)s:LINE:%(lineno)d]',
                    format = '[%(asctime)s]# %(levelname)-8s '
                    ' %(name)-40s %(message)-100s [%(funcName)s:LINE:%(lineno)d]',
                    )
log = logging.getLogger(__name__) 

from setuptools import setup, find_packages
here = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(here, 'README.txt')) as f:
    README = f.read()
with open(os.path.join(here, 'CHANGES.txt')) as f:
    CHANGES = f.read()

requires = [
	'xlwt',
	'pygame',
	'dbfpy',
    ]
setup(name='asdm',
      version='0.0',
      description='asdm',
      long_description=README + '\n\n' + CHANGES,
      classifiers=[
        "Programming Language :: Python",
        ],
      author='',
      author_email='7676562@gmail.com',
      url='http://bysel.by',
      keywords='',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      test_suite='',
      install_requires=requires,
      entry_points="""\
      [paste.app_factory]
      main = main:main
      [console_scripts]
      """,
      )
