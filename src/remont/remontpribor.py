#!/usr/bin/python
# -*- coding: utf-8  -*-
from PyQt4 import QtGui, QtCore
from spr.control import controltable
from datetime import timedelta, datetime, date
from errorspr import errorspr


class priborspr(controltable):
    def __init__(self, parent = None, mysql = None):
        self.initSql()
        controltable.__init__(self, parent, mysql)
        self.dataview = ["nmbr", "count", "count2", "install"]
        self.headername = [u"Номер", u"Нормальных", u"Ошибок", u"Установлен"]

    def initSql(self):
        self.dataparam = ["nmbr", "count", "count2", "install"]
        self.getDataSQL1 = '''
            SELECT p.nmbr, b.garnom, b.gosnom
            FROM pribor as p, bus as b
            WHERE p.idbus = b.id AND b.del = 0
            GROUP BY nmbr'''

        self.getDataSQL2 = '''
            SELECT nmbr, (sum(npoint)+count(nmbr)) as count
            FROM point_%s
            WHERE datetimepribor > '%s' AND datetimepribor < '%s'
            GROUP BY nmbr'''

        self.getDataSQL3 = '''
            SELECT nmbr, (count(nmbr)) as count2
            FROM error_%s
            WHERE datetimeprogramm > '%s' AND datetimeprogramm < '%s'
            GROUP BY nmbr'''

    def _getData(self,data):#tupledata - содержит данные для вывода запроса
        #загрузить данные
        self.datarow = []
        try:

            self.mysql.execsql(self.getDataSQL1)
            result_set = self.mysql.cursor.fetchall()
            self.data = {} #словарь со всеми полями
            self.datarow = []
            for row in result_set:
                tmpdata = {}
                tmpdata["nmbr"] = row["nmbr"]
                tmpdata["count"] = 0
                tmpdata["count2"] = 0
                tmpdata["install"] = "%s [%s]"%(row["garnom"], row["gosnom"])
                self.data[row["nmbr"]] = tmpdata
                self.datarow += [row["nmbr"]]

            self.mysql.execsql(self.getDataSQL2%tuple(data))
            result_set = self.mysql.cursor.fetchall()
            for row in result_set:
                tmpdata = {}
                tmpdata["nmbr"] = row["nmbr"]
                tmpdata["count"] = row["count"]
                tmpdata["count2"] = 0
                try:
                    tmpdata["install"] = self.data[row["nmbr"]]["install"]
                except:
                    tmpdata["install"] = u"склад"
                    self.datarow += [row["nmbr"]]
                self.data[row["nmbr"]] = tmpdata


            self.mysql.execsql(self.getDataSQL3%tuple(data))
            result_set = self.mysql.cursor.fetchall()
            for row in result_set:
                tmpdata = {}
                tmpdata["nmbr"] = row["nmbr"]
                tmpdata["count2"] = row["count2"]
                fl = 0
                try:
                    tmpdata["count"] = self.data[row["nmbr"]]["count"]
                except:
                    tmpdata["count"] = 0
                    fl = 1
                try:
                    tmpdata["install"] = self.data[row["nmbr"]]["install"]
                except:
                    tmpdata["install"] = u"склад"
                    fl = 1
                if fl:
                    self.datarow += [row["nmbr"]]
                self.data[row["nmbr"]] = tmpdata

            self.datarow.sort()
        except Exception, e:
            print e

    def itemtable(self,row,col,s):
        item = QtGui.QTableWidgetItem("%s"%s)
        c =self.data[self.datarow[row]]["count"]
        c2 = self.data[self.datarow[row]]["count2"]
        if c == 0 and c2 ==0:
            item.setBackground(QtGui.QBrush(7,1))
        elif c == 0:
            item.setBackground(QtGui.QBrush(7,5))
        elif c2 > 0:
            item.setBackground(QtGui.QBrush(12,1))
        self.setItem(row,col,item)

class remontpribor(QtGui.QMdiSubWindow):
    def __init__(self, parent = None, mysql = None):
        QtGui.QMdiSubWindow.__init__(self, parent)
        self.setWindowTitle(u'Справочник приборов')
        self.table = priborspr(parent,mysql)
        self.tableerror = errorspr(parent,mysql)
        self.initWidget()

        self.table.redraw([str(self.date1.date().toPyDate()),
                   str(self.date1.date().toPyDate()+timedelta(1,0))])
        self.initAction()
        self.table.setCurrentCell(0,0)

    def initWidget(self):
        widg = QtGui.QWidget(self)
        grid = QtGui.QGridLayout()
        z = date.today()
        #z = date(2011, 9, 29)
        self.date1 = QtGui.QDateEdit(QtCore.QDate(z))
        self.date1.setCalendarPopup(True)
        self.date2 = QtGui.QDateEdit(QtCore.QDate(z+timedelta(1,0)))
        self.date2.setCalendarPopup(True)
        self.rereadbutton = QtGui.QPushButton(u"Обновить")
        hva1 = QtGui.QHBoxLayout()
        hva1 .addWidget(self.date1)
        hva1 .addWidget(self.date2)
        hva1 .addWidget(self.rereadbutton)
        hva1.addStretch(1)
        spl = QtGui.QSplitter()
        spl.addWidget(self.table)
        spl.addWidget(self.tableerror)
        grid.addLayout(hva1,0,0)
        grid.addWidget(spl)
        widg.setLayout(grid)
        self.setWidget(widg)


    def initAction(self):
        self.connect(self.date1, QtCore.SIGNAL("dateChanged(QDate)"),self.dateChanged1)
        self.connect(self.date2, QtCore.SIGNAL("dateChanged(QDate)"),self.dateChanged2)
        self.connect(self.table, QtCore.SIGNAL("itemSelectionChanged()"),self.tblselChanged)
        self.connect(self.rereadbutton, QtCore.SIGNAL("clicked()"),self.dateChanged2)

    def dateChanged1(self,d):
        self.date2.setDate(QtCore.QDate(self.date1.date().toPyDate()+timedelta(1,0)))

    def dateChanged2(self,d=''):
        self.table.redraw([str(self.date1.date().toPyDate()),
                   str(self.date2.date().toPyDate())])
        if self.table.rowCount () > 0:
            nmbr = self.table.datarow[self.table.currentRow ()]
            self.tableerror.redraw([nmbr,str(self.date1.date().toPyDate()),
                       str(self.date2.date().toPyDate())])
        else:
            nmbr = 0

    def tblselChanged(self):
        if self.table.rowCount() > 0:
            nmbr = self.table.datarow[self.table.currentRow ()]
            self.tableerror.redraw([nmbr,str(self.date1.date().toPyDate()),
                       str(self.date2.date().toPyDate())])
        else:
            nmbr = 0
