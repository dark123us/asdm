#!/usr/bin/python
# -*- coding: utf-8  -*-
from PyQt4 import QtGui, QtCore
from db import db
from spispribor import Ui_Dialog
import datetime
from datetime import timedelta
import time


# To change this template, choose Tools | Templates
# and open the template in the editor.

class spisPribor:
    def __init__(self,mysql):
        self.data = []
        self.mysql = mysql

    def getSpisPribor(self,date1,date2):
        sql = """ SELECT id, nmbr FROM pribor ORDER BY nmbr"""
        self.mysql.execsql(sql)
        result_set = self.mysql.cursor.fetchall()
        data = []
        dict = {}
        for row in result_set:
            data += [row["nmbr"]]
            dict[row["nmbr"]] = [row["id"],0,0]
        getdata = self.getSpisNormDataAndError(date1,date2)
        for i in getdata [0]:
            try:
                dict[i][1] = getdata[0][i]
            except KeyError:
                data += [i]
                dict[i] = [0,getdata [0][i],0]
        for i in getdata [1]:
            try:
                dict[i][2] += getdata [1][i]
            except KeyError:
                data += [i]
                dict[i] = [0,0,getdata [1][i]]
        return (data,dict)


    def getSpisNormDataAndError(self,date1,date2):
        date1 = date1.toPyDateTime()
        date2 = date2.toPyDateTime()
        #список нормальных данных и поломанных приборов
        sql = """ SELECT nmbr, (sum(npoint)+count(nmbr)) as count FROM point
            WHERE datetimepribor >= '%s' AND datetimepribor <= '%s'
            GROUP BY nmbr
            ORDER BY nmbr """%(str(date1),str(date2))
        self.mysql.execsql(sql)
        result_set = self.mysql.cursor.fetchall()
        data = {}
        for row in result_set:
            data[row["nmbr"]] = row["count"]
        sql = """ SELECT nmbr, count(nmbr) as count FROM error
            WHERE datetimepribor >= '%s' 
            and datetimepribor <='%s'
            GROUP BY nmbr ORDER BY nmbr """%(str(date1),str(date2))
        self.mysql.execsql(sql)
        result_set = self.mysql.cursor.fetchall()
        err = {}
        for row in result_set:
            err[row["nmbr"]] = row["count"]
        return (data,err)


class wSpisPribor(QtGui.QDialog, Ui_Dialog):
    def __init__(self, parent=None,mysql=None):
        super(wSpisPribor, self).__init__(parent)
        self.setupUi(self)
        #self.mydb = mysql
        self.dt_edit1.setDate(datetime.datetime.now()-timedelta(1))
        self.dt_edit2.setDate(datetime.datetime.now()+timedelta(1))
        self.spis = spisPribor(mysql)

    def show(self):
        self.lw_spis.clear()
        data, dict = self.spis.getSpisPribor(self.dt_edit1.dateTime(),self.dt_edit2.dateTime())
        for i in data:
            self.lw_spis.addItem("%d %d/%d"%(i,dict[i][1],dict[i][2]))
        super(wSpisPribor,self).show()
        