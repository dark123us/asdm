import asyncore, socket
import pickle
import time

class HTTPClient(asyncore.dispatcher):
    def __init__(self, host, port):
        asyncore.dispatcher.__init__(self)
        self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
        self.connect( (host, port) )
        a = pickle.load(open("packet","rb"))
        self.buffer = a
        self.t = time.time()

    def handle_connect(self):
        pass

    def handle_close(self):
        self.close()

    def handle_read(self):
        print self.recv(8192)

    def writable(self):
        if time.time() - self.t > 3:
            self.t = time.time()
            a = pickle.load(open("packet","rb"))
            self.buffer = a
        return (len(self.buffer) > 0)

    def handle_write(self):
        sent = self.send(self.buffer)
        print '%04i <--'%sent
        self.buffer = self.buffer[sent:]


for i in range(2):
    client = HTTPClient('', 21001)
asyncore.loop(1)