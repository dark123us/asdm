#!/usr/bin/python
# -*- coding: utf-8  -*-

from db import db

class database:
    '''Работаем с базой данных
    
    :param int countpoint: количество запросов собрать в буфере для отправки одним запросом в таблицу точек
    :param int counterror: количество запросов собрать в буфере для отправки одним запросом в таблицу ошибок
    :param str sqlpoint: формируемый запрос для добавления в таблицу точек
    :param str sqlerror: формируемый запрос для добавления в таблицу ошибок
    :param db: объект класса :py:class:`server.db.db` - обработка работы с базой
    
    '''    
    
    def __init__(self):
        self.sqlpoint = ""
        self.sqlerror = ""
        self.countpoint = 100 
        self.counterror = 100
        self.db = db()
        self.fields = []
        self.datarow = []
        self.data = {}
    
    def connect(self, config):
        self.db.config = config
        self.db.connect()
    
    def disconnect(self):
        self.db.disconnect()
    
    def execsql(self, sql):
        self.db.execsql(sql)

    def getData(self, sql):
        self.fields, self.datarow, self.data = self.db.getData(sql)
        return (self.fields, self.datarow, self.data)
        
    def getLastid(self):
        id = 0
        id = self.db.getlastid()
        return id