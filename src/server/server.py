#!/usr/bin/python
# -*- coding: utf-8  -*-
from db import db
from buffer import buffer

from resender import resender
from redirect import redirect
from listen import listen
from command import command

import time
from ConfigParser import ConfigParser

import asyncore

class mainServer():
    '''Переписанный сервер (2-я редакция) для регистрации координат. Расширены возможности
    по буфферизации пакетов, уменьшено количество потоков (как самой программы,
    так и подключений к базе), и другие возможности.
    '''
    def __init__(self):
        self.config = {}#конфигурация сервера
        self.readConfig()
        self.initBuffer()#для буферизации данных, чтобы было меньше запросов к базе
        self.initCommand()#слушаем приходящие команды
        self.initListen()#слушаем данные от приборов, создаются по классу на каждый прибор
        self.initResender()#пересылаем данные немым серверам и серверам с запросом информации
        
    def readConfig(self):
        try:
            cp = ConfigParser()
            cp.read(['default.cfg'])
            self.config['nameserver'] = {}
            self.config['nameserver'] = cp.get('main', 'nameserver')
            
            self.config['command'] = {}
            self.config['command']['ip'] = cp.get('command', 'ip')
            self.config['command']['port'] = int(cp.get('command', 'port'))
            self.config['command']['backlog'] = int(cp.get('command', 'backlog'))
            
            self.config["db"] = {}
            self.config["db"]["host"] = cp.get('database', 'host')
            self.config["db"]["user"] = cp.get('database', 'user')
            self.config["db"]["passwd"] = cp.get('database', 'password')
            self.config["db"]["db"] = cp.get('database', 'database')

            self.config["buffer"] = {}
            self.config["buffer"]["maxsql"] = int(cp.get('buffer', 'maxsql'))#сколько пакетов буферизовать, прежде чем сохранить в базе
            
            self.config["listen"] = []
            ls = cp.items('pribor')
            k = 0
            for i in ls:
                section = i[1]
                self.config["listen"] += [{}]
                self.config["listen"][k]['ip'] = cp.get(section, 'ip')
                self.config["listen"][k]['port'] = int(cp.get(section, 'port'))
                self.config["listen"][k]['backlog'] = int(cp.get(section, 'backlog'))
                self.config["listen"][k]['typepribor'] = int(cp.get(section, 'typepribor'))
                self.config["listen"][k]['redirect'] = []
                redirect = cp.get(section, 'redirect')
                if  redirect != '':
                    self.config["listen"][k]['redirect'] += [{}]
                    l = 0
                    ls2 = cp.items(redirect)
                    for j in ls2:
                        section1 = j[1]
                        self.config["listen"][k]['redirect'][l]["ip"] = cp.get(section1, 'ip')
                        self.config["listen"][k]['redirect'][l]["port"] = cp.get(section1, 'port')
                        l += 1
                        
                
                k += 1
            print self.config
        except:
            print "Error in config fils"
            raise

    def initBuffer(self):
        self.mysql = db()
        self.mysql.config = self.config["db"] 
        self.mysql.connect()
        self.buffer = buffer()
        self.buffer.config = self.config["buffer"]
        self.buffer.mysql = self.mysql    
    
    def initCommand(self):
        self.command = command()
        self.command.resender.mysql = self.mysql
        self.command.resender.bufferdirect = self.buffer.buffredirect
        self.command.config = self.config["command"]
        comm = {}
        comm["stop"] = self.stopServer
        comm["getconn"] = self.getConnection
        self.command.command = comm
        self.command.initListenPort()
    

    def initResender(self):
        self.resender = resender()
        
    def initRedirect(self):
        self.redirect = redirect()
    
    def initListen(self):
        self.listen = []
        k = 0
        for i in self.config["listen"]:
            self.listen += [listen()]
            self.listen[k].config = i
            self.listen[k].addbuffer = self.buffer.addbuffer
            self.listen[k].initListenPort()
            k += 1
    
    def stopServer(self):
        'Остановка сервера'
        #останавливаем командную часть сервера
        self.command.close()
        #останавливаем слушающую часть сервера
        for i in self.listen:
            i.close()
        #закрываем буфер, с сохранением данных
        self.buffer.close()
        self.mysql.disconnect()
        print "stopping"
    
    def getConnection(self,id1):
        'Выдать данные по подключениям'
        str = "get connection"
        k = 0
        allconn = 0
        for i in self.listen:
            str += "\nListen server %d"%k
            for j in i.clientdict:
                str = "%s\n%d 0x%08x last data time:[%8.2f] pribor %d"%(str, j, 
                    id(i.clientdict[j]), time.time()-i.clientdict[j].timer, i.clientdict[j].npribor)
                allconn += 1
            k += 1
        str += "\n\n connection = %d"%allconn
        str += "\n"
        print str
        
        self.command.clientdict[id1].buffer_write = str
        
        
if __name__ == "__main__":
    m = mainServer()
    asyncore.loop(5)
    print "Server stoping"
    
    
    
    