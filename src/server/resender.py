#!/usr/bin/python
# -*- coding: utf-8  -*-
import socket, asyncore
import time
from db import db
#import threading
from decode import decode

class threadRedirect(asyncore.dispatcher):
    '''Класс потока для отправки пакетов
    
    :param socket conn: октрытый сокет сервера передачи
    :param int id: id потока
    :param function delclient: ссылка на функцию для сообщении слушающему серверу о закрытии потока
    :param str ip: адрес с которым работаем
    :param int port: порт куда идут данные
    '''
    def __init__(self, id, delclient, ip, port):
        self.buffer_write = ''        
        asyncore.dispatcher.__init__(self)
        self.create_socket(socket.AF_INET,socket.SOCK_STREAM)
        self.connect( (ip, port))
        self.delclient = delclient
        self.id = id

    def handle_read(self):
        '''Метод для чтения пакетов '''
        self.recv(1024)

    def writable(self):
        '''Проверяем необходимость передачи данных '''
        return (len(self.buffer_write) > 0)

    def handle_write(self):
        '''Передаем данные'''
        sent = self.send(self.buffer_write)
        print '%04i <-- redirect '%sent, self.id
        self.buffer_write = self.buffer_write[sent:]

    def handle_close(self):
        '''При закрытии потока внешним потоком сообщаем слушающему серверу о данном событии'''
        self.delclient(self.id)
        self.close()
        
    def handle_error(self):
        '''При возникновении ошибки сообщаем слушающему серверу о закрытии потока'''
        print "error connect redirect"
        self.delclient(self.id)
        self.close()

class threadResend(asyncore.dispatcher):
    '''Класс потока для отправки пакетов
    
    :param socket conn: октрытый сокет сервера передачи
    :param int id: id потока
    :param function delclient: ссылка на функцию для сообщении слушающему серверу о закрытии потока
    :param str ip: адрес с которым работаем
    :param int port: порт куда идут данные
    '''
    def __init__(self, id, delclient, ip, port):
        self.buffer_write = ''        
        asyncore.dispatcher.__init__(self)
        self.create_socket(socket.AF_INET,socket.SOCK_STREAM)
        self.connect( (ip, port))
        self.delclient = delclient
        self.id = id
        self.ip = ip
        self.port = port
        self.lid = 0#по этому id будут удалены строки
        self.pid = 0#id родителя

    def handle_read(self):
        '''Метод для чтения пакетов '''
        #хоть сервер и не присылает данных, но лучше их все же принять
        self.recv(1024)

    def writable(self):
        '''Проверяем необходимость передачи данных '''
        return (len(self.buffer_write) > 0)

    def handle_write(self):
        '''Передаем данные'''
        sent = self.send(self.buffer_write)
        print '%04i <-- resend '%sent, self.id
        self.buffer_write = self.buffer_write[sent:]

    def handle_close(self):
        '''При закрытии потока внешним потоком сообщаем слушающему серверу о данном событии'''
        self.delclient(self.id)
        self.close()
        
    def handle_error(self):
        '''При возникновении ошибки сообщаем слушающему серверу о закрытии потока'''
        print "error connect redirect"
        self.delclient(self.id)
        self.close()
        

class resender:#(threading.Thread):
    '''Сервер для отправки не доставленных данных серверам из буфера и базы данных
    Требуется периодически по данным из буфера и таблицы с транзакциями пробовать
    достукиваться до серверов и при наличии успешного соединения начинать передачу
    освобождая буферы и базы данных
    '''
    def __init__(self):
        self.initvar()
    
    def initvar(self):
        '''Инициализация переменных
        '''
        self.clientdict = {} #ключ - id, параметр - ссылка на поток
        self.lastid = 0#для определения, какой id закрылся от ошибки
        self.time = time.time()#таймер для отсчета при попытках подключения
        self.dtime = 10 #через сколько секунд повторить попытку подключения к серверу
        self.rereadtime = time.time()#таймер для отсчета при попытках подключения
        self.drereadtime = 0.1#через сколько секунд слать следующую порцию
        
        self.mysql = db() #ссылка на db модуль, для работы с базой данных
        self.listconnect = [] #список для попытки подключения
        self.bufferdirect = {}#ссылка на буфер ретрансляции из класса буфера
        self.maxbuffer = 20 #сколько строк подготовить к отправке по максимуму
        self.decode = decode()
        
        self.mainserver = {}#путь к главному серверу
        self.mainserver['ip'] = '10.1.12.52'
        self.mainserver['port'] = 21000
        
    def addthread(self, ip, port):
        '''Создаем поток для переадресации
        '''
        try:
            uid = self.lastid 
            self.clientdict[uid] = None
            self.clientdict[uid] = threadResend(uid, self.delthread, ip, port)
            d, lid, pid = self.getDataBuff(ip, port)
            self.clientdict[uid].lid = lid#до какого id удалять из базы
            self.clientdict[uid].pid = pid#id родителя в базе
            self.clientdict[uid].buffer_write = d
            self.listconnect += ["%s:%d"%(ip, port)]
            self.lastid += 1
        except Exception, e:
            print e
            raise
        
    def delthread(self, uid):
        '''Метод обработки закрытия потока'''
        print '--- Disconnect resend --- ',uid
        #self.clientdict[id] = None
        #если в клиенте буфер не опустошилса - возвращаем назад в буфер
        cld = self.clientdict[uid]
        ipport = "%s:%d"%(cld.ip, cld.port)
        try:
            for i in self.bufferdirect[ipport]:#если какие данные нашли, то ставим в буфере 0 на 3 колонке
                i[2] = 0
        except KeyError:
            #print "key not found %s"%ipport
            pass
        self.listconnect.remove(ipport)#удаляем из списка подключений
        self.clientdict.pop(uid)
        
    
    def close(self):
        'Закрываем все потоки'
        for j in self.clientdict.values():
            if j != None:
                j.close()
        print "close resend"
        
    def getListAddr(self):
        '''Получаем список из буфера и базы данных адресов куда слать данные
        '''
        tmplist = []
        for i in self.bufferdirect:
            nofind = True
            for j in self.listconnect:
                if i == j:
                    nofind = False
                    break
            if nofind:
                tmplist += [i]
        #а также список из базы данных
        tablep = 'buffredirectserver'
        tabled = 'buffredirectdata'
        sql = '''SELECT t1.id, t1.ipport 
                 FROM %s as t1, %s as t2 
                 WHERE t2.idserver = t1.id
                 GROUP BY t1.id'''%(tablep, tabled)
        try:
            f, dr, d = self.mysql.getData(sql)
            tmplist2 = tmplist + self.listconnect #объединенный список уже найденных значений
            for i in dr:
                nofind = True
                for j in tmplist2:
                    if d[i]["ipport"] == j:
                        nofind = False
                        break
                if nofind:
                    tmplist += [d[i]["ipport"]]
        except:
            pass
        return tmplist

    def getDataBuff(self, ip, port):
        '''Перебираем данные в буфере (если есть че, забираем сюда, если не 
        сможем отдать вернем назад) и базе данных
        '''
        ipport =  "%s:%d"%(ip, port)
        tmp = ''
        lid = 0
        pid = 0
        
        k = 0 #счетчик полученных строк
        #получаем список (определенное количество строк) из буфера
        try:
            
            for i in self.bufferdirect[ipport]:
                #если какие данные нашли, то ставим в буфере 1 на 3 колонке - для пометки, что позже удалим
                i[2] = 1
                tmp += i[0]
                k += 1
                if k > self.maxbuffer: break
        except KeyError:
            #print "key %s not found"%ipport
            pass
        #если строки не все еще собрали - обращаемся к базе данных
        if k <= self.maxbuffer: 
            table1 = 'buffredirectdata'
            table2 = 'buffredirectserver'
            sql = '''SELECT  t1.id, t1.buff, t2.id as t2id 
                     FROM %s as t1, %s as t2 
                     WHERE t1.idserver = t2.id AND t2.ipport = "%s"
                     LIMIT 0, %d'''%(table1, table2, ipport, self.maxbuffer - k)
            f,dr,d = self.mysql.getData(sql)
            for i in dr:
                buff = self.decode.sord(d[i]["buff"])#переводим из байт строки
                tmp = "%s%s"%(tmp, buff)
                lid = d[i]["id"]
                pid = d[i]["t2id"]#по каким id ищем
        return tmp, lid, pid
        
    def clearDataBuff(self,ip, port, lid, pid):
        '''Очищаем буфер после отправки данных'''
        ipport =  "%s:%d"%(ip, port)
        tmp = []
        #получаем список (определенное количество строк) из буфера
        try:
            for i in self.bufferdirect[ipport]:
                #если какие данные нашли, то ставим в буфере 1 на 3 колонке - для пометки, что позже удалим
                if i[2] == 0:
                    tmp += [i]
            self.bufferdirect[ipport] = tmp
        except KeyError:
            pass
        #если строки не все еще собрали - обращаемся к базе данных
        if lid > 0:
            table1 = 'buffredirectdata'
            sql = '''DELETE FROM %s WHERE id <= %d AND idserver = %d'''%(table1, lid, pid)
            self.mysql.execsql(sql)
    
    def run(self):
        '''Основной метод - работа с таймером, проверка буферов потоков и др. '''
        #проверяем открытые потоки на случай опустошения буфера
        t = time.time()
        if t - self.rereadtime > self.drereadtime:
            self.rereadtime = t
            for cid in self.clientdict:
                i = self.clientdict[cid]
                if len(i.buffer_write) == 0:
                    #очищаем буферы
                    self.clearDataBuff(i.ip, i.port, i.lid , i.pid)
                    #получаем новые данные
                    d, lid, pid = self.getDataBuff(i.ip, i.port)
                    i.lid = lid#до какого id удалять из базы
                    i.pid = pid#id родителя в базе
                    i.buffer_write = d
                
        #по прошествии времени пытамся открыть сервера ранее не доступные для передачи данных
        if t - self.time > self.dtime:
            self.time = t
            tmp = self.getListAddr()
            for i in tmp:
                #print i.split(":")[0], i.split(":")[1]
                self.addthread(i.split(":")[0], int(i.split(":")[1]))

    def addPacket(self):
        pass
