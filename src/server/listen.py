#!/usr/bin/python
# -*- coding: utf-8  -*-
import socket, asyncore
from decode import decode
import time

class threadListen(asyncore.dispatcher):
    '''Класс потока для слушающего сервера - получаем данные от приборов
    
    :param socket conn: октрытый сокет из слушаещего сервера
    :param addr: адрес на котором открыт слушающий поток
    :param int id: id потока
    :param function delclient: ссылка на функцию для сообщении слушающему серверу о необходимости закрытии потока    
    :param function addbuffer: ссылка на функцию класса буфер для добавления полученного расшифрованного пакета
    :param redirect: список ссылок на классы отправки пакетов
    '''
    def __init__(self, conn):#
        asyncore.dispatcher.__init__(self, conn)
        self.timer = time.time()
        self.buffer_read = ''
        self.npribor = 0 #номер прибора при расшифровке пакетов - получаем номер

        self.address = ''
        self.id = 0
        self.delclient = None
        self.addbuffer = None
        self.resender = []
        self.typepribor = 0
        self.decode = decode()        
        
    def handle_read(self):
        '''Метод для чтения пакетов '''
        #TODO сделать добавление пакета, на случай принятия неполных пакетов
        self.timer = time.time()
        read = self.recv(1024)
        if len(read) > 0:
            print '%04i -->'%len(read), self.addres
            #пересылка пакетов или их складирование, если нет ответа
            for i in self.redirect:#
                i.addPacket(read)
            #расшифровка пакета
            self.buffer_read += read
            k = self.decode.decode(self.buffer_read)
            try:
                self.npribor = k[0][0]["nprib"]
            except:
                self.npribor = -1
            if self.decode.lenost > 0:
                self.buffer_read = self.buffer_read [-self.decode.lenost:]
            else:
                self.buffer_read = ''
            #расшифрованные данные отправляем в буфер            
            self.addbuffer(k)

    def handle_close(self):
        '''При закрытии потока внешним потоком сообщаем слушающему серверу о данном событии'''
        print "close listen thread ", self.id
        self.delclient(self.id)
        self.close()
        
    def writable(self):
        'Передавать нечего - поэтому всегда ложь'
        if time.time() - self.timer > 120:
            return True#Таймер перевалил за 2мин - посылаем истину
        else:
            return False
    
    def handle_write(self):
        #раз сработал метод - значит отрботал таймер более 2мин, следовательно закрываем поток
        self.handle_close()

class listen(asyncore.dispatcher):
    '''Слушающий сервер
    
    Предназначен для слушания эфира по нескольким портам, дабы услышать пакеты 
    от приборов и в зависимости от конфигурации - ретранслировать на 
    следующий сервер, передать в обработку пакеты.
    

    config["redirect"] = []
    config["ip"] = ''
    config["port"] = 21001
    config["backlog"] = 200
    
    addbuffer - ссылка на метод класса буфер, для добавления данных в буфер полученных расшифрованных данных
    
    Инициализация по умолчанию
        
        :param dict config: словарь для настроек класса:
            
            
            * "redirect" - список словарей для настройки серверов по пересылке пакетов, элемент списка имеет следующие поля:
                * "ip" - ip куда слать данные
                * "port" - порт на который высылать данные
            * "ip" - на каком ip слушаем
            * "port"  - на каком порту слушаем
            * "backlog" - количество соединений ожидаем на порту
        :param dict clientdict: словарь открытых потоков класса :py:class:`server.listen.threadListenServer`, где индекс - id процесса
        :param list redirect: список открытых серверов для ретрансляции пакетов :py:class:`server.redirect.redirectServer`
        :param buffer: ссылка на класс :py:class:`server.buffer.buffer` для работы раскодированными пакетами - используем только одну комманду addbuffer при добавлении данных
        clientid - счетчик id потоков для прослушивания
        
    '''
    def __init__(self):
        asyncore.dispatcher.__init__(self)
        self.config = {}
        self.redirect = [] 
        self.clientid = 0
        self.clientdict = {}
        self.addbuffer = None

    def initListenPort(self):
        '''По данным из конфига начинаем прослушивать порт
        '''
        if self.config != {}:
            self.create_socket(socket.AF_INET,socket.SOCK_STREAM)
            self.set_reuse_addr()
            self.bind((self.config["ip"], self.config["port"]))
            self.listen(self.config["backlog"])
            print "listen ip:'",self.config["ip"],"' port:", self.config["port"], " count:",self.config["backlog"]
        else:
            print u"Конфиг пустой"

    def handle_accept(self):
        '''Получаем соединение, включаем поток, добавляем соединение 
        на ретрансляторах'''
        self.addthread()
        
    def addthread(self):
        '''Создаем потоки для прослушивания и потоки (по числу серверов для
        ретрансляции) для передачи пакетов'''
        #создаем поток для прослушивания
        conn, addr = self.accept()
        print '--- Connect --- ',self.clientid
        cl = threadListen(conn)
        cl.address = addr
        cl.id = self.clientid
        cl.delclient = self.delthread
        cl.addbuffer = self.addbuffer
        cl.resender = self.resender
        cl.decode.type = self.config["typepribor"]
        self.clientdict[self.clientid] = cl
        self.clientid += 1

    def delthread(self, id):
        '''Метод обработки закрытия потока'''
        print '--- Disconnect listen --- ',id, self.config["port"]
        self.clientdict.pop(id)

    def close(self):
        '''Закрываем ретрансляторы'''
        for j in self.clientdict.values():
            j.close()
#        for i in self.resender:
#            i.close()
        print "Close listen server"
        asyncore.dispatcher.close(self)