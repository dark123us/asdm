#!/usr/bin/python
# -*- coding: utf-8  -*-
import socket, asyncore

class threadRedirect(asyncore.dispatcher):
    '''Класс потока для отправки пакетов
    
    :param socket conn: октрытый сокет сервера передачи
    :param int id: id потока
    :param function delclient: ссылка на функцию для сообщении слушающему серверу о закрытии потока
    :param str ip: адрес с которым работаем
    :param int port: порт куда идут данные
    '''
    def __init__(self, id, delclient, ip, port):
        self.buffer_write = ''        
        asyncore.dispatcher.__init__(self)
        self.create_socket(socket.AF_INET,socket.SOCK_STREAM)
        self.connect( (ip, port))
        self.delclient = delclient
        self.id = id

    def handle_read(self):
        '''Метод для чтения пакетов '''
        self.recv(1024)

    def writable(self):
        '''Проверяем необходимость передачи данных '''
        return (len(self.buffer_write) > 0)

    def handle_write(self):
        '''Передаем данные'''
        sent = self.send(self.buffer_write)
        print '%04i <-- redirect '%sent, self.id
        self.buffer_write = self.buffer_write[sent:]

    def handle_close(self):
        '''При закрытии потока внешним потоком сообщаем слушающему серверу о данном событии'''
        self.delclient(self.id)
        self.close()
        
    def handle_error(self):
        '''При возникновении ошибки сообщаем слушающему серверу о закрытии потока'''
        print "error connect redirect"
        self.delclient(self.id)
        self.close()
        

class redirect:
    '''Сервер для дальнейшей переадресации данных, подключенным серверам (передает 
    получаемые к указанным в конфиге серврам). При этом если сервер не будет доступен
    для приема данных, данные теряются. 
    '''
    def __init__(self):
        self.config = {} #файл конфигурации
        self.clientdict = {} #ключ - id, параметр - ссылка на поток
        self.lastid = 0
    
    def initvar(self):
        '''Инициализация переменных
        
        :param dict config: словарь всех настроек класса:
            
            * "ip" - ip куда посылать данные
            * "port" - порт куда посылать данные
        '''
        
    def addthread(self, id):
        '''Создаем поток для переадресации
        '''
        try:
            self.lastid = id
            self.clientdict[id] = None
            self.clientdict[id] = threadRedirect(id, self.delthread, 
                self.config["ip"], self.config["port"])
        except Exception, e:
            print e
        
    def delthread(self, id):
        '''Метод обработки закрытия потока'''
        print '--- Disconnect redirect--- ',id
        #self.clientdict[id] = None
        self.clientdict.pop(id)

    
    def close(self):
        'Закрываем все потоки'
        for j in self.clientdict.values():
            if j != None:
                j.close()
        print "close redirect"
        
        
    