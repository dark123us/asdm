#!/usr/bin/python
# -*- coding: utf-8  -*-
from PyQt4 import QtGui, QtCore
from grafik.brigada.brigada import comboBrigada
from grafik.bus.bus import comboBus
from datetime import date

class dialogSotrudn(QtGui.QDialog):#вспомогательное диалоговое окно
    def __init__(self, mysql, parent = None):
        QtGui.QDialog.__init__(self, parent)
        self.setWindowTitle(u'Добавить работника')
        self._brigada = comboBrigada(mysql, parent)
        self._bus = comboBus(mysql, parent)
        self.initWidget()

    def initWidget(self):
        grid = QtGui.QGridLayout(self)
        btn1 = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok|
                                      QtGui.QDialogButtonBox.Cancel)
        self.connect(btn1 , QtCore.SIGNAL("accepted()"), self.accept)
        self.connect(btn1 , QtCore.SIGNAL("rejected()"), self.reject)
        
        lbl1 = QtGui.QLabel(u'Фамилия')
        lbl2 = QtGui.QLabel(u'Имя')
        lbl3 = QtGui.QLabel(u'Отчество')
        lbl4 = QtGui.QLabel(u'Табельный номер')
        lbl5 = QtGui.QLabel(u'Бригада')
        lbl6 = QtGui.QLabel(u'Гаражный номер')
        lbl7 = QtGui.QLabel(u'Принят на работу')
        lbl8 = QtGui.QLabel(u'Уволен')
        
        self._ed1 = QtGui.QLineEdit()
        self._ed2 = QtGui.QLineEdit()
        self._ed3 = QtGui.QLineEdit()
        self._ed4 = QtGui.QLineEdit()
        self._brigada.redraw()
        self._bus.redraw()
        self._ded1 = QtGui.QDateEdit()
        self._ded1.setCalendarPopup(True)
        self._ded2 = QtGui.QDateEdit()
        self._ded2.setCalendarPopup(True)

        grid.addWidget(lbl1,0,0)
        grid.addWidget(lbl2,1,0)
        grid.addWidget(lbl3,2,0)
        grid.addWidget(lbl4,3,0)
        grid.addWidget(lbl5,4,0)
        grid.addWidget(lbl6,5,0)
        grid.addWidget(lbl7,6,0)
        grid.addWidget(lbl8,7,0)
        
        grid.addWidget(self._ed1,0,1)
        grid.addWidget(self._ed2,1,1)
        grid.addWidget(self._ed3,2,1)
        grid.addWidget(self._ed4,3,1)
        grid.addWidget(self._brigada,4,1)
        grid.addWidget(self._bus,5,1)
        grid.addWidget(self._ded1,6,1)
        grid.addWidget(self._ded2,7,1)
        
        grid.addWidget(btn1,8,0,1,2)
        
    def setAddMode(self):
        self.setWindowTitle(u'Добавить данные')
        self._brigada.redraw()
        self._bus.redraw()

        self._ed1.setText('')
        self._ed2.setText('')
        self._ed3.setText('')
        self._ed4.setText('0')
        self._ded1.setDate(QtCore.QDate(date(2001,1,1)))
        self._ded2.setDate(QtCore.QDate(date(2001,1,1)))
        
        self.setModal(1)
        self._mode = 0
        self.show()
    
    def setEditMode(self,f, i, o, tabelnmbr, idbrigada, idbus, prinyat, uvolen):
        self.setWindowTitle(u'Изменить данные')
        self._brigada.redraw()
        self._brigada.setSelect(idbrigada)
        self._bus.redraw()
        self._bus.setSelect(idbus)

        self._ed1.setText(f)
        self._ed2.setText(i)
        self._ed3.setText(o)
        self._ed4.setText('%d'%tabelnmbr)
        
        self._ded1.setDate(QtCore.QDate(prinyat))
        self._ded2.setDate(QtCore.QDate(uvolen))
        
        self.setModal(1)
        self.show()
        self._mode = 1
    
    def getResult(self):
        idbrigada = self._brigada.getSelect()
        idbus = self._bus.getSelect()
        f = self._ed1.text()
        i = self._ed2.text()
        o = self._ed3.text()
        tabelnmbr = int(self._ed4.text())
        prinyat = self._ded1.date().toPyDate()
        uvolen = self._ded2.date().toPyDate()
        
        return (f, i, o, tabelnmbr, idbrigada, idbus, prinyat, uvolen, self._mode)