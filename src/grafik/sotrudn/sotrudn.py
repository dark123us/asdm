#!/usr/bin/python
# -*- coding: utf-8  -*-
from PyQt4 import QtGui, QtCore
from datetime import date
from dialogsotrudn import dialogSotrudn
import calendar
from grafik.standart.getdata import getData

class getVodBusBrigada(getData):
    '''Для вертикального заголовка список водителей с его автобусами, по бригаде'''
    def __init__(self, mysql):
        getData.__init__(self, mysql)
    
    def getDataSql(self, param):
        idbrigada = param["idbrigada"]
        datebegin = param["datebegin"]
        dateend = param["dateend"]
        
        sql = '''
            SELECT v.id, v.family, v.name, v.surname, v.tabelnmbr, 0 as garnom, 0 as garnomfull,
                0 as idbus
            FROM voditel as v
            WHERE v.idbus = 0 AND v.brigada = %d AND 
                ((v.date_begin <='%s' AND v.date_end >='%s') OR v.date_end = '2001-01-01')
                
            UNION 
            SELECT v.id, v.family, v.name, v.surname, v.tabelnmbr, b.garnom, b.garnomfull,
                v.idbus
            FROM voditel as v, bus as b
            WHERE v.idbus = b.id AND b.del = 0 AND v.brigada = %d AND 
                ((v.date_begin <='%s' AND v.date_end >= '%s') OR v.date_end = '2001-01-01')
            ORDER BY 6,2,3,4'''%(idbrigada, dateend , datebegin, idbrigada, dateend , datebegin)
        return sql
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++        
class getSotrudn:
    def __init__(self, mysql):
        self._mysql = mysql
        self.initSql()
        
    def initSql(self):
        ''' Список всех сотрудников, с бригадой, '''
        self._getDataSql = '''
            SELECT v.id, v.family, v.name, v.surname, v.tabelnmbr, br.name as brigada, 0 as garnom, 0 as garnomfull,
                0 as idbus, date_begin, date_end, br.id as idbrigada
            FROM voditel as v, brigada as br
            WHERE v.idbus = 0 AND v.brigada = br.id
            UNION 
            SELECT v.id, v.family, v.name, v.surname, v.tabelnmbr, br.name as brigada, b.garnom, b.garnomfull,
                v.idbus, date_begin, date_end, br.id as idbrigada
            FROM voditel as v, bus as b, brigada as br
            WHERE (v.idbus = b.id) AND b.del = 0 AND v.brigada = br.id
            ORDER BY 2'''
    
    def getData(self):
        (f,dr,d) = self._mysql.getData(self._getDataSql)
        return (f,dr,d)
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++        
class getSotrudnWorking:
    def __init__(self, mysql):
        self._mysql = mysql
        self.initSql()
    
    def initSql(self):
        self._getDataSql = '''
            SELECT v.id, v.family, v.name, v.surname, v.tabelnmbr, br.name as brigada, 0 as garnom, 0 as garnomfull,
                0 as idbus, date_begin, date_end, br.id as idbrigada  
            FROM voditel as v, brigada as br
            WHERE v.idbus = 0 AND v.brigada = br.id AND br.id = %d AND
                (not (date_begin >= '%s' or date_end <= '%s') or date_end = '2001-01-01') 
            UNION 
            SELECT v.id, v.family, v.name, v.surname, v.tabelnmbr, br.name as brigada, b.garnom, b.garnomfull,
                v.idbus, date_begin, date_end, br.id as idbrigada  
            FROM voditel as v, bus as b, brigada as br
            WHERE (v.idbus = b.id) AND b.del = 0 AND v.brigada = br.id AND br.id = %d AND
                (NOT (date_begin >= '%s' or date_end <= '%s') or date_end = '2001-01-01') 
            ORDER BY 7,2,3,4'''
    
    def getData(self, bday, eday, idbrigada):
        '''bday - первый день месяца в графике, который рассматриваем, чтобы выбрать только тех людей, которые
        могли работать в этом месяце
        eday - последний день
        '''
        (f,dr,d) = self._mysql.getData(self._getDataSql%(idbrigada, str(eday), str(bday), 
                                                         idbrigada, str(eday), str(bday)))
        return (f,dr,d)
    
    def getDataMonth(self, datewithmonth, idbrigada):
        bday = date(datewithmonth.year,datewithmonth.month,1)
        
        eday = date(datewithmonth.year,
                    datewithmonth.month,
                    calendar.monthrange(datewithmonth.year, datewithmonth.month)[1])
        f,dr,d = self.getData(bday, eday, idbrigada)
        return (f,dr,d)
        
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class editSotrudn(getSotrudn):
    def __init__(self, mysql):
        getSotrudn.__init__(self, mysql)
    
    def initSql(self):
        getSotrudn.initSql(self)
        self._addDataSql = '''INSERT  INTO voditel (family, name, surname, 
                                    tabelnmbr, brigada, idbus, date_begin, date_end)
                                      VALUES ('%s', '%s', '%s', %d, %d, %d, '%s', '%s')'''
                                      
        self._editDataSql = '''UPDATE voditel SET family = '%s', name = '%s', surname = '%s', 
                                tabelnmbr = %d, brigada = %d, idbus = %d, 
                                date_begin = '%s', date_end = '%s'
                               WHERE id = %d'''
    def addData(self, family, name, surname, tabelnmbr, brigada, idbus, date_begin, date_end):
        self._mysql.execsql(self._addDataSql%(family, name, surname, tabelnmbr, brigada, idbus, date_begin, date_end))
    
    def editData(self, family, name, surname, tabelnmbr, brigada, idbus, date_begin, date_end, id):
        self._mysql.execsql(self._editDataSql%(family, name, surname, tabelnmbr, brigada, idbus, date_begin, date_end, id))
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class tableSotrudn(QtGui.QTableWidget):
    def __init__(self, mysql, parent = None):
        QtGui.QTableWidget.__init__(self,parent)
        self.setEditTriggers(QtGui.QAbstractItemView.EditTriggers(0))
        self._sotrudn = editSotrudn(mysql)
        self._headername = [u'ФИО', u'Табельный', u'Бригада', u'Гаражный', u'Принят - Уволен'] #поля, которые будем отображать
    
    def _initTable(self):
        self.setRowCount(0)
        self.setRowCount(len(self._datarow))
        self.setColumnCount(len(self._headername))
        self.setHorizontalHeaderLabels(self._headername)
    
    def _resizeTable(self):
        self.resizeColumnsToContents()
        self.resizeRowsToContents ()
        self.setCurrentCell(0,0)
        
    def redraw(self):
        (f, self._datarow, self._data) = self._sotrudn.getData()
        self._initTable()
        row = 0
        for id in self._datarow:
            f = self._data[id]["family"]
            try:
                i = "%s."%self._data[id]["name"][0]
            except IndexError:
                i = ''
            try:
                o = "%s."%self._data[id]["surname"][0]
            except IndexError:
                o = ''
            fio = "%s %s%s"%(f, i, o)
            self.itemDraw(row, 0, fio)
            tabel = "%d"%(self._data[id]["tabelnmbr"])
            self.itemDraw(row, 1, tabel)
            brigada = "%s"%(self._data[id]["brigada"])
            self.itemDraw(row, 2, brigada)
            garaj = self._data[id]["garnom"]
            if garaj == 0:
                garaj = "-"
            else:
                garaj = "%d"%garaj
            self.itemDraw(row, 3, garaj)
            prinyat = self._data[id]["date_begin"]
            uvolen = self._data[id]["date_end"]
            if prinyat == date(2001,1,1):
                dt1 = '__.____.___'
            else:
                dt1 = '%s'%str(prinyat)

            if uvolen == date(2001,1,1):
                dt2 = '__.____.___'
            else:
                dt2 = '%s'%str(uvolen)
            dt = "%s - %s"%(dt1,dt2)
            self.itemDraw(row, 4, dt)
            row += 1 
        self._resizeTable()
            
    def itemDraw(self,row,col,data):
        if type(data) == str or type(data) == unicode:
            item = QtGui.QTableWidgetItem("%s"%data)
            self.setItem(row,col,item)    
        elif type(data) == int or type(data) == long:
            item = QtGui.QTableWidgetItem("%d"%data)
            self.setItem(row,col,item)
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class tableEditSotrudn(tableSotrudn):
    def __init__(self, mysql, parent = None):
        tableSotrudn.__init__(self,mysql, parent)
        self._dialog = dialogSotrudn(mysql, parent)
        self.connect(self._dialog, QtCore.SIGNAL("accepted()"),self.save)
    
    def addData(self):        
        self._dialog.setAddMode()

    def editData(self):    
        id = self._datarow[self.currentRow()]
        f = self._data[id]["family"]
        i = self._data[id]["name"]
        o = self._data[id]["surname"]
        tabelnmbr = self._data[id]["tabelnmbr"]
        idbrigada = self._data[id]["idbrigada"]
        prinyat = self._data[id]["date_begin"]
        uvolen = self._data[id]["date_end"]
        idbus = self._data[id]["idbus"]
        self._dialog.setEditMode(f, i, o, tabelnmbr, idbrigada, idbus, prinyat, uvolen)

    def save(self):    
        (f, i, o, tabelnmbr, idbrigada, idbus, prinyat, uvolen, mode) = self._dialog.getResult()
        sort = self.currentRow()
        if mode == 0:
            self._sotrudn.addData(f, i, o, tabelnmbr, idbrigada, idbus, prinyat, uvolen)
        else:
            id = self._datarow[sort]
            self._sotrudn.editData(f, i, o, tabelnmbr, idbrigada, idbus, prinyat, uvolen, id)
        self.redraw()
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class sprDataSotrudn(QtGui.QMdiSubWindow):
    def __init__(self, mysql, parent = None):
        QtGui.QMdiSubWindow.__init__(self, parent)
        self.setWindowTitle(u'Список водителей')        
        self._table = tableEditSotrudn(mysql, parent)
        self._table.redraw()
#        self._brigadacombo = comboBrigada(mysql)
#        self._brigadacombo.redraw()
        self.initAction()        
        self.initWidget()
        self.initConnect()
            
    def initWidget(self):
        widg = QtGui.QWidget()
        grd = QtGui.QGridLayout()
        vh = QtGui.QVBoxLayout()
        tool = QtGui.QToolBar()
        tool.addAction(self.addDataAct)
        tool.addAction(self.editDataAct)
        tool.setIconSize(QtCore.QSize(48,48))
        grd.addWidget(tool)
        grd.addWidget(self._table)
        vh.addLayout(grd)
        widg.setLayout(vh)
        self.setWidget(widg)
    
    def initAction(self):
        self.addDataAct = QtGui.QAction(u'Добавить',self)
        self.addDataAct.setIcon(QtGui.QIcon("../ico/dbplus2.png"))
        self.editDataAct = QtGui.QAction(u'Изменить',self)
        self.editDataAct .setIcon(QtGui.QIcon("../ico/db_update.png"))
        
    def initConnect(self):
        self.connect(self.addDataAct, QtCore.SIGNAL("triggered()"),self._table.addData)
        self.connect(self.editDataAct, QtCore.SIGNAL("triggered()"),self._table.editData)



