#!/usr/bin/python
# -*- coding: utf-8  -*-
from PyQt4 import QtGui, QtCore
from grafik.standart.getdata import getData, editData
from grafik.standart.table import tableData, tableEditData, dialogData, wData
from grafik.brigada.brigada import comboBrigada
from grafik.kartarange.kartarange import generateFlag


class getKartaBrigada(getData):
    'Выдаем информацию какие карточки действия к каким бригадам привязка'
    def __init__(self, mysql):
        getData.__init__(self, mysql)
        
    def getDataSql(self, param):
        idbrigada = param["idbrigada"]
        sql = '''
            SELECT krb.id, k.name, kr.flag, kr.flagnot, k.id as idkarta, k.put
            FROM kartarangebrigada as krb, karta as k,
                kartarange as kr
            WHERE krb.idkartarange = kr.id AND krb.idbrigada = %d AND kr.idkarta = k.id
            ORDER BY 2
                '''%idbrigada 
        return sql
                
    def getData(self, param):
        data = {}
        datarow = []
        (f,dr,d) = self._mysql.getData(self.getDataSql(param))
        for i in dr:
            kid = d[i]["idkarta"]
            try:
                data[kid]
            except:
                datarow += [kid]
                data[kid] = {}
                data[kid]["name"] = d[i]["name"]
                data[kid]["put"] = d[i]["put"]
                #data[kid]["idkarta"] = kid
                data[kid]["kr"] = []
            tmp = {}
            tmp["flag"] = d[i]["flag"]
            tmp["flagnot"] = d[i]["flagnot"]
            data[kid]["kr"] += [tmp]
        
        return (f, datarow, data)
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class kartabrigada(editData):
    '''Редактируем данные по бригадам для карточек-действия '''
    def __init__(self, mysql):
        editData.__init__(self, mysql)
    
    def getDataSql(self, param):
        idkartarange = param["idkartarange"]
        sql = '''
            SELECT krb.id, b.name, krb.idbrigada 
            FROM kartarangebrigada as krb, brigada as b
            WHERE krb.idkartarange = %d AND krb.idbrigada = b.id'''%idkartarange 
        return sql
    
    def addDataSql(self, param):
        idkartarange = param["idkartarange"]
        idbrigada = param["idbrigada"]
        sql = 'INSERT INTO kartarangebrigada (idkartarange, idbrigada) VALUES (%d, %d)'%(idkartarange, idbrigada)
        return sql
    
    def editDataSql(self, param, idkartarangebrigada):
        idkartarange = param["idkartarange"]
        idbrigada = param["idbrigada"]
        sql = 'UPDATE kartarangebrigada SET idkartarange = %d, idbrigada = %d WHERE id = %d'%(idkartarange, idbrigada, idkartarangebrigada)
        return sql
    
    def delDataSql(self, idkartarangebrigada):        
        sql = '''DELETE FROM kartarangebrigada WHERE id = %d'''% idkartarangebrigada
        return sql 
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class tableGetKartaBrigada(tableData):
    'Выдаем таблицу с карточками привязкой по бригадам и инофрмацией по ним'
    def __init__(self, mysql, parent):
        self.setIdBrigada(0)
        self._getData = getKartaBrigada(mysql)
        self._flag = generateFlag()
        tableData.__init__(self, mysql, parent)

    def setIdBrigada(self, idbrigada):
        self._idbrigada = idbrigada
    
    def getIdBrigada(self):
        return self._idbrigada 
        
    def getPack(self):
        param = {}
        param["idbrigada"] = self.getIdBrigada()
        return self.getData().getData(param)
    
    def getHeader(self):
        header = [u'Карточка', u'Сроки действия']
        return header 
    
    def getData(self):
        return self._getData 
        
    def addRow(self, data, row):
        dr = [data["name"]]
        s = ''
        for i in data["kr"]:
            s = '%s [%s]'%(s,self._flag.getDayRangeAll(i['flag'], i['flagnot']))
        dr += [s]
        col = 0
        for i in dr:
            self.itemDraw(row, col, i)
            col += 1
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class tableKartaBrigada(tableEditData):
    '''Таблица для редактирования данных по бригадам для карточек-действия '''
    def __init__(self, mysql, parent):
        self._getData = kartabrigada(mysql)
        self._dialog = dialogKartaBrigada(mysql, parent)
        self.setIdKartaRange(0)
        tableEditData.__init__(self, mysql, parent)
        
    def getPack(self):
        '''Добавляем параметры для получения данных из базы'''
        param = {}
        param["idkartarange"] = self.getIdKartaRange()
        return self.getData().getData(param)    
    
    def setIdKartaRange(self, idKartaRange):
        self._idKartaRange = idKartaRange
    
    def getIdKartaRange(self):
        return self._idKartaRange 
    
    def getHeader(self):    
        return [u'Бригада']
        
    def getData(self):
        return self._getData
    
    def getDialog(self):
        return self._dialog 
    
    def addRow(self, data, row):
        self.itemDraw(row, 0, data["name"])    
    
    def addData(self):
        if self.rowCount() > 0:
            self.editData()
        else:
            self._dialog.setMode(0, u'Добавить бригаду')
    
    def editData(self):
        if self.rowCount() == 0:
            self.addData()
        else:
            data = {}
            iddata = self._datarow[self.currentRow()]
            data["idbrigada"] = self._data[iddata]["idbrigada"]
            self._dialog.setMode(1, u'Изменить бригаду', data)
    
    def getStrForDel(self, iddatas):
        s = self._data[iddatas[0]]["name"]
        res = u'''Вы действительно хотите удалить запись '%s'?'''%s
        return res        
    
    def addParamForSave(self,param):
        param["idkartarange"] = self.getIdKartaRange()
        return param
    
    def delData(self):    
        row = self.currentRow()
        iddata = self._datarow[row]

        dlg = QtGui.QMessageBox(self)
        dlg.setWindowTitle(u'Удаление записи')
        s = self.getStrForDel(iddata)
        dlg.setText(s)
        dlg.setStandardButtons(QtGui.QMessageBox.Cancel | QtGui.QMessageBox.Ok );
        ok = dlg.exec_()
        if ok==QtGui.QMessageBox.Ok:
            self.getData().delData(iddata)
            self.redraw()
            self.setSelect(0)
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++        
class dialogKartaBrigada(dialogData):
    'Диалог для выбора бригады для карточки-действия'
    def __init__(self, mysql, parent = None):
        self._brigada = comboBrigada(mysql, parent)
        dialogData.__init__(self, parent)
        
    def addComponent(self):
        grid = QtGui.QGridLayout()
        lbl1 = QtGui.QLabel(u'Бригада')
        grid.addWidget(lbl1,0,0)
        grid.addWidget(self._brigada,0,1)
        return grid
    
    def setAddMode(self):
        self._brigada.redraw()
        self._brigada.setSelect(0)
        
    def setEditMode(self,param):
        self._brigada.redraw()
        self._brigada.setSelect(param["idbrigada"])
    
    def getResult(self):
        res = {}
        res["idbrigada"] = self._brigada.getSelect()
        return (res, self._mode)
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++        
class wKartaBrigada(wData):
    'Виджет для вывода таблицы по отображению и редактированию бригад для карточек действия'
    def __init__(self, mysql, parent):
        self.setTable(tableKartaBrigada(mysql, parent))
        wData.__init__(self, mysql, parent)

    def setTable(self, tableClass):
        self._table = tableClass
    
    def getTable(self):
        return self._table
    
    def setIdKartaRange(self, idKartaRange):
        self.getTable().setIdKartaRange(idKartaRange)
    
    def initTool(self):
        tool = QtGui.QToolBar()
        tool.addAction(self.addDataAct)
        tool.addAction(self.editDataAct)
        tool.addSeparator()
        tool.addAction(self.delDataAct)
        tool.setIconSize(QtCore.QSize(36,36))
        return tool
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++                
class wGetKartaBrigada(QtGui.QWidget):
    'Виджет для вывода таблицы по отображению и редактированию бригад для карточек действия'
    def __init__(self, mysql, parent):
        self.setTable(tableGetKartaBrigada(mysql, parent))
        QtGui.QWidget.__init__(self, parent)
        self.initWidget()
        self.initConnect()
        self.redraw()
        
    def setTable(self, tableClass):
        self._table = tableClass
    
    def getTable(self):
        return self._table
    
    def setIdBrigada(self, idBrigada):
        self.getTable().setIdBrigada(idBrigada)

    def initWidget(self):
        grd = QtGui.QGridLayout()
        vh = QtGui.QVBoxLayout()
        grd.addWidget(self.getTable())
        vh.addLayout(grd)
        self.setLayout(vh)
        
    def initConnect(self):
        self.connect(self.getTable(), QtCore.SIGNAL("itemSelectionChanged()"),self.emitChangeRow)
            
    def redraw(self):
        self.getTable().redraw()
    
    def getSelect(self):
        return self.getTable().getSelect()
    
    def emitChangeRow(self):
        self.emit(QtCore.SIGNAL('itemSelectionChanged()'))    
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class dialogGetKartaBrigada(QtGui.QDialog):#вспомогательное диалоговое окно
    def __init__(self, mysql, parent = None):
        QtGui.QDialog.__init__(self, parent)
        self.setWindowTitle(u'Добавить карточку')
        self._gkb = wGetKartaBrigada(mysql, parent)        
        self._gkb.redraw()
        self.initWidget()

    def initWidget(self):
        grid = QtGui.QGridLayout(self)
        btn1 = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok|
                                      QtGui.QDialogButtonBox.Cancel)
        self.connect(btn1 , QtCore.SIGNAL("accepted()"), self.accept)
        self.connect(btn1 , QtCore.SIGNAL("rejected()"), self.reject)
        grid.addWidget(self._gkb)
        grid.addWidget(btn1)

    def getResult(self):
        kid = self._gkb.getSelect()
        return (kid)
    
    def setIdBrigada(self, idbrigada):
        self._gkb.setIdBrigada(idbrigada)
        self._gkb.redraw()