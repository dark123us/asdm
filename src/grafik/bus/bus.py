#!/usr/bin/python
# -*- coding: utf-8  -*-
from PyQt4 import QtGui, QtCore

class getBus:
    ''' '''
    def __init__(self, mysql):
        self._mysql = mysql
        self.initSql()
    
    def initSql(self):
        self._getDataSql = '''
            SELECT b.id, b.garnom, b.garnomfull, b.gosnom, bt.name as marka, 
                bt.id as idbt, bt.timeadd
            FROM bus as b, bustype as bt
            WHERE b.idbustype = bt.id AND del = 0
            
            UNION            
            
            SELECT b.id, b.garnom, b.garnomfull, b.gosnom, '' as marka,
                0 as idbt, time('00:00:00') as timeadd
            FROM bus as b
            WHERE b.idbustype = 0 AND del = 0
            
            ORDER BY 2'''
    
    def getData(self):
        (f,dr,d) = self._mysql.getData(self._getDataSql)
        return (f,dr,d)   
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++                
class getBusType:
    ''' '''
    def __init__(self, mysql):
        self._mysql = mysql
        self.initSql()
    
    def initSql(self):
        self._getDataSql = '''
            SELECT id, name
            FROM bustype
            ORDER BY name'''
    
    def getData(self):
        (f,dr,d) = self._mysql.getData(self._getDataSql)
        return (f,dr,d)   
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++                
class editBus(getBus):
    def __init__(self, mysql):
        getBus.__init__(self,mysql)
    
    def initSql(self):
        getBus.initSql(self)
        self._addDataSql = '''INSERT INTO bus (garnom, garnomfull,  gosnom, idbustype) 
            VALUES (%d, %d, '%s', %d)'''
        self._editDataSql = '''UPDATE bus SET garnom = %d,  garnomfull = %d,
            gosnom = '%s', idbustype = %d WHERE id = %d'''
        self._delDataSql = '''UPDATE bus SET del = 1 WHERE id = %d'''
    
    def addData(self, garnom, garnomfull, gosnom, idbustype):
        self._mysql.execsql(self._addDataSql%(garnom, garnomfull, gosnom, idbustype))
        idbus = self._mysql.getlastid()
        return idbus
    
    def editData(self, garnom, garnomfull, gosnom, idbustype, idbus):
        self._mysql.execsql(self._editDataSql%(garnom, garnomfull, gosnom, idbustype, idbus))
    
    def delData(self, idbus):
        self._mysql.execsql(self._delDataSql%(idbus))
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++                
class comboBusType(QtGui.QComboBox):
    def __init__(self, mysql, parent = None):
        QtGui.QComboBox.__init__(self, parent)
        self._bustype = getBusType(mysql)
    
    def redraw(self):
        (f, self._datarow, self._data) = self._bustype.getData()
        self.clear()
        s = []
        for idbt in self._datarow:
            s += ["%s"%(self._data[idbt]['name'])]
        self.addItems(s)
        self.setSelect(0)
        
    def setSelect(self, idbus):
        k = 0
        for i in self._datarow:
            if i == idbus:
                self.setCurrentIndex(k)
                break
            k += 1        
    
    def getSelect(self):
        id = self._datarow[self.currentIndex()]
        return id
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++                
class comboBus(QtGui.QComboBox):
    def __init__(self, mysql, parent = None):
        QtGui.QComboBox.__init__(self, parent)
        self._bus = getBus(mysql)
        
    def redraw(self):
        (f, self._datarow, self._data) = self._bus.getData()
        self.clear()
        s = []
        s = [u"Не закреплен"] #дополнительно добавляем, для
        for id in self._datarow:
            s += ["%s [%s] [%s]"%(self._data[id]['garnom'], 
                  self._data[id]['gosnom'], self._data[id]['marka'])]
        self.addItems(s)
        
    def setSelect(self, idbus):
        k = 1
        for i in self._datarow:
            if i == idbus:
                self.setCurrentIndex(k)
                break
            k += 1        
    
    def getSelect(self):
        if self.currentIndex() == 0:
            id = 0
        else:
            id = self._datarow[self.currentIndex()-1]
        return id
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++        
class tableBus(QtGui.QTableWidget):
    def __init__(self, mysql, parent = None):
        QtGui.QTableWidget.__init__(self,parent)
        self.setEditTriggers(QtGui.QAbstractItemView.EditTriggers(0))
        self._bus = getBus(mysql)
        self._headername = [u'Гаражный номер', u'Гаражный номер полный',  u'Гос.номер', u'Марка']
    
    def _initTable(self):
        self.setRowCount(0)
        self.setRowCount(len(self._datarow))
        self.setColumnCount(len(self._headername))
        self.setHorizontalHeaderLabels(self._headername)
    
    def _resizeTable(self):
        self.resizeColumnsToContents()
        self.resizeRowsToContents ()
        self.setCurrentCell(0,0)
    
    def addRow(self, data, row):
        garnom = data["garnom"]
        self.itemDraw(row, 0, garnom)
        garnomfull = data["garnomfull"]
        self.itemDraw(row, 1, garnomfull)
        gosnom = data["gosnom"]
        self.itemDraw(row, 2, gosnom)
        marka = data["marka"]
        self.itemDraw(row, 3, marka)
        
    def redraw(self):
        (f, self._datarow, self._data) = self._bus.getData()
        self._initTable()
        row = 0
        for i in self._datarow:
            self.addRow(self._data[i], row)
            row += 1 
        self._resizeTable()
            
    def itemDraw(self,row,col,data):
        if type(data) == str or type(data) == unicode:
            item = QtGui.QTableWidgetItem("%s"%data)
            self.setItem(row,col,item)    
        elif type(data) == int or type(data) == long:
            s = str(data)            
            item = QtGui.QTableWidgetItem(s)
            self.setItem(row,col,item)
            
    def getSelect(self):
        i = self._datarow[self.currentRow()]
        return i
        
    def setSelect(self, idbus):
        k = 0
        for i in self._datarow:
            if i == idbus:
                self.setCurrentCell(k,0)
                break
            k += 1
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class tableEditBus(tableBus):
    def __init__(self, mysql, parent = None):
        tableBus.__init__(self,mysql, parent)
        self._bus = editBus(mysql)
        self._dialog = dialogBus(mysql, parent)
        self.connect(self._dialog, QtCore.SIGNAL("accepted()"),self.save)
    
    def addData(self):        
        self._dialog.setAddMode()

    def editData(self, row=0, col=0):    
        i = self._datarow[self.currentRow()]
        garnom = self._data[i]["garnom"]
        garnomfull = self._data[i]["garnomfull"]
        gosnom = self._data[i]["gosnom"]
        idbt = self._data[i]["idbt"]
        self._dialog.setEditMode(garnom, garnomfull, gosnom, idbt)
        
    def delData(self):    
        if self.rowCount()>0:
            row = self.currentRow()
            i = self._datarow[row]
            s = "%d [%s]"%(self._data[i]["garnom"], self._data[i]["gosnom"])
            dlg = QtGui.QMessageBox(self)
            dlg.setWindowTitle(u'Удаление записи')
            dlg.setText(u'''Вы действительно хотите удалить запись '%s'?'''%s)
            dlg.setStandardButtons(QtGui.QMessageBox.Cancel | QtGui.QMessageBox.Ok );
            ok = dlg.exec_()
            if ok==QtGui.QMessageBox.Ok:
                self._bus.delData(i)
                self.redraw()

    def save(self):    
        (garnom, garnomfull, gosnom, idbt, mode) = self._dialog.getResult()
        garnom = int(garnom)
        garnomfull = int(garnomfull)
        row = self.currentRow()
        if mode == 0:
            idb = self._bus.addData(garnom, garnomfull, gosnom, idbt)
        else:
            idb = self._datarow[row]
            self._bus.editData(garnom, garnomfull, gosnom, idbt, idb)
        self.redraw()
        self.setSelect(idb)
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class dialogBus(QtGui.QDialog):#вспомогательное диалоговое окно
    def __init__(self, mysql, parent = None):
        QtGui.QDialog.__init__(self, parent)
        self.setWindowTitle(u'Добавить автобус')
        self._cbt = comboBusType(mysql, parent)        
        self._cbt.redraw()
        self.initWidget()

    def initWidget(self):
        grid = QtGui.QGridLayout(self)
        btn1 = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok|
                                      QtGui.QDialogButtonBox.Cancel)
        self.connect(btn1 , QtCore.SIGNAL("accepted()"), self.accept)
        self.connect(btn1 , QtCore.SIGNAL("rejected()"), self.reject)
        lbl1 = QtGui.QLabel(u'Гаражный номер')
        lbl11 = QtGui.QLabel(u'Гаражный номер полный')
        lbl2 = QtGui.QLabel(u'Гос.номер')        
        lbl3 = QtGui.QLabel(u'Марка автобуса')
        self._ed1 = QtGui.QLineEdit()
        self._ed11 = QtGui.QLineEdit()
        self._ed2 = QtGui.QLineEdit()
        grid.addWidget(lbl1,0,0)
        grid.addWidget(lbl11,1,0)
        grid.addWidget(lbl2,2,0)
        grid.addWidget(lbl3,3,0)
        grid.addWidget(self._ed1,0,1)
        grid.addWidget(self._ed11,1,1)
        grid.addWidget(self._ed2,2,1)
        grid.addWidget(self._cbt,3,1)        
        grid.addWidget(btn1,4,0,1,2)
        
    def setAddMode(self):
        self.setWindowTitle(u'Добавить автобус')
        self._ed1.setText('0')
        self._ed11.setText('0')
        self._ed2.setText('')
        self._cbt.setSelect(0)
        self.setModal(1)
        self._mode = 0
        self.show()
    
    def setEditMode(self, garnom, garnomfull, gosnom, idbt):
        self.setWindowTitle(u'Изменить карточку')
        self._ed1.setText(str(garnom))
        self._ed11.setText(str(garnomfull))
        self._ed2.setText(gosnom)
        self._cbt.setSelect(idbt)
        self.setModal(1)
        self.show()
        self._mode = 1
    
    def getResult(self):
        garnom = self._ed1.text()
        garnomfull = self._ed11.text()
        gosnom = self._ed2.text()
        idbt = self._cbt.getSelect()
        return (garnom, garnomfull, gosnom, idbt, self._mode)
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class wBus(QtGui.QWidget):
    def __init__(self, mysql, parent = None):
        QtGui.QWidget.__init__(self, parent)
        self.initValue(mysql, parent)
    
    def initValue(self, mysql, parent):
        self._table = tableEditBus(mysql, parent)
        self._table.redraw()
        self.initAction()        
        self.initWidget()
        self.initConnect()        
        
    def initWidget(self):
        #widg = QtGui.QWidget(self)
        grd = QtGui.QGridLayout()
        vh = QtGui.QVBoxLayout()
        tool = QtGui.QToolBar()
        tool.addAction(self.addDataAct)
        tool.addAction(self.editDataAct)
        tool.addAction(self.delDataAct)
        tool.setIconSize(QtCore.QSize(48,48))
        grd.addWidget(tool)
        grd.addWidget(self._table)
        vh.addLayout(grd)
        #widg.setLayout(vh)
        self.setLayout(vh)
    
    def initAction(self):
        self.addDataAct = QtGui.QAction(u'Добавить',self)
        self.addDataAct.setIcon(QtGui.QIcon("../ico/dbplus2.png"))
        self.editDataAct = QtGui.QAction(u'Изменить',self)
        self.editDataAct .setIcon(QtGui.QIcon("../ico/db_update.png"))
        self.delDataAct = QtGui.QAction(u'Удалить',self)
        self.delDataAct .setIcon(QtGui.QIcon("../ico/dbmin2.png"))
        
    def initConnect(self):
        self.connect(self.addDataAct, QtCore.SIGNAL("triggered()"),self._table.addData)
        self.connect(self.editDataAct, QtCore.SIGNAL("triggered()"),self._table.editData)
        self.connect(self._table, QtCore.SIGNAL("cellDoubleClicked(int,int)"),self._table.editData)
        self.connect(self.delDataAct, QtCore.SIGNAL("triggered()"),self._table.delData)
        self.connect(self._table, QtCore.SIGNAL("itemSelectionChanged()"),self.emitChangeRow)
            
    def redraw(self):
        self._table.redraw()
    
    def getSelect(self):
        return self._table.getSelect()
    
    def emitChangeRow(self):
        self.emit(QtCore.SIGNAL('itemSelectionChanged()'))
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class sprBus(QtGui.QMdiSubWindow):
    def __init__(self, mysql, parent = None):
        QtGui.QMdiSubWindow.__init__(self, parent)
        self.setWindowTitle(u'Список автобусов')        
        self._widg = wBus(mysql, parent)
        self.setWidget(self._widg)
        