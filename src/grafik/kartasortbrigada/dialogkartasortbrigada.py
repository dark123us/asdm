#!/usr/bin/python
# -*- coding: utf-8  -*-
from PyQt4 import QtGui, QtCore
from grafik.dop.dop import comboDop
from grafik.karta.karta import comboKarta

class dialogKartaSortBrigada(QtGui.QDialog):#вспомогательное диалоговое окно
    def __init__(self, mysql, parent = None):
        QtGui.QDialog.__init__(self, parent)
        self.setWindowTitle(u'Добавить карточку')
        self._karta = comboKarta(mysql, parent)
        self._dop = comboDop(mysql, parent)
        self._karta.redraw()
        self._dop.redraw() 
        self.initWidget()

    def initWidget(self):
        grid = QtGui.QGridLayout(self)
        btn1 = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok|
                                      QtGui.QDialogButtonBox.Cancel)
        self.connect(btn1 , QtCore.SIGNAL("accepted()"), self.accept)
        self.connect(btn1 , QtCore.SIGNAL("rejected()"), self.reject)
        self._rbtn1 = QtGui.QRadioButton(u'Карточка')
        self._rbtn2 = QtGui.QRadioButton(u'Дополнительно')
        self._rbtn1.setChecked(2)
        self.connect(self._rbtn1, QtCore.SIGNAL("toggled(bool)"), self.chEnabled)
        self._dop.setEnabled(False)
        grid.addWidget(self._rbtn1,0,0)
        grid.addWidget(self._rbtn2,1,0)
        grid.addWidget(self._karta,0,1)
        grid.addWidget(self._dop,1,1)
        grid.addWidget(btn1,2,0,1,2)
        
    def chEnabled(self, state=0):
        st = state
        self._dop.setEnabled(not st)
        self._karta.setEnabled(st)
        
    def setAddMode(self):
        self.setWindowTitle(u'Добавить данные')
        self._karta.redraw()
        self._dop.redraw()
        self._rbtn1.setChecked(2)
        self.setModal(1)
        self.show()
    
    def setEditMode(self, idvodgrsort, idkarta, idtype):
        self.setWindowTitle(u'Изменить данные')
        try:
            self._karta.redraw()
            self._dop.redraw()
            if  idtype:
                self._dop.setSelect(idkarta)
                self._rbtn2.setChecked(2)
            else:
                self._karta.setSelect(idkarta)
                self._rbtn1.setChecked(2)
            self.setModal(1)
            self.show()
        except:
            self.setAddMode()
    
    def getResult(self):
        if self._rbtn1.isChecked():
            idkarta = self._karta.getSelect()
            idtype = 0
        else:
            idkarta = self._dop.getSelect()
            idtype = 1
        return (idkarta, idtype)