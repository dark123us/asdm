#!/usr/bin/python
# -*- coding: utf-8  -*-
from PyQt4 import QtGui, QtCore
from datetime import date

class getHoliday:
    def __init__(self, mysql):
        self._mysql = mysql
        self.initSql()
        
    def initSql(self):
        ''' Список всех выходных'''
        self._getDataSql = '''SELECT id, name, date FROM holiday ORDER BY date'''
        self._getDataDateSql = '''
            SELECT id, name, date 
            FROM holiday 
            WHERE date>= '%s' AND date<= '%s' ORDER BY date'''
    
    def getData(self, bdate = date(2001,1,1), edate = date(2001,1,1) ):
        if bdate > date(2001,1,1):
            (f,dr,d) = self._mysql.getData(self._getDataDateSql%(bdate,edate))
        else:
            (f,dr,d) = self._mysql.getData(self._getDataSql)
        return (f,dr,d)
    
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class editHoliday(getHoliday):
    def __init__(self, mysql):
        getHoliday.__init__(self, mysql)
    
    def initSql(self):
        getHoliday.initSql(self)
        self._addDataSql = '''INSERT INTO holiday(name, date) VALUES ('%s', '%s')'''
        self._editDataSql = '''UPDATE holiday SET name = '%s', date = '%s' WHERE id = %d'''
        self._delDataSql = '''DELETE FROM holiday WHERE id = %d'''
        
    def addData(self, name, ddate):
        self._mysql.execsql(self._addDataSql%(name, ddate))
    
    def editData(self, name, ddate, id):
        self._mysql.execsql(self._editDataSql%(name, ddate, id))
    
    def delData(self, id):
        self._mysql.execsql(self._delDataSql%(id))
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class tableHoliday(QtGui.QTableWidget):
    def __init__(self, mysql, parent = None):
        QtGui.QTableWidget.__init__(self,parent)
        self.setEditTriggers(QtGui.QAbstractItemView.EditTriggers(0))
        self._holiday = editHoliday(mysql)
        self._headername = [u'Название', u'Дата']
    
    def _initTable(self):
        self.setRowCount(0)
        self.setRowCount(len(self._datarow))
        self.setColumnCount(len(self._headername))
        self.setHorizontalHeaderLabels(self._headername)
    
    def _resizeTable(self):
        self.resizeColumnsToContents()
        self.resizeRowsToContents ()
        self.setCurrentCell(0,0)
        
    def redraw(self):
        (f, self._datarow, self._data) = self._holiday.getData()
        self._initTable()
        row = 0
        for id in self._datarow:
            s = self._data[id]["name"]
            self.itemDraw(row, 0, s)
            s = str(self._data[id]["date"])
            self.itemDraw(row, 1, s)
            row += 1 
        self._resizeTable()
            
    def itemDraw(self,row,col,data):
        if type(data) == str or type(data) == unicode:
            item = QtGui.QTableWidgetItem("%s"%data)
            self.setItem(row,col,item)    
        elif type(data) == int or type(data) == long:
            item = QtGui.QTableWidgetItem("%d"%data)
            self.setItem(row,col,item)
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class tableEditHoliday(tableHoliday):
    def __init__(self, mysql, parent = None):
        tableHoliday.__init__(self,mysql, parent)
        self._dialog = dialogHoliday(mysql, parent)
        self.connect(self._dialog, QtCore.SIGNAL("accepted()"),self.save)
    
    def addData(self):        
        self._dialog.setAddMode()

    def editData(self):    
        id = self._datarow[self.currentRow()]
        name = self._data[id]["name"]
        ddate = self._data[id]["date"]
        self._dialog.setEditMode(name, ddate)

    def save(self):    
        (name, ddate, mode) = self._dialog.getResult()
        sort = self.currentRow()
        if mode == 0:
            self._holiday.addData(name, ddate)
        else:
            id = self._datarow[sort]
            self._holiday.editData(name, ddate, id)
        self.redraw()
    
    def delData(self):    
        if self.rowCount()>0:
            row = self.currentRow()
            id = self._datarow[row]
            s = self._data[id]["name"]
            dlg = QtGui.QMessageBox(self)
            dlg.setWindowTitle(u'Удаление записи')
            dlg.setText(u'''Вы действительно хотите удалить запись '%s'?'''%s)
            dlg.setStandardButtons(QtGui.QMessageBox.Cancel | QtGui.QMessageBox.Ok );
            ok = dlg.exec_()
            if ok==QtGui.QMessageBox.Ok:
                self._holiday.delData(id)
                self.redraw()
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class sprDataHoliday(QtGui.QMdiSubWindow):
    def __init__(self, mysql, parent = None):
        QtGui.QMdiSubWindow.__init__(self, parent)
        self.setWindowTitle(u'Список праздничных')        
        self._table = tableEditHoliday(mysql, parent)
        self._table.redraw()
        self.initAction()        
        self.initWidget()
        self.initConnect()
            
    def initWidget(self):
        widg = QtGui.QWidget()
        grd = QtGui.QGridLayout()
        vh = QtGui.QVBoxLayout()
        tool = QtGui.QToolBar()
        tool.addAction(self.addDataAct)
        tool.addAction(self.editDataAct)
        tool.addAction(self.delDataAct)
        tool.setIconSize(QtCore.QSize(48,48))
        grd.addWidget(tool)
        grd.addWidget(self._table)
        vh.addLayout(grd)
        widg.setLayout(vh)
        self.setWidget(widg)
    
    def initAction(self):
        self.addDataAct = QtGui.QAction(u'Добавить',self)
        self.addDataAct.setIcon(QtGui.QIcon("../ico/dbplus2.png"))
        self.editDataAct = QtGui.QAction(u'Изменить',self)
        self.editDataAct .setIcon(QtGui.QIcon("../ico/db_update.png"))
        self.delDataAct = QtGui.QAction(u'Удалить',self)
        self.delDataAct .setIcon(QtGui.QIcon("../ico/dbmin2.png"))
        
    def initConnect(self):
        self.connect(self.addDataAct, QtCore.SIGNAL("triggered()"),self._table.addData)
        self.connect(self.editDataAct, QtCore.SIGNAL("triggered()"),self._table.editData)
        self.connect(self.delDataAct, QtCore.SIGNAL("triggered()"),self._table.delData)
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class dialogHoliday(QtGui.QDialog):#вспомогательное диалоговое окно
    def __init__(self, mysql, parent = None):
        QtGui.QDialog.__init__(self, parent)
        self.setWindowTitle(u'Добавить праздничный')
        self.initWidget()

    def initWidget(self):
        grid = QtGui.QGridLayout(self)
        btn1 = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok|
                                      QtGui.QDialogButtonBox.Cancel)
        self.connect(btn1 , QtCore.SIGNAL("accepted()"), self.accept)
        self.connect(btn1 , QtCore.SIGNAL("rejected()"), self.reject)
        lbl1 = QtGui.QLabel(u'Название')
        lbl2 = QtGui.QLabel(u'Дата')
        self._ed1 = QtGui.QLineEdit()
        self._ded1 = QtGui.QDateEdit()
        self._ded1.setCalendarPopup(True)
        grid.addWidget(lbl1,0,0)
        grid.addWidget(lbl2,1,0)
        grid.addWidget(self._ed1,0,1)
        grid.addWidget(self._ded1,1,1)
        grid.addWidget(btn1,2,0,1,2)
        
    def setAddMode(self):
        self.setWindowTitle(u'Добавить данные')
        self._ed1.setText('')
        self._ded1.setDate(QtCore.QDate(date.today()))
        
        self.setModal(1)
        self._mode = 0
        self.show()
    
    def setEditMode(self, name, ddate):
        self.setWindowTitle(u'Изменить данные')
        self._ed1.setText(name)
        self._ded1.setDate(QtCore.QDate(ddate))
        self.setModal(1)
        self.show()
        self._mode = 1
    
    def getResult(self):
        name = self._ed1.text()
        ddate = self._ded1.date().toPyDate()
        return (name, ddate, self._mode)
