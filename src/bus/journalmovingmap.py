#!/usr/bin/python
# -*- coding: utf-8  -*-
from sprmap.controlmap import controlmap
from sprmap.modelmap import track
import threading
import time
import pygame
import Image,ImageDraw

class journalmovingmap(controlmap):
    def __init__(self, data, datarow):
        self.drawpointfl = 1
        self.datatrack = data
        self.datarowtrack = datarow
        controlmap.__init__(self)
        self.mode = (600,400)
        self.initScreen()
        self.track = track(self.xyt)
        self.lat = 0
        self.lon = 0

    def initrun(self):
        self.loadnotile()
        self.loadtile()
        self.redraw()
        self.draw()
        self.fps = 0
        self.st = time.time()

    def run(self):
        self.readevent()
        pygame.display.update()
        self.calcfps()

    def closerun(self):
        pygame.quit()

    def calcfps(self):
        self.fps += 1
        if time.time()-self.st>1:
            #print "fps = %d"%self.fps
            self.fps = 0
            self.st = time.time()
#            x = -self.x + self.xk
#            y = -self.y + self.yk
#            lon, lat = self.track.getLatLon(x, y, self.zoom)
#            xx = self.track.getXY(lat,lon,self.zoom)
#            print x, y, (lat,lon), xx, self.tileangle

    def drawpoint(self):
        xx = self.track.getXY(self.lat,self.lon,self.zoom)
        self.pointoldx = xx[0]
        self.pointoldy = xx[1]

    def calctrack(self):
        st = time.time()
        x1 = 0
        y1 = 0
        fl = 0
        self.tracksurf.fill(255)
        self.tracksurf.set_alpha(255)
        for id in self.datarowtrack:
            lat = self.datatrack[id]["lat"]
            lon = self.datatrack[id]["lon"]
            xx = self.track.getXY(lat,lon,self.zoom)
            x = xx[0] + self.x
            y = xx[1] + self.y
            if fl:
                pygame.draw.line(self.tracksurf,(0,0,255),(x,y),(x1,y1),3)
            else:
                fl = 1
            x1 = x
            y1 = y

    def redraw(self):
        controlmap.redraw(self)
        if self.drawpointfl:
            xs = self.x - self.pointoldx
            ys = self.y - self.pointoldy
            self.pointsurf.fill(255)
            self.pointsurf.set_alpha(255)
            x = self.pointoldx + self.x
            y = self.pointoldy + self.y
            pygame.draw.rect(self.pointsurf,(255,0,255),(x-3,y-3,6,6),3)

    def draw(self):
        controlmap.draw(self)
        if self.moveflag:
            self.screen.blit(self.tracksurf,(self.x - self.x1, self.y - self.y1))
        else:
            self.screen.blit(self.tracksurf,(0,0))
        if self.drawpointfl:
            self.screen.blit(self.pointsurf,(0,0))

    def loadtile(self):
        controlmap.loadtile(self)
        self.calctrack()
        if self.drawpointfl:
            self.drawpoint()

    def initScreen(self):
        controlmap.initScreen(self)
        self.pointsurf = pygame.Surface(self.mode,pygame.SRCALPHA)
        self.tracksurf = pygame.Surface(self.mode,pygame.SRCALPHA)


            

