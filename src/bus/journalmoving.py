#!/usr/bin/python
# -*- coding: utf-8  -*-
from spr.view import viewtable
from PyQt4 import QtGui, QtCore, Qt
from datetime import date,timedelta
from spr.view import viewcombo
from journalmovingmap import journalmovingmap

class cmbGarNom(viewcombo):
    def __init__(self, parent = None, mysql = None):
        self.initSql()
        viewcombo.__init__(self, parent, mysql)
        self.dataview = ["garnom","gosnom"]

    def initSql(self):
        self.dataparam = ["id", "garnom", "gosnom", "nmbr"]
        self.getDataSQL = '''SELECT b.id, b.garnom, b.gosnom, p.nmbr
                FROM bus as b, pribor as p
                WHERE b.del = 0 AND p.idbus = b.id
                ORDER BY b.garnom'''

class journaltable(viewtable):
    def __init__(self, parent=None, mysql=None):
        self.initSql()
        viewtable.__init__(self, parent, mysql)
        self.headername = [u"Ид", u"Широта", u"Долгота", u"Дата и время",
            u"Точки"]#, u"Угол", u"Скорость", u"Высота"]
        self.dataview = ["id", "lat", "lon", "datetimepribor", "npoint"]#,"angle", "speed", "alt"]
        self.dataparam = ["id", "lat", "lon", "datetimepribor", "npoint"]#, "angle", "speed", "alt"]

    def initSql(self):
        #nmbr, nmbr, date1-1, datetime1, nmbr, datetime1, datetime2
        self.getDataSQL = '''
            SELECT id, lat, lon, nmbr, datetimepribor, datetimeprogramm, npoint
            FROM point%s WHERE nmbr = %d AND datetimepribor =(
                SELECT max(datetimepribor)
                FROM point%s
                WHERE nmbr = %d AND
                    datetimepribor > '%s' AND
                    datetimepribor < '%s')
            UNION
            SELECT id, lat, lon, nmbr, datetimepribor, datetimeprogramm, npoint 
            FROM point%s
            WHERE nmbr = %d AND
                datetimepribor > '%s' AND
                datetimepribor < '%s'
            ORDER BY datetimepribor'''

    def itemtable(self,row,col,s):
        item = QtGui.QTableWidgetItem("%s"%s)
        if self.data[self.datarow[row]]["npoint"]>0:
            item.setBackground(QtGui.QBrush(12,1))
        self.setItem(row,col,item)

class journalmoving(QtGui.QDialog):
    def __init__(self, mysql, parent = None):
        QtGui.QDialog.__init__(self, parent)
        self.setWindowTitle(u'Журнал движения')
        #self.karta = getkarta(mysql)
        self.garnom = cmbGarNom(self,mysql)
        self.garnom.redraw()
        self.table = journaltable(self,mysql)
        self.initWidget()
        
        nmbr = self.garnom.data[self.garnom.datarow[0]]["nmbr"]
        date1 = self.date1.dateTime().toPyDateTime()
        date2 = self.date2.dateTime().toPyDateTime()
        dateold = date1.date() - timedelta(1,0)
        yearmonth = "_%04d%02d"%(date1.year,date1.month)
        self.table.redraw([yearmonth, nmbr, yearmonth, nmbr, dateold, date1, yearmonth, nmbr, date1, date2])
        self.initAction()
        self.currow = 0
        self.showmapfl = 0

    def initWidget(self):
        #self.initTable()
        z = date.today()
        #z = date(2011, 10, 5)
        
        gb1 = QtGui.QGroupBox(u'Период')
        hv1 = QtGui.QHBoxLayout()
        lb1 = QtGui.QLabel(u"с")
        lb2 = QtGui.QLabel(u"по")
        self.date1 = QtGui.QDateTimeEdit(QtCore.QDateTime(z))
        self.date1.setCalendarPopup(True)
        self.date2 = QtGui.QDateTimeEdit(QtCore.QDateTime(z+timedelta(1,0)))
        self.date2.setCalendarPopup(True)
        hv1.addWidget(lb1)
        hv1.addWidget(self.date1)
        hv1.addWidget(lb2)
        hv1.addWidget(self.date2)
        gb1.setLayout(hv1)
        
        gb2 = QtGui.QGroupBox(u'Дополнительно')
        hv2 = QtGui.QHBoxLayout()
        lb3 = QtGui.QLabel(u"Гаражный номер")
        hv2.addWidget(lb3)
        hv2.addWidget(self.garnom)
        gb2.setLayout(hv2)
        
        self.btnmap = QtGui.QPushButton(u'Карта')

        hva1 = QtGui.QHBoxLayout()
        hva1.addWidget(gb1)
        hva1.addWidget(gb2)
        hva1.addWidget(self.btnmap)
        hva1.addStretch(1)

        grid = QtGui.QGridLayout(self)
        #grid.addWidget(self.tool)
        grid.addLayout(hva1,0,0)
        grid.addWidget(self.table)
        #grid.addWidget(self.table)
        self.tm = QtCore.QTimer()

    def initAction(self):
        self.connect(self.date1, QtCore.SIGNAL("dateTimeChanged(QDateTime)"),self.dateTimeChanged1)
        self.connect(self.date2, QtCore.SIGNAL("dateTimeChanged(QDateTime)"),self.dateTimeChanged2)
        self.connect(self.garnom, QtCore.SIGNAL("currentIndexChanged(int)"),self.nmbrChanged)
        self.connect(self.table, QtCore.SIGNAL("itemSelectionChanged()"),self.tblselChanged)
        self.connect(self.btnmap, QtCore.SIGNAL("clicked()"),self.showmap)
        self.connect(self.tm, QtCore.SIGNAL("timeout ()"),self.runmap)
    
    def dateTimeChanged1(self,datetime):
        dt = datetime.toPyDateTime() + timedelta(1,0)# self.date1.dateTime().toPyDateTime()+timedelta(1,0)
        self.date2.setDateTime(QtCore.QDateTime(dt))

    def dateTimeChanged2(self,datetime):
        row = self.garnom.currentIndex()
        nmbr = self.garnom.data[self.garnom.datarow[row]]["nmbr"]
        date1 = self.date1.dateTime().toPyDateTime()
        date2 = self.date2.dateTime().toPyDateTime()

        yearmonth = "_%04d%02d"%(date1.year,date1.month)
        self.table.redraw([yearmonth, nmbr, yearmonth, nmbr, date1, date2, yearmonth, nmbr, date1, date2])

    def nmbrChanged(self,row):
        nmbr = self.garnom.data[self.garnom.datarow[row]]["nmbr"]
        date1 = self.date1.dateTime().toPyDateTime()
        date2 = self.date2.dateTime().toPyDateTime()
        yearmonth = "_%04d%02d"%(date1.year,date1.month)
        self.table.redraw([yearmonth, nmbr, yearmonth, nmbr, date1, date2, yearmonth, nmbr, date1, date2])
        #self.table.redraw([nmbr, nmbr, date1, date2, nmbr, date1, date2])
        if self.showmapfl:
            self.map.datatrack = self.table.data
            self.map.datarowtrack = self.table.datarow
            self.map.calctrack()
            self.map.redraw()
            self.map.draw()
            self.tblselChanged()

    def tblselChanged(self):
        if self.showmapfl:
            if self.table.rowCount() > 0:
                try:                
                    id = self.table.datarow[self.table.currentRow ()]
                    self.map.lat = self.table.data[id]["lat"]
                    self.map.lon = self.table.data[id]["lon"]
    
                    self.map.drawpointfl = 1
                    self.map.loadtile()
                    self.map.redraw()
                    self.map.draw()
                except IndexError:
                    pass

    def showmap(self):
        if self.showmapfl:
            self.tm.stop()
            self.map.closerun()
            self.showmapfl = 0
        else:
            self.map = journalmovingmap(self.table.data,self.table.datarow)
            self.map.initrun()
            self.showmapfl = 1
            self.tm.start(1)

    def runmap(self):
        if self.map.runflag:
            self.map.run()
        else:
            self.showmap()