#!/usr/bin/python
# -*- coding: utf-8  -*-
from PyQt4 import QtGui, QtCore
from spr.view import view

class editWind(QtGui.QDialog):#вспомогательное диалоговое окно
    def __init__(self, parent = None):
        QtGui.QDialog.__init__(self, parent)
        self.grid = QtGui.QGridLayout(self)
        self.btn1 = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok|
                                      QtGui.QDialogButtonBox.Cancel)
        self.connect(self.btn1 , QtCore.SIGNAL("accepted()"), self.accept)
        self.connect(self.btn1 , QtCore.SIGNAL("rejected()"), self.reject)
        lbl1 = QtGui.QLabel(u'Карточка')
        lbl2 = QtGui.QLabel(u'Соответствие карточки')
        lbl3 = QtGui.QLabel(u'Смена')
        self.lb1 = QtGui.QLabel('')
        self.ed1 = QtGui.QLineEdit()
        self.ed2 = QtGui.QLineEdit()
        self.grid.addWidget(lbl1,0,0)
        self.grid.addWidget(self.lb1,0,1)
        self.grid.addWidget(lbl2,1,0)
        self.grid.addWidget(self.ed1,1,1)
        self.grid.addWidget(lbl3,2,0)
        self.grid.addWidget(self.ed2,2,1)
        self.grid.addWidget(self.btn1,3,0,1,2)

class kartaput(QtGui.QDialog):
    "Диалоговое окно установки соответствия имени карточки и кодового обозначения"
    
    def __init__(self, parent = None, mysql = None):
        QtGui.QDialog.__init__(self,parent)
        self.list = view(parent, mysql)#основной виджет, куда выводим список данных
        self.initSql()
        self.initWidget()
        self.edit = editWind(self)
        self.connect(self.edit, QtCore.SIGNAL("accepted()"),self.save)

    def initWidget(self):
        "Инициализация виджета"
        
        grid = QtGui.QGridLayout(self)
        self.tool = QtGui.QToolBar()
        self.tool.setIconSize(QtCore.QSize(48,48))
        self.initAction()
        self.tool.addAction(self.editDataAct)
        grid.addWidget(self.tool)
        grid.addWidget(self.list)
        self.list.redraw([])

    def initAction(self):
        "Инициализация действий"
        
        self.editDataAct = QtGui.QAction(u'Изменить',self)
        self.editDataAct.setShortcut('Return')
        self.editDataAct .setIcon(QtGui.QIcon("../../ico/db_update.png"))
        self.connect(self.editDataAct , QtCore.SIGNAL("triggered()"),self.editDataControl)
        self.connect(self.list , QtCore.SIGNAL("triggered()"),self.editDataControl)

    def initSql(self):
        "Инициализация запросов и их параметров"
        
        self.list.dataview = ["name", "smena", "put"]
        self.list.dataparam = ["id", "name", "smena", "put"]
        self.list.getDataSQL = '''SELECT id, name, smena, put FROM karta WHERE del = 0 ORDER BY name'''
        self.list.editDataSQL = '''UPDATE karta SET smena = %d, put = %d WHERE id = %d'''

    def editDataControl(self):
        "Действия при изменении данных - в частности запись кода путевого листа"
        
        r = self.list.list.currentRow()
        self.edit.lb1.setText(self.list.data[self.list.datarow[r]]["name"])
        self.edit.ed1.setText(str(self.list.data[self.list .datarow[r]]["put"]))
        self.edit.ed2.setText(str(self.list.data[self.list .datarow[r]]["smena"]))
        self.edit.setModal(1)
        self.edit.show()

    def save(self):
        "Действия по сохранению данных"
        
        try:
            datasave = [int(self.edit.ed2.text()),int(self.edit.ed1.text())]
        except:
            datasave = 0
        r = self.list.list.currentRow()
        id = self.list.datarow[r]
        self.list._editData(datasave+[id])
        self.list.redraw([])
        self.list.list.setCurrentRow(r)
