#!/usr/bin/python
# -*- coding: utf-8  -*-
from PyQt4 import QtGui, QtCore
from spr.view import viewtable

class buspribor(viewtable):
    def __init__(self, parent = None, mysql = None):
        self.initSql()
        viewtable.__init__(self, parent, mysql)
        self.dataview = ["garnom", "gosnom", "marka", "nmbr", "telefon"]
        self.headername = [u"Гаражный", u"Гос.номер", u"Марка", u"номер прибора",
            u"Номер телефона"]

    def initSql(self):
        self.dataparam = ["id", "garnom", "gosnom", "marka", "nmbr", "telefon"]
        self.getDataSQL = '''
            SELECT b.id, b.garnom, b.gosnom, b.marka, p.nmbr, p.telefon
            FROM bus as b, pribor as p
            WHERE b.del = 0 AND p.idbus = b.id
            ORDER BY %s'''

class buspribordialog(QtGui.QDialog):
    def __init__(self, parent = None, mysql = None):
        QtGui.QDialog.__init__(self, parent)
        self.setWindowTitle(u'Справочник автобусов и приборов')
        self.table = buspribor(self,mysql)
        self.selcol = 0
        self.dest = 0
        self.sortChanged(self.selcol)
        self.initWidget()
        self.initAction()

    def initWidget(self):
        grid = QtGui.QGridLayout(self)
        grid.addWidget(self.table)

    def initAction(self):
        self.connect(self.table.horizontalHeader(), QtCore.SIGNAL("sectionClicked(int)"),self.sortChanged)

    def sortChanged(self,i):
        if i == self.selcol:
            self.dest = 1 - self.dest
        else:
            self.selcol = i
            self.dest = 0
        if self.dest: s = ' ASC'
        else: s = ' DESC'
        d = "%s %s"%(self.table.dataview[self.selcol],s)
        self.table.redraw([d])
