#!/usr/bin/python
# -*- coding: utf-8  -*-
from PyQt4 import QtGui, QtCore
from db import db
from editostanov import Ui_Dialog
from ostanov import ostanovSpis

class spravSpis(QtGui.QTableWidget):
    '''класс списка остановок
    '''
    def __init__(self, parent=None):
        QtGui.QTableWidget.__init__(self, parent)
        self.setRowCount(2)
        self.setColumnCount(2)
        self.mydb = db()
        self.setEditTriggers(QtGui.QAbstractItemView.EditTriggers(0))
        self.initAction()

    def initAction(self):
        self.viewSpisAct = QtGui.QAction(u'Список остановок',self)
        self.viewSpisAct.setShortcut('Alt+O')
        self.viewSpisAct.setIcon(QtGui.QIcon('../ico/database.png'))
        self.connect(self.viewSpisAct,QtCore.SIGNAL("triggered()"),self.reread)

    def reread(self):
        self.mydb.sql = 'SELECT id,name,soundfile FROM ostanov ORDER BY name'
        self.mydb.execsql()
        res = self.mydb.cursor.fetchall()
        n = self.mydb.cursor.rowcount
        self.setRowCount(n)
        i = 0
        self.id = []
        self.soundfile = []
        self.name = []
        for row in res:
            self.id += [row["id"]]
            self.soundfile += [row["soundfile"]]
            self.name += [row["name"]]

            item = QtGui.QTableWidgetItem(str(self.id[i]))
            self.setItem(i,0,item)
            item = QtGui.QTableWidgetItem(row["name"])
            self.setItem(i,1,item)
            i += 1
        self.resizeColumnsToContents()

class ostanovEdutForm(QtGui.QDialog,Ui_Dialog):
    '''Диалоговая форма для добавление и изменения
    '''
    def __init__(self, parent=None):
        QtGui.QDialog.__init__(self, parent)
        self.setupUi(self)
        self.connect(self.pushButton,QtCore.SIGNAL('clicked()'),self.opendialog)
        #self.connect(self,QtCore.SIGNAL('rejected()'),self.clos)
    def opendialog(self):
        self.lineEdit_2.setText(QtGui.QFileDialog.getOpenFileName(self,
            u'Выбрать звуковой файл',"",u'Звуковые файлы (*.wav *.mp3)'))
    def clos(self):
        print 1
        

class ostanovEdit(ostanovSpis):
    '''расширяющий класс для редактирования записей с остановками
    '''
    def __init__(self):
        super(ostanovEdit,self).__init__()
        self.diag = ostanovEdutForm(self)
    def __add(self):
        self.mydb.sql = '''INSERT INTO ostanov (name,soundfile)
            VALUES ('%s','%s')'''%(self.diag.lineEdit.text(),
            self.diag.lineEdit_2.text())
        self.mydb.execsql()
        self.reread()
    def add(self):
        self.diag.setWindowTitle(u'Добавить остановку')
        self.connect(self.diag,QtCore.SIGNAL('accepted()'),self.__add)
        self.diag.show()
    def __edit(self):
        self.mydb.sql = '''UPDATE ostanov SET name='%s',soundfile='%s'
            WHERE id = %d'''%(self.diag.lineEdit.text(),
            self.diag.lineEdit_2.text(),self.id[self.currentRow()])
        self.mydb.execsql()
        self.reread()
    def edit1(self):
        self.diag.setWindowTitle(u'Изменить остановку')
        self.diag.lineEdit.setText(self.name[self.currentRow()])
        self.diag.lineEdit_2.setText(self.soundfile[self.currentRow()])
        self.connect(self.diag,QtCore.SIGNAL('accepted()'),self.__edit)
        self.diag.show()
    def delete(self):
        dlg = QtGui.QMessageBox(self)
        dlg.setWindowTitle(u'Удаление записи')
        dlg.setText(u'''Вы действительно хотите удалить остановку '%s?' '''% self.name[self.currentRow()])
        #dlg.setInformativeText(u'Вы действительно хотите удалить запись?')
        dlg.setStandardButtons(QtGui.QMessageBox.Cancel | QtGui.QMessageBox.Ok );
        ok = dlg.exec_()
        if ok==QtGui.QMessageBox.Ok :
            self.mydb.sql = '''DELETE FROM ostanov WHERE id = %d'''%(self.id[self.currentRow()])
            self.mydb.execsql()
            self.reread()
    def initAction(self):
        super(ostanovEdit,self).initAction()
        self.addSpisAct = QtGui.QAction(u'Добавить остановку',self)
        self.addSpisAct.setShortcut('Ins')
        self.addSpisAct.setIcon(QtGui.QIcon('../ico/dbplus2.png'))
        
        self.connect(self.addSpisAct,QtCore.SIGNAL("triggered()"),self.add)

        self.editSpisAct = QtGui.QAction(u'Редактировать остановку',self)
        self.editSpisAct.setShortcut('Home')
        self.editSpisAct.setIcon(QtGui.QIcon('../ico/db_update.png'))
        self.connect(self.editSpisAct,QtCore.SIGNAL("triggered()"),self.edit1)

        self.delSpisAct = QtGui.QAction(u'Удалить остановку',self)
        self.delSpisAct.setShortcut('Del')
        self.delSpisAct.setIcon(QtGui.QIcon('../ico/dbmin2.png'))
        self.connect(self.delSpisAct,QtCore.SIGNAL("triggered()"),self.delete)

class ostanovMain(QtGui.QDialog):
    '''класс формы остановок, содержит список,
    для дальнейшего их редактирования или использования при выборе
    к примеру очередности
    '''
    def __init__(self, parent=None):
        QtGui.QDialog.__init__(self, parent)
        mainwidget = QtGui.QWidget()
        self.setWindowTitle(u'Остановки')
        self.setMinimumSize(600,400)
        self.spis = ostanovEdit() #Spis()
        self.tool = QtGui.QToolBar(self)
        self.tool.setIconSize(QtCore.QSize(64,64))
        self.tool.addAction(self.spis.viewSpisAct)
        self.tool.addAction(self.spis.addSpisAct)
        self.tool.addAction(self.spis.editSpisAct)
        self.tool.addAction(self.spis.delSpisAct)
        #self.spis.add()
        vb = QtGui.QVBoxLayout(self)
        vb.addWidget(self.tool)
        vb.addWidget(self.spis)


if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    qb = ostanovMain()
    qb.show()
    sys.exit(app.exec_())

