#!/usr/bin/python
# -*- coding: utf-8  -*-
from PyQt4 import QtGui, QtCore
from db import db
#from ui_marshwostanov import Ui_Dialog
from marsh import marshSpis
from ostanov import ostanovSpis

class widgOstanov(QtGui.QWidget):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.mydb = db()
        self.obrat = 0
        self.idmarsh = 0
        self.idostanov = 0
        self.mem = []
        self.initWidget()
        
    def initWidget(self):
        self.twSpisOstanov = QtGui.QTableWidget()
        self.twSpisOstanov.setColumnCount(1)
        self.twSpisOstanov.setRowCount(0)
        pbSave = QtGui.QPushButton()
        pbPaste = QtGui.QPushButton()
        pbRevers = QtGui.QPushButton()
        pbAdd = QtGui.QPushButton()
        pbIns = QtGui.QPushButton()
        pbDel = QtGui.QPushButton()
        self.label = QtGui.QLabel()
        self.label.setText(u"прямое направление")
        pbSave.setText(u"запомнить")
        pbPaste.setText(u"вставить")
        pbRevers.setText(u"в обратном")
        pbAdd.setText(u"добавить")
        pbIns.setText(u"вставить")
        pbDel.setText(u"удалить")
        hl = QtGui.QHBoxLayout()
        hl2 = QtGui.QHBoxLayout(self)
        vl = QtGui.QVBoxLayout()
        vl2 = QtGui.QVBoxLayout()
        vl.addWidget(self.label)
        vl.addWidget(self.twSpisOstanov)
        hl.addWidget(pbSave)
        hl.addWidget(pbPaste)
        hl.addWidget(pbRevers)
        vl.addLayout(hl)
        hl2.addLayout(vl)
        spacerItem = QtGui.QSpacerItem(20, 58, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        vl2.addItem(spacerItem)
        vl2.addWidget(pbAdd)
        vl2.addWidget(pbIns)
        vl2.addWidget(pbDel)
        spacerItem1 = QtGui.QSpacerItem(20, 28, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        vl2.addItem(spacerItem1)
        hl2.addLayout(vl2)

        self.connect(pbSave,QtCore.SIGNAL('clicked()'),self.saveMem)
        self.connect(pbPaste,QtCore.SIGNAL('clicked()'),self.pasteMem)
        self.connect(pbRevers,QtCore.SIGNAL('clicked()'),self.reversMem)
        self.connect(pbAdd,QtCore.SIGNAL('clicked()'),self.addOstanov)
        self.connect(pbIns,QtCore.SIGNAL('clicked()'),self.insOstanov)
        self.connect(pbDel,QtCore.SIGNAL('clicked()'),self.delOstanov)

    def saveMem(self):
        s = self.twSpisOstanov.selectedIndexes()
        self.mem = []
        for i in s:
            self.mem += [self.idostanov[i.row()]]
        self.emit(QtCore.SIGNAL("savemem"),self.mem)
        
    def pasteMem(self):
        sort = self.twSpisOstanov.currentRow()
        ln = len(self.mem)
        self.mydb.sql = '''UPDATE marshostanov SET sort=sort+%d
            WHERE sort>%d AND idmarsh=%d AND obrat =%d'''%(ln,sort,self.idmarsh,self.obrat)
        self.mydb.execsql()
        for id in self.mem:
            sort+=1
            self.mydb.sql = '''INSERT INTO marshostanov (idmarsh,idostanov,sort,obrat)
                VALUES (%d,%d,%d,%d)'''%(self.idmarsh,id,sort,self.obrat)
            self.mydb.execsql()
        self.viewSpis()

    def reversMem(self):
        print 4
        pass

    def viewSpis(self):
        self.mydb.sql = '''SELECT mo.id, o.name, o.id as idostanov
            FROM ostanov as o, marsh as m, marshostanov as mo
            WHERE o.id = mo.idostanov AND m.id = mo.idmarsh
                AND mo.obrat=%d AND m.id = %d AND mo.del = 0 AND m.del = 0
                AND o.del = 0
            ORDER BY mo.sort '''%(self.obrat,self.idmarsh)
        self.mydb.execsql()
        res = self.mydb.cursor.fetchall()
        n = self.mydb.cursor.rowcount
        self.twSpisOstanov.setRowCount(n)
        i = 0
        self.id = []
        self.idostanov = []
        self.name = []
        for row in res:
            self.id += [row["id"]]
            self.idostanov += [row["idostanov"]]
            self.name += [row["name"]]
            item = QtGui.QTableWidgetItem(row["name"])
            self.twSpisOstanov.setItem(i,0,item)
            i += 1
        self.twSpisOstanov.resizeColumnsToContents()

    def addOstanov(self):
        sort = self.twSpisOstanov.currentRow()
        self.mydb.sql = '''UPDATE marshostanov SET sort=sort+1
            WHERE sort>%d AND idmarsh=%d AND obrat =%d'''%(sort,self.idmarsh,self.obrat)
        self.mydb.execsql()

        self.mydb.sql = '''INSERT INTO marshostanov (idmarsh,idostanov,sort,obrat)
            VALUES (%d,%d,%d,%d)'''%(self.idmarsh,self.idostanov,sort+1,self.obrat)
        self.mydb.execsql()
        self.viewSpis()

    def insOstanov(self):
        sort = self.twSpisOstanov.currentRow()
        self.mydb.sql = '''UPDATE marshostanov SET sort=sort+1
            WHERE sort>=%d AND idmarsh=%d AND obrat =%d'''%(sort,self.idmarsh,self.obrat)
        self.mydb.execsql()

        self.mydb.sql = '''INSERT INTO marshostanov (idmarsh,idostanov,sort,obrat)
            VALUES (%d,%d,%d,%d)'''%(self.idmarsh,self.idostanov,sort,self.obrat)
        self.mydb.execsql()
        self.viewSpis()


    def delOstanov(self):
        sort = self.twSpisOstanov.currentRow()
        dlg = QtGui.QMessageBox(self)
        dlg.setWindowTitle(u'Удаление записи')
        dlg.setText(u'''Вы действительно хотите удалить остановку '%s?' '''% self.name[sort])
        #dlg.setInformativeText(u'Вы действительно хотите удалить запись?')
        dlg.setStandardButtons(QtGui.QMessageBox.Cancel | QtGui.QMessageBox.Ok );
        ok = dlg.exec_()
        if ok==QtGui.QMessageBox.Ok :
            #self.mydb.sql = '''DELETE FROM marsh WHERE id = %d'''%(self.id[self.currentRow()])
            self.mydb.sql = '''UPDATE marshostanov SET del=1
                WHERE id = %d'''% (self.id[sort])
            self.mydb.execsql()
            self.viewSpis()

class marshwostanovMain(QtGui.QDialog):
    '''класс формы работы с перекрестной таблицей остановок и маршрутов,
    '''
    def __init__(self, parent=None):
        QtGui.QDialog.__init__(self, parent)
        self.mem = []
        self.setupUi()
        self.viewSpis()
        self.initAct()
        

    def setupUi(self):
        self.setWindowTitle(u'Остановки на маршрутах')
        self.resize(681, 364)
        self.twMarsh = marshSpis()
        self.twOstanov = ostanovSpis()
        self.twMarshOstanov = widgOstanov()
        self.twMarshOstanov2 = widgOstanov()
        self.twMarshOstanov2.label.setText(u"обратное направление")
        self.twMarshOstanov2.obrat = 1
        label = QtGui.QLabel()
        label_2 = QtGui.QLabel()
        label_3 = QtGui.QLabel()
        label.setText(u"маршруты")
        label_2.setText(u"остановки на маршруте")
        label_3.setText(u"остановки")
        gl = QtGui.QGridLayout(self)
        hl = QtGui.QHBoxLayout()
        vl = QtGui.QVBoxLayout()
        vl2 = QtGui.QVBoxLayout()
        vl3 = QtGui.QVBoxLayout()
        vl.addWidget(label)
        vl.addWidget(self.twMarsh)
        vl2.addWidget(label_2)
        vl2.addWidget(self.twMarshOstanov)
        vl2.addWidget(self.twMarshOstanov2)
        vl3.addWidget(label_3)
        vl3.addWidget(self.twOstanov)
        hl.addLayout(vl)
        hl.addLayout(vl2)
        hl.addLayout(vl3)
        gl.addLayout(hl, 0, 0, 1, 1)


    def initAct(self):
        self.connect(self.twMarsh,QtCore.SIGNAL('itemSelectionChanged () '),self.rereadMarshOstanov)
        self.connect(self.twOstanov,QtCore.SIGNAL('itemSelectionChanged () '),self.rereadMarshOstanov)
        self.connect(self.twMarshOstanov,QtCore.SIGNAL('savemem'),self.savemem)
        self.connect(self.twMarshOstanov2,QtCore.SIGNAL('savemem'),self.savemem)
        
    def savemem(self,mem):
        self.mem = mem
        self.twMarshOstanov.mem = mem
        self.twMarshOstanov2.mem = mem

    def viewSpis(self):
        self.twMarsh.reread()
        self.twOstanov.reread()
        self.rereadMarshOstanov()

    def rereadMarshOstanov(self):
        self.twMarshOstanov.mem = self.mem
        self.twMarshOstanov2.mem = self.mem
        self.twMarshOstanov.idmarsh = self.twMarsh.id[self.twMarsh.currentRow()]
        self.twMarshOstanov.idostanov = self.twOstanov.id[self.twOstanov.currentRow()]
        self.twMarshOstanov.viewSpis()

        self.twMarshOstanov2.idmarsh = self.twMarsh.id[self.twMarsh.currentRow()]
        self.twMarshOstanov2.idostanov = self.twOstanov.id[self.twOstanov.currentRow()]
        self.twMarshOstanov2.viewSpis()

    def show(self):
        self.viewSpis()
        super(marshwostanovMain,self).show()

if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    qb = ostanovMain()
    qb.show()
    sys.exit(app.exec_())

