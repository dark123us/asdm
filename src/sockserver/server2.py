#!/usr/bin/python
# -*- coding: utf-8  -*-
import socket, asyncore
from decodedata import decodedata
from tilenames import lengthMetr
from db import db
import datetime

class serveraccept(asyncore.dispatcher):
    def __init__(self, ip, port, typepribor = 1, backlog = 200):
        asyncore.dispatcher.__init__(self)
        self.clientdict = []
        self.clientid = 0
        self.create_socket(socket.AF_INET,socket.SOCK_STREAM)
        self.set_reuse_addr()
        self.bind((ip,port))
        self.listen(backlog)

    def handle_accept(self):
        self.addclient()

    def resend(self,data,id):
        if data[0:4] == 'stop':
            for i in self.clientdict:
                i[1].close()
            print "stopping"
            self.close()

    def addclient(self):
        conn, addr = self.accept()
        print '--- Connect --- ',self.clientid, addr
        cl = client(conn, self.clientid, self.resend, self.delclient, addr)
        self.clientdict += [[self.clientid, cl]]
        self.clientid += 1

    def delclient(self, id, addr):
        k = 0
        print '--- Disconnect --- ',id, addr
        for i in self.clientdict:
            if i[0] == id:
                self.clientdict = self.clientdict[0:k]+self.clientdict[k+1:]
            k += 1

class client(asyncore.dispatcher):
    def __init__(self, conn, id, resend, delclient, addr, mysqldb = None):
        self.resend = resend
        self.delclient = delclient
        self.id = id
        self.addr = addr
        self.mysql = mysqldb
        if self.mysql != None:
            self.writesql = writeSqlData(self.mysql)
        asyncore.dispatcher.__init__(self, conn)
        self.buffer_read = ''
#        self.buffer_write = ''

    def handle_connect(self):
        pass

    def handle_read(self):
        read = self.recv(4096)
        print '%04i -->'%len(read), self.addr
        self.buffer_read += read
        if self.mysql != None:
            self.writesql.sendData(self.buffer_read)
            self.buffer_read = self.buffer_read [-self.writesql.ostatok:]#оставляем в буфере только остаток
        if self.buffer_read[0:4] == 'stop':
            self.resend('stop', self.id)
        elif self.buffer_read[0:3] == 'end':
            self.handle_close()

#    def writable(self):
#        return (len(self.buffer_write) > 0)
#
#    def handle_write(self):
#        sent = self.send(self.buffer_write)
#        # print '%04i <--'%sent
#        self.buffer_write = self.buffer_write[sent:]

    def handle_close(self):
        self.delclient(self.id, self.addr)
        self.close()

class dataPacket:
    def __init__(self):
        self.lat = 0 #координаты точки
        self.lon = 0
        self.nmbr = 0#номер прибора
        self.idlastpoint = 0#id в таблице pointlast
        self.idpoint = 0#id в таблице point для увеличения количества точек при стоянке
        self.point = 0#количество точек при стоянке
        self.datetimepribor = 0
    

class writeSqlData:
    def __init__(self, mysqldb = None):
        if db == None:
            self.mysql = db('localhost', 'asdm', 'root', '234912234912')
        else:
            self.mysql = mysqldb
        self.ostatok = 0 #сколько байт останется непрочитанными
        self.dec = decodedata()#класс расшифровки пакета
        self.data = dataPacket()
        self.noerror = [0,1,2,3,4,5]#коды ошибок, которые не будут учитываться и будут писаться как обычный пакет
    ''
    def sendData(self,data):
        lenghtM = [0,0]#массив для замера расстояний и углов
        l = self.dec.decode(data)#расшифровываем пакет
        self.ostatok = self.dec.lenost #получаем сколько байт осталось в конце пакета
        for i in l:#т.к. может быть несколько данных, перебираем все
            if len(i)==1:#если в данном пакете нет ошибок
                j = i[0]
                if self.data.idlastpoint == 0:#если это первый проход, то добавляем данные
                    self.data.nmbr = j['nprib']
                    self.data.lat = j['lat']
                    self.data.lon = j['lon']
                    self.data.datetimepribor = j['datetime']
                    self._insertTempData(j)
                    self._insertData(j)
                else:#иначе, если данные уже приходили
                    self._insertTempData(j)
                    self._insertData(j)
            else:#обработка пакета с ошибками
                self._insertError(i)
                print "------------=================--------------"
    ''
    def _initsql(self):
        self.sqlseldatalast = 'SELECT id FROM pointlast WHERE nmbr = %d'
        self.sqlinsdatalast = '''
                    INSERT INTO pointlast (lat, lon, nmbr, datetimepribor,
                        datetimeprogramm, sluj, sluj2, speed, angle, alt)
                    VALUES (%2.8f, %2.8f, %d, '%s',
                        '%s', %d, %d, %d, %d, %d)
                        '''
        self.sqlupddatalast = '''
                    UPDATE  pointlast SET lat = %2.8f, lon = %2.8f,
                        datetimepribor = '%s', datetimeprogramm = '%s', sluj = %d, sluj2 = %d,
                        speed = %d, angle=%d, alt=%d
                    WHERE  id = %d
                    '''
        self.sqlinsdata =  '''
                INSERT INTO point (lat, lon, nmbr, datetimepribor,
                    datetimeprogramm, sluj, sluj2, speed, angle, alt, npoint)
                VALUES (%2.8f, %2.8f, %d, '%s',
                    '%s', %d, %d, %d, %d, %d, %d)
                    '''
        self.sqlupddata = '''UPDATE  point SET npoint = %d WHERE  id = %d'''
        self.sqlinserr = '''
                INSERT INTO error (lat, lon, nmbr, datetimepribor,
                    datetimeprogramm, sluj, sluj2, speed, angle, alt, koderror, mess)
                VALUES (%2.8f, %2.8f, %d, '%s',
                    '%s', %d, %d, %d, %d, %d, %d, '%s')
                    '''
        #получить последнюю точку по прибору на указанную дату                    
        self.sqllastdata = '''
                SELECT * 
                FROM `point` 
                WHERE nmbr = 895 AND `datetimeprogramm` = (
                   SELECT max(datetimeprogramm)
                   FROM `point` 
                   WHERE nmbr = 895 AND datetimeprogramm <'2011-10-15') 
                '''
        
    def _insertTempData(self,packet,id=0):#сохраняем временную точку
        try:
            d = packet
            dt = datetime.datetime.now()
            if self.data.idlastpoint == 0:
                self.mysql.sql = self.sqlseldatalast % self.data.nmbr
                self.mysql.execsql()
                result_set = self.mysql.cursor.fetchall()
                for row in result_set:
                    self.data.idlastpoint = row["id"]
            #если в таблице с текущими данными прибор еще не добавлен (не найден его id) - создаем новую запись
            if self.data.idlastpoint == 0:
                sql = """ INSERT INTO pointlast (lat, lon, nmbr, datetimepribor,
                        datetimeprogramm, sluj, sluj2, speed, angle, alt)
                    VALUES (%2.8f, %2.8f, %d, '%s',
                        '%s', %d, %d, %d, %d, %d)
                    """%(d['lat'], d['lon'], d['nprib'], str(d['datetime']),
                        str(dt), d['sluj'], d['sluj2'],  d['speed'],  d['angle'],  d['alt'])
                self.mysql.execsql(sql)
                id = self.mysql.getlastid()
            else:
                #иначе подновляем запись
                self.mysql.sql = """ UPDATE  pointlast SET lat = %2.8f, lon = %2.8f,
                    datetimepribor = '%s', datetimeprogramm = '%s', sluj = %d, sluj2 = %d,
                    speed = %d, angle=%d, alt=%d
                    WHERE  id = %d"""%(d['lat'], d['lon'],
                    str(d['datetime']), str(dt), d['sluj'],d['sluj2'],
                    d['speed'], d['angle'], d['alt'], id)
                self.mysql.execsql()
            return id
        except Exception, e:
            print "readdatasock.readPacketShkiper.insertTempData ",e
    ''
    def _insertData(self,packet):
        try:
            print "insert ",packet
            d = packet
            dt = datetime.datetime.now()
            sql = """ INSERT INTO point (lat, lon, nmbr, datetimepribor,
                    datetimeprogramm, sluj, sluj2, speed, angle, alt, npoint)
                VALUES (%2.8f, %2.8f, %d, '%s',
                    '%s', %d, %d, %d, %d, %d, %d)
                """%(d['lat'], d['lon'], d['nprib'], str(d['datetime']),
                    str(dt), d['sluj'], d['sluj2'],  d['speed'],  d['angle'],  d['alt'], 0)
            #print self.mysql.sql
            self.mysql.execsql(sql)
            id2 = self.mysql.getlastid()
            return id2
        except Exception, e:
            print "readdatasock.readPacketShkiper.insertData ",e
    ''
    def _updateData(self,id2,npoint):
        sql = """ UPDATE  point SET npoint = %d
            WHERE  id = %d """%(npoint,id2)
        self.mysql.execsql(sql)
    ''
    def _insertError(self,packet):
        try:
            print "error ",packet
            data = packet[0]
            error = packet[1]
            dt = datetime.datetime.now()
            koderror = 0
            for i in error[0]:
                if i[0] == 1:
                    data ['datetime'] = datetime.datetime(2000, 1, 1, 0, 0)
                elif i[0] == 2:
                    data ['lat'] = -1
                    data ['lon'] = -1
                elif i[0] == 3:
                    data ['speed'] = -1
                elif i[0] == 4:
                    data ['angle'] = -1
                elif i[0] == 5:
                    data ['alt'] = -1
                elif i[0] == 6:
                    data ['crc16'] = -1
                koderror += 2**(i[0]-1)
            dt = datetime.datetime.now()
            sql = """ INSERT INTO error (lat, lon, nmbr, datetimepribor,
                    datetimeprogramm, sluj, sluj2, speed, angle, alt, koderror, mess)
                VALUES (%2.8f, %2.8f, %d, '%s',
                    '%s', %d, %d, %d, %d, %d, %d, '%s')
                """%(data['lat'], data['lon'], data['nprib'], str(data['datetime']),
                    str(dt), data['sluj'], data['sluj2'],  data['speed'],  data['angle'],
                    data['alt'], koderror, error[1])
            #print self.mysql.sql
            self.mysql.execsql(sql)
        except Exception, e:
            print "readdatasock.readPacketShkiper.insertError ",e
    ''

if __name__ == "__main__":
    serveraccept('',21001,200)
    #serveraccept('',21003,200)
    asyncore.loop()