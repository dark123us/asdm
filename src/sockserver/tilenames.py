#!/usr/bin/env python
# -*- coding: utf-8  -*-
#-------------------------------------------------------
# Translates between lat/long and the slippy-map tile
# numbering scheme
#
# http://wiki.openstreetmap.org/index.php/Slippy_map_tilenames
#
# Written by Oliver White, 2007
# This file is public-domain
#-------------------------------------------------------
from math import *

def numTiles(z):
  return(pow(2,z))

def sec(x):
  return(1/cos(x))

def latlon2relativeXY(lat,lon):
  x = (lon + 180) / 360
  y = (1 - log(tan(radians(lat)) + sec(radians(lat))) / pi) / 2
  return(x,y)

def latlon2xy(lat,lon,z):
  n = numTiles(z)
  x,y = latlon2relativeXY(lat,lon)
  return(n*x, n*y)

def tileXY(lat, lon, z):
  x,y = latlon2xy(lat,lon,z)
  return(int(x),int(y))

def xy2latlon(x,y,z):
  n = numTiles(z)
  relY = y / n
  lat = mercatorToLat(pi * (1 - 2 * relY))
  lon = -180.0 + 360.0 * x / n
  return(lat,lon)

def latEdges(y,z):
  n = numTiles(z)
  unit = 1 / n
  relY1 = y * unit
  relY2 = relY1 + unit
  lat1 = mercatorToLat(pi * (1 - 2 * relY1))
  lat2 = mercatorToLat(pi * (1 - 2 * relY2))
  return(lat1,lat2)

def lonEdges(x,z):
  n = numTiles(z)
  unit = 360 / n
  lon1 = -180 + x * unit
  lon2 = lon1 + unit
  return(lon1,lon2)

def tileEdges(x,y,z):
  lat1,lat2 = latEdges(y,z)
  lon1,lon2 = lonEdges(x,z)
  return((lat2, lon1, lat1, lon2)) # S,W,N,E

def mercatorToLat(mercatorY):
  return(degrees(atan(sinh(mercatorY))))

def tileSizePixels():
  return(256)

def tileLayerExt(layer):
  if(layer in ('oam')):
    return('jpg')
  return('png')

def tileLayerBase(layer):
  layers = { \
    "tah": "http://cassini.toolserver.org:8080/http://a.tile.openstreetmap.org/+http://toolserver.org/~cmarqu/hill/",
	#"tah": "http://tah.openstreetmap.org/Tiles/tile/",
    "oam": "http://oam1.hypercube.telascience.org/tiles/1.0.0/openaerialmap-900913/",
    "mapnik": "http://tile.openstreetmap.org/mapnik/"
    }
  return(layers[layer])

def tileURL(x,y,z,layer):
  return "%s%d/%d/%d.%s" % (tileLayerBase(layer),z,x,y,tileLayerExt(layer))


def lengthMetr(llat1,llong1,llat2,llong2):
    if llat1 == llat2 and llong1 == llong2:
        return (0,0)
    else:
        #pi - число pi, rad - радиус сферы (Земли)
        rad = 6372795

        try:
            #в радианах
            lat1 = llat1*pi/180.
            lat2 = llat2*pi/180.
            long1 = llong1*pi/180.
            long2 = llong2*pi/180.
        except Exception, e:
            print "01 - Error",e
            raise

        try:
            #косинусы и синусы широт и разницы долгот
            cl1 = cos(lat1)
            cl2 = cos(lat2)
            sl1 = sin(lat1)
            sl2 = sin(lat2)
            delta = long2 - long1
            cdelta = cos(delta)
            sdelta = sin(delta)
        except Exception, e:
            print "02 - Error",e
            raise

        try:
            #вычисления длины большого круга
            y = sqrt(pow(cl2*sdelta,2)+pow(cl1*sl2-sl1*cl2*cdelta,2))
            x = sl1*sl2+cl1*cl2*cdelta
            ad = atan2(y,x)
            dist = ad*rad
            #вычисление начального азимута
            x = (cl1*sl2) - (sl1*cl2*cdelta)
            y = sdelta*cl2
            z = degrees(atan(-y/x))
        except Exception, e:
            print "03 - Error",e
            print x,y
            raise

        try:
            if (x < 0):
                z = z+180.

            z2 = (z+180.) % 360. - 180.
            z2 = - radians(z2)
            anglerad2 = z2 - ((2*pi)*floor((z2/(2*pi))) )
            angledeg = (anglerad2*180.)/pi
        except Exception, e:
            print "04 - Error",e
            raise

        #print 'Distance >> %.0f' % dist, ' [meters]'
        #print 'Initial bearing >> ', angledeg, '[degrees]'
        return (dist,angledeg)

if __name__ == "__main__":
    for z in range(0,20):
        x,y = tileXY(51.14,33.0, z)
        s,w,n,e = tileEdges(x,y,z)
        print "%d: %d,%d --> %1.3f :: %1.3f, %1.3f :: %1.3f" % (z,x,y,s,n,w,e)
        #print "<img src='%s'><br>" % tileURL(x,y,z)
        x,y = tileXY(56.4,23.0, z)

        s,w,n,e = tileEdges(x,y,z)
        print "%d: %d,%d --> %1.3f :: %1.3f, %1.3f :: %1.3f" % (z,x,y,s,n,w,e)
        #print "<img src='%s'><br>" % tileURL(x,y,z)
    print tileEdges(1,0,1)
    print tileEdges(2,1,2)
    print lengthMetr(52.781065,27.515945,52.764032,27.514572)