#!/usr/bin/python
# -*- coding: utf-8  -*-
import socket
import threading
import sys
import time

class mainClient:
    def __init__(self):
        self.readConfig()


    def readConfig(self):
        ff = open('config')
        s = ff.readlines()
        for i in s:
            conf = i.split()
            if conf[0] == 'ip':
                self.ip = conf[1]
            if conf[0] == 'port':
                self.port = int(conf[1])

    def start(self,list):
        try:
            for l in list:
                if self.ip == '0':
                    self.ip = l
                if self.port == 0:
                    self.port = int(l)
                if l == '-i' or l == '--ip':
                    self.ip = '0'
                if l == '-p' or l == '--port':
                    self.port = 0
            self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.server.connect((self.ip, self.port))
            #self.channel, details = self.server.accept()
            data = 'client version 1.0'
            self.server.send(data)
            data = self.server.recv(1024)
            print data
            forward = '-1'
            for l in list:
                if l == '-s' or l == '--stop':
                    data = 'stop'
                    self.server.send(data)
                    data = self.server.recv(1024)
                    print data
                if l == '-m' or l == '--mysql':
                    data = 'mysql'
                    self.server.send(data)
                    data = self.server.recv(1024)
                    print data
                if l == '-sm' or l == '--stopmysql':
                    data = 'stopmysql'
                    self.server.send(data)
                    data = self.server.recv(1024)
                    print data
                if l == '-g' or l == '--get':
                    data = 'get'
                    self.server.send(data)
                    data = self.server.recv(1024)
                    print data
                if forward == '0':
                    forward = -1
                    data = 'forward %s'%l
                    self.server.send(data)
                    data = self.server.recv(1024)
                    print data
                if forward == '1':
                    forward = -1
                    data = 'stopforward %s'%l
                    self.server.send(data)
                    data = self.server.recv(1024)
                    print data
                if l == '-f' or l == '--forwarding':
                    forward = '0'
                if l == '-sf' or l == '--stopforward':
                    forward = '1'
            self.server.close()
        except Exception, e:
            print e

if __name__ == "__main__":
    list = sys.argv[1:]
    if len(list) == 0:
        print "'client --help' для получения более подробного описания "

    else:
        if len(list) == 1 and (list[0]=='--help' or list[0]=='-h'):
            print "Использование: client.py [параметр]"
            print " Параметры:"
            print "  -s, --stop                 остановить сервер"
            print "  -w, --widget               показать список потоков, Qt4"
            print "  -g, --get                  показать список потоков, terminal"
            print "  -i, --ip                   на какой ip посылать сигналы"
            print "  -p, --port                 на какой порт посылать сигналы"
            print "  -f <ip>, "
            print "     --forwarding <ip>       пересылать сигналы на ip:port"
            print "  -sf <ip>, "
            print "     --stopforward <ip>      остановить пересылку сигналов на ip:port"
            print "  -m, --mysql                сохранять данные в mysql"
            print "  -sm, --stopmysql           остановить сохранение данных в mysql"
        else:
            m = mainClient()
            m.start(list)
    sys.exit (0)	# Выход из программы.