#!/usr/bin/python
# -*- coding: utf-8  -*-
import socket
import threading
import sys
import time

class mainClient:
    def __init__(self):
        self.readConfig()

    def readConfig(self):
        ff = open('config')
        s = ff.readlines()
        for i in s:
            conf = i.split()
            if conf[0] == 'ip':
                self.ip = conf[1]
            if conf[0] == 'port':
                self.port = int(conf[1])
        self.port = 21001

    def start(self,list):
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server.connect((self.ip, self.port))
        data = 'client version 1.0'
        self.server.send(data)
        time.sleep(20)
        self.server.close()

if __name__ == "__main__":

    m = mainClient()
    m.start(list)
    sys.exit (0)	# Выход из программы.