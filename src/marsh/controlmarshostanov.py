#!/usr/bin/python
# -*- coding: utf-8  -*-
from PyQt4 import QtGui, QtCore
from ostanov import ostanov
from marsh import marsh
from marshostanov import marshostanov

class controlmarshostanov(QtGui.QDialog):
    def __init__(self, parent = None, mysql = None):
        QtGui.QDialog.__init__(self, parent)
        self.mrsh = marsh(self,mysql)
        self.ost = ostanov(self,mysql)
        self.mrshost1 = marshostanov(self,mysql,0)
        self.mrshost2 = marshostanov(self,mysql,1)
        self.initWidget()
        self.initAction()
        self.mrsh.redraw([0])
        self.ost.redraw([0])
        self.mrsh.list.setCurrentRow(0)
        self.ost.list.setCurrentRow(0)

    def initWidget(self):
        grid = QtGui.QGridLayout(self)
        spl1 = QtGui.QSplitter(self)
        spl2 = QtGui.QSplitter(QtCore.Qt.Vertical)
        spl2.addWidget(self.mrshost1)
        spl2.addWidget(self.mrshost2)
        spl1.addWidget(self.mrsh)
        spl1.addWidget(spl2)
        spl1.addWidget(self.ost)
        grid.addWidget(spl1)

    def initAction(self):
        self.connect(self.mrsh.list, QtCore.SIGNAL("itemSelectionChanged ()"),self.mrshChange)
        self.connect(self.ost.list, QtCore.SIGNAL("itemSelectionChanged ()"),self.ostChange)
        self.connect(self.mrshost1, QtCore.SIGNAL("copied()"),self.copyOstanov1)
        self.connect(self.mrshost2, QtCore.SIGNAL("copied()"),self.copyOstanov2)

    def mrshChange(self):
        if self.mrsh.list.count()>0:
            id = self.mrsh.datarow[self.mrsh.list.currentRow()]
            self.mrshost1.setMarsh(id)
            self.mrshost2.setMarsh(id)

    def ostChange(self):
        if self.ost.list.count()>0:
            id = self.ost.datarow[self.ost.list.currentRow()]
            self.mrshost1.setOstanov(id)
            self.mrshost2.setOstanov(id)

    def copyOstanov1(self):
        self.mrshost2.copy = self.mrshost1.copy

    def copyOstanov2(self):
        self.mrshost1.copy = self.mrshost2.copy

