#!/usr/bin/python
# -*- coding: utf-8  -*-
from PyQt4 import QtGui, QtCore
from spr.control import view, editWind

class modEditWind(editWind):
    def __init__(self, parent = None):
        editWind.__init__(self, parent)
        lbl1 = QtGui.QLabel(u'Время')
        self.ed1 = QtGui.QLineEdit()
        self.grid.addWidget(lbl1,0,0)
        self.grid.addWidget(self.ed1,0,1)
        self.grid.addWidget(self.btn1,1,0,1,2)

class marshostanov(view):
    def __init__(self, parent = None, mysql = None, obrat = 0):#obrat - в какую сторону направлен маршрут
        self.obrat = obrat
        self.initSql()
        view.__init__(self, parent, mysql)
        self.edit = modEditWind(self)
        self.connect(self.edit, QtCore.SIGNAL("accepted()"),self.saveTimeMarshOstanov)
        self.dataview = ["name","dtime"]
        self.copy = []

    def initSql(self):
        self.dataparam = ["id", "idostanov", "sort", "dtime", "name"]
        self.getDataSQL = '''SELECT mo.id, mo.idostanov, mo.sort, mo.dtime, o.name
            FROM marshostanov as mo, ostanov as o
            WHERE del = 0 AND mo.idostanov = o.id AND mo.idmarsh = %s AND mo.obrat = %d
            ORDER BY mo.sort'''%('%d',self.obrat)
        self.addDataSQL = '''INSERT INTO marshostanov (idmarsh, idostanov, sort, dtime, obrat)
                             VALUES (%s, %s, %s, 0, %d)'''%('%d', '%d','%d',self.obrat)
        self.timeDataSQL = '''UPDATE marshostanov SET dtime = %d WHERE id = %d'''
        self.insDataSQL = '''UPDATE marshostanov
                              SET sort = sort + 1
                              WHERE idmarsh = %s AND sort>= %s AND obrat = %d '''%('%d','%d',self.obrat)
        self.upSortDataSQL = '''UPDATE marshostanov
                              SET sort = sort + 1
                              WHERE idmarsh = %s AND sort = %s AND obrat = %d '''%('%d','%d',self.obrat)
        self.downSortDataSQL = '''UPDATE marshostanov
                              SET sort = sort - 1
                              WHERE idmarsh = %s AND sort = %s AND obrat = %d '''%('%d','%d',self.obrat)
        self.updSortDataSQL = '''UPDATE marshostanov SET sort = %d WHERE id = %d'''

        self.delsortDataSQL = '''UPDATE marshostanov
                              SET sort = sort - 1
                              WHERE idmarsh = %s AND sort>= %s AND obrat = %s '''%('%d','%d',self.obrat)
        self.delDataSQL ='''DELETE FROM marshostanov WHERE id = %d'''

    def initWidget(self):
        grid = QtGui.QGridLayout(self)
        self.list = QtGui.QListWidget(self)#основной виджет, куда выводим список данных
        self.list.setSelectionMode (QtGui.QAbstractItemView.ExtendedSelection)
        self.tool = QtGui.QToolBar(self)
        self.tool.setIconSize(QtCore.QSize(24,24))
        self.initAction()
        grid.addWidget(self.tool)
        grid.addWidget(self.list)

    def initAction(self):
        self.addDataAct = QtGui.QAction(u'Добавить',self)
        self.addDataAct.setIcon(QtGui.QIcon("../ico/dbplus2.png"))
        self.insDataAct = QtGui.QAction(u'Вставить',self)
        self.insDataAct.setIcon(QtGui.QIcon("../ico/db_update.png"))
        self.delDataAct = QtGui.QAction(u'Удалить',self)
        self.delDataAct.setIcon(QtGui.QIcon("../ico/dbmin2.png"))
        self.timeDataAct = QtGui.QAction(u'Время прибытия',self)
        #self.timeDataAct.setShortcut('Return')
        self.timeDataAct.setIcon(QtGui.QIcon("../ico/gnome-panel-clock.png"))
        self.upDataAct = QtGui.QAction(u'Выше',self)
        self.upDataAct.setIcon(QtGui.QIcon("../ico/1uparrow.png"))
        self.downDataAct = QtGui.QAction(u'Ниже',self)
        self.downDataAct.setIcon(QtGui.QIcon("../ico/1downarrow.png"))
        self.copyDataAct = QtGui.QAction(u'Скопировать',self)
        self.copyDataAct.setIcon(QtGui.QIcon("../ico/3floppy_unmount.png"))
        self.pasteDataAct = QtGui.QAction(u'Вставить',self)
        self.pasteDataAct.setIcon(QtGui.QIcon("../ico/paste.png"))
        self.reversDataAct = QtGui.QAction(u'Развернуть',self)
        self.reversDataAct.setIcon(QtGui.QIcon("../ico/arrow_cycle.png"))

        self.connect(self.addDataAct, QtCore.SIGNAL("triggered()"),self.addMarshOstanov)
        self.connect(self.insDataAct, QtCore.SIGNAL("triggered()"),self.insMarshOstanov)
        self.connect(self.delDataAct, QtCore.SIGNAL("triggered()"),self.delMarshOstanov)
        self.connect(self.timeDataAct, QtCore.SIGNAL("triggered()"),self.timeMarshOstanov)
        self.connect(self.upDataAct, QtCore.SIGNAL("triggered()"),self.upMarshOstanov)
        self.connect(self.downDataAct, QtCore.SIGNAL("triggered()"),self.downMarshOstanov)
        self.connect(self.reversDataAct, QtCore.SIGNAL("triggered()"),self.reversMarshOstanov)
        self.connect(self.copyDataAct, QtCore.SIGNAL("triggered()"),self.copyMarshOstanov)
        self.connect(self.pasteDataAct, QtCore.SIGNAL("triggered()"),self.pasteMarshOstanov)
        
        self.tool.addAction(self.addDataAct)
        self.tool.addAction(self.insDataAct)
        self.tool.addAction(self.delDataAct)
        self.tool.addAction(self.timeDataAct)
        self.tool.addAction(self.upDataAct)
        self.tool.addAction(self.downDataAct)
        self.tool.addAction(self.copyDataAct)
        self.tool.addAction(self.pasteDataAct)
        self.tool.addAction(self.reversDataAct)

    def setMarsh(self,id):
        self.idmarsh = id
        self.redraw([id])

    def setOstanov(self,id):
        self.idostanov = id

    def addMarshOstanov(self):
        datasave = [self.idmarsh,self.idostanov,self.list.count()]
        self._addData(datasave)
        self.redraw([self.idmarsh])

    def insMarshOstanov(self):
        r = self.list.selectedIndexes()[0].row()
        self.mysql.execsql(self.insDataSQL%(self.idmarsh,r))
        datasave = [self.idmarsh,self.idostanov,r]
        self._addData(datasave)
        self.redraw([self.idmarsh])

    def delMarshOstanov(self):
        r = []
        for i in self.list.selectedIndexes():
            r += [i.row()]
        r.reverse()
        for i in r:
            self.mysql.execsql(self.delsortDataSQL%(self.idmarsh,i))
            id = self.datarow[i]
            self._delData(id)
        self.redraw([self.idmarsh])

    def timeMarshOstanov(self):
        r = self.list.selectedIndexes()[0].row()
        id = self.datarow[r]
        self.edit.ed1.setText(str(self.data[id]["dtime"]))
        self.edit.setModal(1)
        self.edit.show()

    def saveTimeMarshOstanov(self):
        r = self.list.selectedIndexes()[0].row()
        id = self.datarow[r]
        try:
            self.mysql.execsql(self.timeDataSQL%(int(self.edit.ed1.text()),id))
        except Exception,e:
            print e
        self.redraw([self.idmarsh])

    def upMarshOstanov(self):
        r = []
        r1 = self.list.selectedIndexes()[0].row()
        for i in self.list.selectedIndexes():
            r += [i.row()]
        r.sort()
        #print r
        for i in r:
            id = self.datarow[i]
            sort = self.data[id]["sort"]
            if sort>0:
                sql = self.upSortDataSQL%(self.idmarsh,sort-1)
                #print sql
                self.mysql.execsql(sql)
                sql = self.updSortDataSQL%(sort-1,id)
                #print sql
                self.mysql.execsql(sql)
        self.redraw([self.idmarsh])
        if r1-1>0:
            self.list.setCurrentRow(r1-1)
        else:
            self.list.setCurrentRow(r1)

    def downMarshOstanov(self):
        r = []
        r1 = self.list.selectedIndexes()[0].row()
        for i in self.list.selectedIndexes():
            r += [i.row()]
        r.reverse()
        #print r
        for i in r:
            id = self.datarow[i]
            sort = self.data[id]["sort"]
            if sort<self.list.count()-1:
                sql = self.downSortDataSQL%(self.idmarsh,sort+1)
                #print sql
                self.mysql.execsql(sql)
                sql = self.updSortDataSQL%(sort+1,id)
                #print sql
                self.mysql.execsql(sql)
        self.redraw([self.idmarsh])
        if r1+1 < self.list.count():
            self.list.setCurrentRow(r1+1)
        else:
            self.list.setCurrentRow(r1)

    def reversMarshOstanov(self):
        l = len(self.datarow)
        for i in range(l):
            id = self.datarow[i]
            sort = l - 1 - self.data[id]["sort"]
            self.mysql.execsql(self.updSortDataSQL%(sort,id))
        self.redraw([self.idmarsh])

    def copyMarshOstanov(self):
        self.copy = []
        for i in self.list.selectedIndexes():
            id = self.datarow[i.row()]
            idostanov = self.data[id]["idostanov"]
            self.copy += [idostanov]
        self.emit(QtCore.SIGNAL("copied()"))

    def pasteMarshOstanov(self):
        n = self.list.count()
        for i in self.copy:
            datasave = [self.idmarsh,i,n]
            self._addData(datasave)
            n += 1
        self.redraw([self.idmarsh])


