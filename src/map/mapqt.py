# -*- coding: utf-8 -*-

from PyQt4 import QtGui, QtCore
from sprmap.modelmap import modelmap
from sprmap import tilenames

class mapModelQt:
    '''Класс карты на технологии Qt.
    В частности здесь будут все расчеты по выводы карты, ее координатах и др. данных
    '''
    
    def __init__(self):
        self.path = '/home/dark123us/osm/tiles2/'#где храним тайлы
        self.xyt = self.initTilesXY()#храним [x,y,x1,y1] для каждого зума карты
        self.zoom = 12 #начальный уровень зума
        self.x = -26216 #смещение правого края карты
        self.y = -36311 #смещение верхнего края карты
        self.tileimage = []#углы загруженных тайлов x,y,тип картинки - 0 нет, 1 - есть [x,y,тип]
        self.mode = (256, 256) #размеры экрана
        self.tileangle = [0,0,0,0]#углы загруженных тайлов

    def initTilesXY(self):
        #начальная загрузка всех углов для наблюдаемой области карты, для всех зумов
        self.xyt = []
        for z in range(1,21):#перебор зумов
            x,y = tilenames.tileXY(56.4,23.0, z)
            x1,y1 = tilenames.tileXY(51.14,33.0, z)
            self.xyt += [[x,y,x1+1,y1+1]]
        return self.xyt

    def loadtile(self):
        #перенос старых изображений на новую сетку
        #создаем массив на количество загружаемых тайлов
        mas = []
        for i in range(self.tileangle[0],self.tileangle[2]):
            mas2 = []
            for j in range(self.tileangle[1],self.tileangle[3]):
                mas2 += [[i,j,0,0]]
            mas += [mas2]
        #проход по всем загруженным тайлам
        #если не вписываются - выбрасываем из массива
        for i in self.tileimage:
            x = i[0]- self.tileangle[0]
            y = i[1]- self.tileangle[1]
            if (x>=0 and y>=0 and i[0]<self.tileangle[2] and
                    i[1]<self.tileangle[3] and i[2]):#i[2] - 1 - тайл существует, 0 - тайл пустой
                #если есть ставим отметку в массив- такой тайл есть
                mas[x][y][2] = 1
                mas[x][y][3] = i[3]#переносим копию изображения
        return mas

    def calcScreenTile(self):
        #получаем углы тайлов
        xt1 = self.xyt[self.zoom][0]
        yt1 = self.xyt[self.zoom][1]
        xt2 = self.xyt[self.zoom][2]
        yt2 = self.xyt[self.zoom][3]
        #получаем разницу между тайлами
        dxt = xt2 - xt1 + 1
        dyt = yt2 - yt1 + 1
        #если тайлов меньше чем размер экрана
        #то эти тайлы центрируем на экране
        dxt256 = dxt*256
        dyt256 = dyt*256
        if dxt256<=self.mode[0]:
            self.x = (self.mode[0]-dxt256)/2
        if dyt256<=self.mode[1]:
            self.y = (self.mode[1]-dyt256)/2
        #если же тайлов больше, чем размер экрана
        #а в предудыщем не хватало тайлов на экран, то отсчет карты в угол экрана
        if dxt256 > self.mode[0]:
            if self.x > 0:
                self.x = 0
        if dyt256 > self.mode[1]:
            if self.y > 0:
                self.y = 0
        #получаем номера тайлов для вывода на экран
        xt1a = xt1-(self.x/256)-2#-2 - чтобы при движении карты не было рывков
        yt1a = yt1-(self.y/256)-2
        xt2a = xt1a + self.mode[0]/256+4#+2 тайла за пределы экрна
        yt2a = yt1a + self.mode[1]/256+4
        #если разница в пикселях меньше размеров экрана,
        #то экранные тайлы равны всем тайлам
        if dxt256 <= self.mode[0]:
            xt1a = xt1
            xt2a = xt2
        if dyt256 <= self.mode[1]:
            yt1a = yt1
            yt2a = yt2
        #если номера экранных тайлов выходят за границы, то присвоить границы
        if xt1a < xt1: xt1a = xt1
        if yt1a < yt1: yt1a = yt1
        if xt2a > xt2: xt2a = xt2
        if yt2a > yt2: yt2a = yt2
        #получаем отсчет для экранных тайлов
        self.xs = (xt1a - xt1) * 256
        self.ys = (yt1a - yt1) * 256
        #возвращаем экранные тайлы
        self.tileangle = [xt1a,yt1a,xt2a,yt2a]
        return self.tileangle

class mapViewQt(mapModelQt, QtGui.QWidget):
    '''Класс карты, здесь будем использовать вывод на виджет'''
    def __init__(self,parent):
        '''Инициализация и описание используемых переменных'''
        self.img = QtGui.QImage()
        mapModelQt.__init__(self)
        QtGui.QWidget.__init__(self,parent)
        self.pathnoimage = './map/'
        self.noimage = []
        self.pathimage = '/home/dark123us/osm/tiles2'        
        self.mode = (self.width(), self.height())
        self.resizeImage(self.img,QtCore.QSize(self.width(), self.height()))
        
        self.loadnotile()
        self.loadtile()
        self.update()

    def paintEvent(self, event):
        x = self.xs+self.x
        y = self.ys+self.y
        if not (-768 < x < -128 and -768 < y < -128):
            self.loadtile()
            x = self.xs+self.x
            y = self.ys+self.y
        painter = QtGui.QPainter()
        painter.begin(self)
        painter.drawImage(QtCore.QPoint(x, y), self.img)
        painter.end()        
        
    def resizeEvent(self, event):
        self.mode = (self.width(), self.height())
        
        if self.width()+512 > self.img.width() or self.height()+512 > self.img.height():
            self.mode = (self.width(), self.height())
            #self.resizeImage(self.img, QtCore.QSize(self.width(), self.height()))
            self.loadtile()
            self.update()
#        self.update()
        QtGui.QWidget.resizeEvent(self, event)
        
    def resizeImage(self, image, newSize):
        if image.size() == newSize:
            return
        newImage = QtGui.QImage(newSize, QtGui.QImage.Format_RGB32)
        newImage.fill(QtGui.qRgb(255, 255, 255))
        painter = QtGui.QPainter()
        painter.begin(newImage)
        painter.drawImage(QtCore.QPoint(0, 0), image)
        painter.end()
        self.img = newImage
    
    def loadtile(self):
        xold = self.tileangle[0]
        yold = self.tileangle[1]
        self.calcScreenTile()
        mas = mapModelQt.loadtile(self)
        #проходим по массиву - дополняем недостающие массивы
        self.tileimage = []
        x1 = self.tileangle[0]
        y1 = self.tileangle[1]
        x = self.tileangle[2]
        y = self.tileangle[3]
        self.resizeImage(self.img, QtCore.QSize((x-x1)*256, (y-y1)*256))

        newImage = QtGui.QImage(self.img.size(), QtGui.QImage.Format_RGB32)
        #newImage.fill(QtGui.qRgb(255, 255, 255))        
        painter = QtGui.QPainter()
        painter.begin(newImage)
        painter.drawImage(QtCore.QPoint((xold-x1) * 256, (yold-y1)*256), self.img)
        for i in mas:
            for j in i:
                if j[2] == 0:
                    x = j[0]
                    y = j[1]
                    x = (x-x1)*256
                    y = (y-y1)*256
                    map = "%s/%d/%d/%d.png"%(self.pathimage,self.zoom+1,j[0],j[1])
                    a = QtGui.QImage()
                    if a.load(map):
                        j[3] = a
                        j[2] = 1
                        painter.drawImage(QtCore.QPoint(x, y), a)
                    else:
                        j[3] = self.noimage[self.zoom]
                        j[2] = 0
                        painter.drawImage(QtCore.QPoint(x, y), self.noimage[self.zoom])
                self.tileimage += [j]
        painter.end()
        self.img = newImage
        return self.tileimage

    def loadnotile(self):
        for i in range (1,21):
            map = ("%s%d.png"%(self.pathnoimage,i))
            a = QtGui.QImage()
            if not a.load(map):
                print "notloading %s"%map
            self.noimage += [a]
    
class mapControlQt(mapViewQt):
    '''Класс карты, здесь будем реагировать на нажатия мыши, клавиатуры, внешние события'''
    
    def __init__(self,parent):
        '''Инициализация и описание используемых переменных'''
        mapViewQt.__init__(self,parent)

    def mousePressEvent(self, event):
        self.beginMoveMap(event.pos())
    
    def mouseMoveEvent(self, event):
        self.moveMap(event.pos())
        
    def wheelEvent(self, event):
        if event.delta() > 0:
            self.zoomUpMap()
        if event.delta() < 0:
            self.zoomDownMap()
        
    
    def beginMoveMap(self,eventpos):
        self.xm = eventpos.x()#смещение от угла экрана
        self.ym = eventpos.y()
        self.x1 = self.x #старые координаты для проверки необходимости подгрузки
        self.y1 = self.y #при прохождении расстояния 256
    ''
    def moveMap(self,eventpos):
        #если угол карты остается меньше угла экрана, то двигаем карту
        xm1 = eventpos.x()
        ym1 = eventpos.y()
        dxm = xm1 - self.xm
        dym = ym1 - self.ym
        if self.x1 + dxm < 0:
            self.x = self.x1 + dxm
        if self.y1 + dym < 0:
            self.y = self.y1 + dym
        self.update()
    ''

    def zoomUpMap(self):#приближение
        #увеличиваем зум
        if self.zoom < 17:
            self.zoom += 1
            if self.xyt[self.zoom-1][0] != self.xyt[self.zoom][0]*2:
                self.x += 128
            if self.xyt[self.zoom-1][1] != self.xyt[self.zoom][1]*2:
                self.y += 128
            x = - self.mode[0]/2 #eventpos[0] - self.mode[0]/2
            y = - self.mode[1]/2#eventpos[1] - self.mode[1]/2
            self.x = (self.x)*2 + x
            self.y = (self.y)*2 + y
            self.loadtile()
            self.update()
    ''
    def zoomDownMap(self):#приближение
        #увеличиваем зум
        if self.zoom >0:
            self.zoom -= 1
            if self.xyt[self.zoom+1][0] != self.xyt[self.zoom][0]*2:
                self.x -= 128
            if self.xyt[self.zoom+1][1] != self.xyt[self.zoom][1]*2:
                self.y -= 128

            x = - self.mode[0]/2 #eventpos[0] - self.mode[0]/2
            y = - self.mode[1]/2#eventpos[1] - self.mode[1]/2
            self.x = (self.x - x)/2
            self.y = (self.y - y)/2
            #if self.getChange()>0:
            #    self.createpng()
            
            self.loadtile()
            self.update()
    
class mapQt(QtGui.QMdiSubWindow):
    '''Класс - справочник, для вывода в окне или с другими виджетами карты'''
    
    def __init__(self, parent = None):
        QtGui.QMdiSubWindow.__init__(self, parent)
        self.setWindowTitle(u'Карта')        
        self.setMinimumSize(256,256)
        self.map = mapControlQt(parent)
        self.initWidget()
                    
    def initWidget(self):
        widg = QtGui.QWidget()
        lh = QtGui.QHBoxLayout()
        lh.addWidget(self.map)
        widg.setLayout(lh)
        self.setWidget(widg)
        
        
    