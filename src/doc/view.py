#!/usr/bin/python
# -*- coding: utf-8  -*-
from model import model
from PyQt4 import QtGui, QtCore
from datetime import datetime, date

class view(model,QtGui.QMdiSubWindow):
    def __init__(self, parent = None, mysql = None):
        QtGui.QMdiSubWindow.__init__(self, parent)
        #self.model = model(mysql)
        model.__init__(self,mysql)
        self.initWidget()
    
    def initWidget(self):
        widg = QtGui.QWidget()
        lv1 = QtGui.QVBoxLayout()
        lh1 = QtGui.QHBoxLayout()
        lbNmbr = QtGui.QLabel(u'Номер документа')
        self.edNmbr = QtGui.QLineEdit()
        lbDate = QtGui.QLabel(u'Дата документа')
        self.edDate = QtGui.QDateEdit()
        lh1.addWidget(lbNmbr)
        lh1.addWidget(self.edNmbr)
        lh1.addWidget(lbDate )
        lh1.addWidget(self.edDate)
        lh1.addStretch()
        self.bb = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Save| QtGui.QDialogButtonBox.Cancel)
        lv1.addLayout(lh1)
        lv1.addStretch()
        lv1.addWidget(self.bb)
        gr = QtGui.QGridLayout()
        gr.addLayout(lv1,0,0)
        widg.setLayout(gr)
        self.setWidget(widg)

    def new(self):
        self._getSqlParam()
        self._new()
        self.data["id"] = 0
        self.edNmbr.setText('0')
        self.edDate.setDate(QtCore.QDate(date.today()))

