#!/usr/bin/python
# -*- coding: utf-8  -*-
from db import db

#ver 1.0
class model:#абстрактный класс документов
    def __init__(self,mysql=None):
        if mysql == None: #если не передали указатель, то конектимся к базе
            self.mysql = db()
        else:
            self.mysql = mysql
        self.tabledoc = 'doctable'#имя таблицы в базе данных для документа
        self.tabletable = 'doctabletable'#имя таблицы в базе данных для таблицы документа
        self._new()
        self.dataparam = ["id", "NomerDoc", "DataDoc", "Autor", "Comment", "Status"]
        #self.selectwhereid = "id = %d" #по чем производится поиск
        self.insertvalues = ["%d", "'%s'", "%d", "'%s'", "%d"]#строка подставляемых параметров, согласно dataparam за вычетом id, т.к. он инкрементный
        self._getSqlParam()

    def _getSqlParam(self):
        sqlparam = ''
        for dataparam in self.dataparam:
            sqlparam = "%s, %s"%(sqlparam, dataparam)
        sqlparam = sqlparam [2:]#убираем начальные запятую и пробел
        #self.getDataSQL = "SELECT %s FROM %s WHERE %s"%(sqlparam, self.tabledoc, self.selectwhereid)
        self.getDataSQL = "SELECT %s FROM %s WHERE id = %d"%(sqlparam, self.tabledoc, self.data["id"])
        sqlparam = sqlparam [4:]#убираем из списка параметров id - он создается автоматом
        values = ''
        for value in self.insertvalues:
            values = "%s, %s"%(values, value)
        values = values [2:]
        self.addDataSQL = "INSERT INTO %s (%s) VALUES (%s)"%(self.tabledoc, sqlparam, values )
        sqlparam = ''
        for i in range(len(self.insertvalues)):
            sqlparam = "%s, %s = %s"%(sqlparam, self.dataparam[i+1], self.insertvalues[i])
        sqlparam = sqlparam[2:]
        #self.editDataSQL = '''UPDATE %s SET %s WHERE %s'''%(self.tabledoc, sqlparam, self.selectwhereid)
        self.editDataSQL = '''UPDATE %s SET %s WHERE id = %d'''%(self.tabledoc, sqlparam, self.data["id"])
        #self.delDataSQL ='''UPDATE %s SET status = 1 WHERE %s'''%(self.tabledoc, self.selectwhereid)
        #self.provDataSQL ='''UPDATE %s SET status = 2 WHERE %s'''%(self.tabledoc, self.selectwhereid)
        #self.delProvDataSQL ='''UPDATE %s SET status = 0 WHERE %s'''%(self.tabledoc, self.selectwhereid)
        self.updStatusDataSQL ='''UPDATE %s SET status = %s WHERE id = %d'''%(self.tabledoc, "%d", self.data["id"])

    def _getData(self,data):#tupledata - содержит данные для вывода запроса
        #загрузить данные
        self.datarow = []
        try:
            if len(self.getDataSQL)>0:
                try:
                    self.mysql.execsql(self.getDataSQL%tuple(data))
                except TypeError:#на случай если нет входных параметров
                    self.mysql.execsql(self.getDataSQL)
                result_set = self.mysql.cursor.fetchall()
                self.data = {} #словарь со всеми полями
                self.datarow = []
                for row in result_set:
                    tmpdata = {}
                    for i in self.dataparam:
                        tmpdata[i] = row[i] #проход по полям
                    self.data[row["id"]] = tmpdata#сохраняем в данных
                    self.datarow += [row["id"]]
        except Exception,e:
            print e

    def _crTable(self):
        sql = '''
            CREATE TABLE  %s (
                id INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
                NomerDoc INT NOT NULL DEFAULT  '0',
                DataDoc DATE NOT NULL DEFAULT  '2011-01-01',
                Autor INT NOT NULL DEFAULT  '0',
                Status INT NOT NULL ,
                Comment VARCHAR( 100 ) NOT NULL DEFAULT  '',
                INDEX (NomerDoc)
                ) ENGINE = MYISAM
            '''%self.tabledoc
        try:
            self.mysql.execsql(sql)
        except Exception, e:
            print e

    def _addData(self, data):
        self.mysql.execsql(self.addDataSQL%(tuple(data)))

    def _editData(self, data):
        self.mysql.execsql(self.editDataSQL%(tuple(data)))

    def _delData(self, id):
        self.mysql.execsql(self.delDataSQL%(id))#id

    def _getLastId(self):
        return self.mysql.getlastid()

    def _new(self):
        self.data = {}#словарь для основных элементов документа
        self.data["id"] = 0 #id документа, на случай если будет дана возможность одинаковых номеров
        self.data["NomerDoc"] = 0 #номер документа
        self.data["DataDoc"] = '2011-01-01' #дата документа
        self.data["Autor"] = 0 #кто создал документ
        self.data["Comment"] = 'Admin' #кто создал документ
        self.data["Status"] = 0 #0 - сохранен по умолчанию, 1 - удален, 2 - проведен
