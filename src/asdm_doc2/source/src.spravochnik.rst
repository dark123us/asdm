spravochnik Package
===================

:mod:`spravochnik` Package
--------------------------

.. automodule:: src.spravochnik
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`control` Module
---------------------

.. automodule:: src.spravochnik.control
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`model` Module
-------------------

.. automodule:: src.spravochnik.model
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`test` Module
------------------

.. automodule:: src.spravochnik.test
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`view` Module
------------------

.. automodule:: src.spravochnik.view
    :members:
    :undoc-members:
    :show-inheritance:

Subpackages
-----------

.. toctree::

    src.spravochnik.bus
    src.spravochnik.holiday
    src.spravochnik.kartarange

