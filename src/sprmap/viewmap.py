#!/usr/bin/python
# -*- coding: utf-8  -*-
from modelmap import modelmap
import pygame
from pygame.locals import *

class viewmap(modelmap):
    def __init__(self):
        modelmap.__init__(self)
        pygame.init()
        pygame.display.set_caption('Карта')
        self.initScreen()
        self.pathnoimage = './map/'
        self.noimage = []
        self.pathimage = '/home/dark123us/osm/tiles2'

    def initScreen(self):
        self.screen = pygame.display.set_mode(self.mode,RESIZABLE,32)
        self.screen.fill((255,255,255,0))
        self.mapsurf = pygame.Surface(self.mode)

    def draw(self):
        self.screen.blit(self.mapsurf,(0,0))

    def redraw(self):
        self.mapsurf.fill((255,255,255,0))
        for image in self.tileimage:
            x = (image[0] - self.xyt[self.zoom][0])*256 + self.x
            y = (image[1] - self.xyt[self.zoom][1])*256 + self.y
            self.mapsurf.blit(image[3],(x,y))

    def loadtile(self):
        self.calcScreenTile()
        mas = modelmap.loadtile(self)
        #проходим по массиву - дополняем недостающие массивы
        self.tileimage = []
        for i in mas:
            for j in i:
                if j[2] == 0:
                    try:
                        map = "%s/%d/%d/%d.png"%(self.pathimage,self.zoom+1,j[0],j[1])
                        j[3] = pygame.image.load(map).convert()
                        j[2] = 1
                    except Exception, e:
                        j[3] = self.noimage[self.zoom]
                        j[2] = 0
                        print e
                self.tileimage += [j]
        return self.tileimage

    def loadnotile(self):
        for i in range (1,21):
            map = ("%s%d.png"%(self.pathnoimage,i))
            self.noimage += [pygame.image.load(map).convert()]

    def run(self):
        while self.runflag:
            for event in pygame.event.get():
                if event.type == QUIT: self.runflag = 0
            pygame.display.update()
        pygame.quit()