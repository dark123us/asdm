#!/usr/bin/python
# -*- coding: utf-8  -*-
import MySQLdb
import config as c

class db():
    def __init__(self, host= c.localhost,user = c.user,passwd = c.passwd,
                database = c.database, charset = "utf8", use_unicode = True):
        self.sql=''
        self.cursor = ''
        self.conn = ''
        self.connected = False

        self.host = host
        self.user = user
        self.passwd = passwd
        self.database = database
        self.charset = charset
        self.use_unicode = use_unicode

        self.connect()

    def connect(self):
        try:
            self.conn = MySQLdb.connect (host = self.host , user = self.user , passwd = self.passwd ,
                db = self.database, charset = self.charset, use_unicode = self.use_unicode)
            self.cursor = self.conn.cursor (MySQLdb.cursors.DictCursor)
            self.connected = True
        except MySQLdb.Error, e:
            print "Error %d: %s" % (e.args[0], e.args[1])

    def execsql(self,sql=''):
        try:
            if (sql != ''):
                self.sql = sql
            self.cursor.execute (self.sql)
        except MySQLdb.Error, e:
            try:
                print "Error %d: %s" % (e.args[0], e.args[1])
            except:
                print e
                self.connected = False
            self.disconnect()
    def disconnect(self):
        try:
            self.cursor.close ()
            self.conn.close ()
            self.connected = False
        except MySQLdb.Error, e:
            try:
                print "Error %d: %s" % (e.args[0], e.args[1])
            except:
                print e
                self.connected = False