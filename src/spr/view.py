#!/usr/bin/python
# -*- coding: utf-8  -*-
from model import model
from PyQt4 import QtGui, QtCore

class view(QtGui.QWidget, model):
    '''Класс отображения информации в виде списка
    
    Параметры инициализации:
        * parent - ссылка от главного окна для виджетов (QtGui.QWidget.__init__(self, parent))
        * mysql - ссылка на :py:class:`db.db`
    Переменные:
        * dataview - список ключей полей которые необходимо отображать
        * list - экземпляр класса QtGui.QListWidget
        '''
        
    def __init__(self, parent = None, mysql = None):
        QtGui.QWidget.__init__(self, parent)
        model.__init__(self, mysql)
        self.dataview = []#поля, которые будут отображаться
        #self.getDataSQL = []
        self.initWidget()
        self.redraw([0])

    def initWidget(self):
        "Инициализация виджета"
        
        self.list = QtGui.QListWidget(self)#основной виджет, куда выводим список данных
        grid = QtGui.QGridLayout(self)
        grid.addWidget(self.list)

    def redraw(self,data):#единственный метод, только отрисовка, с перечитыванием данных
        "Перерисовка виджета списка с перечитыванием данных"
        
        self.list.clear()
        self._getData(data)
        for id in self.datarow:
            s = self.itemlist(id)
            self.list.addItem("%s"%s)
            
    def itemlist(self,id):
        s = ''
        for j in self.dataview:
            try:
                s = "%s %d"%(s,str(self.data[id][j]))
            except:
                s = "%s %s"%(s,self.data[id][j])
        return s
#############################################################################
class viewcombo(QtGui.QComboBox,model):
    '''Класс отображения информации в виде комбобокса
    
    Параметры инициализации:
        * parent - ссылка от главного окна для виджетов (QtGui.QComboBox.__init__(self, parent))
        * mysql - ссылка на :py:class:`db.db`
        '''

    def __init__(self, parent=None, mysql=None):
        QtGui.QComboBox.__init__(self, parent)
        model.__init__(self, mysql)
        self.setEditable(False)
        self.setMaxVisibleItems(10)
        self.selectid = 0

    def redraw(self,data = [],idselect = 0):#единственный метод, только отрисовка, с перечитыванием данных; idselect - какой id выделить
        "Перерисовка виджета, перерасчет данных. idselect - какой id выделить"
        
        self.clear()
        self._getData(data)
        selectdata = -1
        k = 0 #счетчик, чтобы узнать какую строку выделить
        for id in self.datarow:#проходим по id
            if idselect == id:#проверяем на совпадение id и id
                selectdata = k #ставим метку, в конце на эту строку поместим указатель
            self.addItem("%s"%self.itemcombo(id))
            k += 1
        self.setCurrentIndex(selectdata)
    
    def itemcombo(self,id):
        "Порядок отрисовки информации"
        
        s = ''
        for j in self.dataview:#проходим по массиву, который будем отображать
            try:
                s = "%s %s"%(s,self.data[id][j])#если параметр числовой, то будет глюк и тогда занесем с переформатированием
            except:
                s = "%s %d"%(s,str(self.data[id][j]))
        return s
###############################################################################
class viewtable(model,QtGui.QTableWidget):
    '''Класс отображения информации в виде таблицы
    
    Параметры инициализации:
        * parent - ссылка от главного окна для виджетов (QtGui.QComboBox.__init__(self, parent))
        * mysql - ссылка на :py:class:`db.db`

    Для работы с классом необходимо определить запрос вывода данных список ключей
    для словаря, список ключей для вывода в таблице, список имен заголовков колонок. Пример::
        
        def initSql(self):
            self.dataview = ["name", "put"]
            self.headername = [u"Название", u"В путевых"]
            self.dataparam = ["name", "put", "idkarta"]
            self.getDataSQL = '
                SELECT v.id, k.name, k.put, k.id as idkarta
                FROM karta as k, vodgrkartasort as v
                WHERE v.idkarta = k.id AND v.brigada = %d AND k.del = 0
                GROUP BY v.sort'
        
    Если необходимо определяются запросы на добавление, изменение и удаление 
    элементов (класс :py:class:`spr.model`)
        '''

    def __init__(self, parent=None, mysql=None):
        model.__init__(self, mysql)
        QtGui.QTableWidget.__init__(self, parent)
        #self.table = QtGui.QTableWidget(self)
        self.setEditTriggers(QtGui.QAbstractItemView.EditTriggers(0))

    def redraw(self,data=[]):#
        '''Отрисовка, с перечитыванием данных'''
        
        self._getData(data)
        self.setRowCount(0)
        self.setRowCount(len(self.datarow))
        self.setColumnCount(len(self.dataview))
        self.setHorizontalHeaderLabels(self.headername)
        row = 0
        for id in self.datarow:
            col = 0
            for j in self.dataview:
                dr = 1
                try:
                    s = "%d"%(str(self.data[id][j]))
                except KeyError:
                    dr = 0
                except:
                    try:
                        s = "%s"%(self.data[id][j])
                    except:
                        #print id, j
                        #print self.data
                        #print self.data[id][j]
                        raise
                if dr:
                    self.itemtable(row,col,s)
                col += 1
            row += 1
        self.resizeColumnsToContents()
        self.resizeRowsToContents ()
        self.setCurrentCell(0,0)

    def itemtable(self,row,col,s):
        item = QtGui.QTableWidgetItem("%s"%s)
        self.setItem(row,col,item)
###############################################################################
class viewtable2(model,QtGui.QWidget):
    '''Класс отображения информации в виде таблицы
    
    Параметры инициализации:
        * parent - ссылка от главного окна для виджетов (QtGui.QComboBox.__init__(self, parent))
        * mysql - ссылка на :py:class:`db.db`

    Для работы с классом необходимо определить запрос вывода данных список ключей
    для словаря, список ключей для вывода в таблице, список имен заголовков колонок. Пример::
        
        def initSql(self):
            self.dataview = ["name", "put"]
            self.headername = [u"Название", u"В путевых"]
            self.dataparam = ["name", "put", "idkarta"]
            self.getDataSQL = '
                SELECT v.id, k.name, k.put, k.id as idkarta
                FROM karta as k, vodgrkartasort as v
                WHERE v.idkarta = k.id AND v.brigada = %d AND k.del = 0
                GROUP BY v.sort'
        
    Если необходимо определяются запросы на добавление, изменение и удаление 
    элементов (класс :py:class:`spr.model`)
        '''

    def __init__(self, parent=None, mysql=None):
        model.__init__(self, mysql)
        QtGui.QTableWidget.__init__(self, parent)
        self.initWidget()
        self.table.setEditTriggers(QtGui.QAbstractItemView.EditTriggers(0))
        #self.table = QtGui.QTableWidget(self)

    def initWidget(self):
        "Инициализация виджета"
        
        self.table = QtGui.QTableWidget(self)#основной виджет, куда выводим список данных
        self.table.setEditTriggers(QtGui.QAbstractItemView.EditTriggers(0))
        grid = QtGui.QGridLayout(self)
        grid.addWidget(self.table)

    def redraw(self,data=[]):#
        '''Отрисовка, с перечитыванием данных'''
        
        self._getData(data)
        self.table.setRowCount(0)
        self.table.setRowCount(len(self.datarow))
        self.table.setColumnCount(len(self.dataview))
        self.table.setHorizontalHeaderLabels(self.headername)
        row = 0
        for id in self.datarow:
            col = 0
            for j in self.dataview:
                dr = 1
                try:
                    s = "%d"%(str(self.data[id][j]))
                except KeyError:
                    dr = 0
                except:
                    try:
                        s = "%s"%(self.data[id][j])
                    except:
                        #print id, j
                        #print self.data
                        #print self.data[id][j]
                        raise
                if dr:
                    self.itemtable(row,col,s)
                col += 1
            row += 1
        self.table.resizeColumnsToContents()
        self.table.resizeRowsToContents ()
        self.table.setCurrentCell(0,0)

    def itemtable(self,row,col,s):
        item = QtGui.QTableWidgetItem("%s"%s)
        self.table.setItem(row,col,item)        
###########################################################################
class viewfreezetable(QtGui.QTableView):
    '''Визуализация таблицы с замороженными столбцами
    
    Переменные:
        * mdl - экземпляр класса QtGui.QStandardItemModel который отвечает за отрисовку ячеек (все руки не доходят сделать свой)
        * mdls - экземпляр класса QtGui.QItemSelectionModel(self.mdl) отвечающий за выделение ячеек
        * frozenTableView - замороженная таблица
        * freezeColumnCount - количество замороженных столбцов
        * freeColumnCount - количество других столбцов
        * headername - заголовки таблицы
        * datarow - список ключей для словаря datas (переопределяется в наследнике)
        * dataview - список ключей словаря datas, которые будут выводится в таблице
        '''    
    
    def __init__(self, parent=None, mysql=None):
        self.initValues()
        self.mysql = mysql
        QtGui.QTableView.__init__(self,parent)
        self.setEditTriggers(QtGui.QAbstractItemView.EditTriggers(0))
        self.initClasses()
        self._initWidget()
        self.initWidget()
    
    def initValues(self):
        '''Функция создания переменных, дабы класс не завалить в исключение из-за их отсутствия'''
        try:#self.freezeColumnCount количество замороженных колонок, определяем в наследнике
            a = self.freezeColumnCount 
        except: self.freezeColumnCount = 0
        try:#self.freeColumnCount количество остальных колонок, определяем в наследнике
            a = self.freeColumnCount 
        except: self.freeColumnCount = 1
#        try:#self.widthFreezeColumn список ширин фиксированных столбцов
#            a = self.widthFreezeColumn 
#        except: self.widthFreezeColumn = []
        try:#self.headername список заголовков колонок 
            a = self.headername 
        except: self.headername = []
    
    def initClasses(self):
        self.mdl = QtGui.QStandardItemModel()
        self.setModel(self.mdl)
        self.mdls = QtGui.QItemSelectionModel(self.mdl)
        self.setSelectionModel(self.mdls)
        self.frozenTableView = QtGui.QTableView(self)
        #self.horizontalHeader().setStretchLastSection(True)
        
    def _initWidget(self):
        self.frozenTableView.setModel(self.model())
        self.frozenTableView.setFocusPolicy(QtCore.Qt.NoFocus)
        self.frozenTableView.verticalHeader().hide()
        self.frozenTableView.horizontalHeader().setResizeMode(QtGui.QHeaderView.Fixed)
        self.viewport().stackUnder(self.frozenTableView)
        self.frozenTableView.setStyleSheet("QTableView { border: none;"
                                      "background-color: #8EDE21;"
                                      "selection-background-color: #999}")
        self.frozenTableView.setSelectionModel(self.selectionModel())
        self.frozenTableView.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.frozenTableView.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.setHorizontalScrollMode(self.ScrollPerPixel)
        self.setVerticalScrollMode(self.ScrollPerPixel)
        self.frozenTableView.setVerticalScrollMode(self.ScrollPerPixel)
        
        self.frozenTableView.show()

    def initWidget(self):
        "Перерисовка виджета (всей таблицы)"
        for col in range(self.freezeColumnCount,self.model().columnCount()):
             self.frozenTableView.setColumnHidden(col, True)
        for w in range(0,self.freezeColumnCount):
            self.frozenTableView.setColumnWidth(w, self.columnWidth(w))
        for w in range(0,self.model().rowCount()):
            self.frozenTableView.setRowHeight(w, self.rowHeight(w))
        
        self.updateFrozenTableGeometry()
        
    
    def redraw(self,data=[]):
        "Отрисовка, с перечитыванием данных"
        self._getData(data)
        self.mdl.setColumnCount(self.freezeColumnCount)
        self.mdl.setHorizontalHeaderLabels(self.headername)
        self.mdl.setRowCount(len(self.datarow))
        self.mdl.setColumnCount(self.freeColumnCount+self.freezeColumnCount)
        self.initWidget()
#        for w in range(len(self.widthFreezeColumn )):
#            self.setColumnWidth(w,self.widthFreezeColumn[w])
        row = 0
        for id in self.datarow:
            col = 0
            for j in self.dataview:
                dr = 1
                try:
                    s = "%d"%(str(self.datas[id][j]))
                except KeyError:
                    dr = 0
                except:
                    try:
                        s = "%s"%(self.datas[id][j])
                    except:
                        #print id, j
                        #№print self.datas
                        #print self.datas[id][j]
                        raise
                if dr:
                    self.itemtable(row,col,s)
                col += 1
            row += 1
        self.resizeColumnsToContents()
        self.resizeRowsToContents()
        self.updateFrozenTableGeometry()

    def itemtable(self,row,col,s):
        "Способ отрисовки ячейки"
        item = QtGui.QStandardItem("%s"%s)
        self.mdl.setItem(row,col,item)

    def updateSectionWidth(self,logicalIndex,i,newSize):
        if logicalIndex <= self.freezeColumnCount:
             self.frozenTableView.setColumnWidth(logicalIndex,newSize)
             self.updateFrozenTableGeometry()

    def updateSectionHeight(self, logicalIndex, i, newSize):
        self.frozenTableView.setRowHeight(logicalIndex, newSize)

    def resizeEvent(self,event):
        super(QtGui.QTableView, self).resizeEvent(event)
        self.updateFrozenTableGeometry()

    def moveCursor(self, cursorAction, modifiers):
        current = QtGui.QTableView.moveCursor(self, cursorAction, modifiers)
        if (cursorAction == self.MoveLeft and current.column()>0
                and  self.visualRect(current).topLeft().x() < self.frozenTableView.columnWidth(0) ):
            newValue = self.horizontalScrollBar().value() + self.visualRect(current).topLeft().x() - self.frozenTableView.columnWidth(0)
            self.horizontalScrollBar().setValue(newValue)
        return current

    def scrollTo(self, index, hint):
        if (index.column()>0):
            QtGui.QTableView.scrollTo(self, index, hint)

    def updateFrozenTableGeometry(self):
        size = 0
        for i in range(0,self.freezeColumnCount):
            size += self.columnWidth(i)
        self.frozenTableView.setGeometry(self.verticalHeader().width() + self.frameWidth(),
            self.frameWidth(),
            size,
            self.viewport().height()+self.horizontalHeader().height())

    def _getData(self,data):#tupledata - содержит данные для вывода запроса
        #загрузить данные
        self.datarow = []
        try:
            if len(self.getDataSQL)>0:
                try:
                    self.mysql.execsql(self.getDataSQL%tuple(data))
                except TypeError:#на случай если нет входных параметров
                    self.mysql.execsql(self.getDataSQL)
                result_set = self.mysql.cursor.fetchall()
                self.data = {} #словарь со всеми полями
                self.datarow = []
                for row in result_set:
                    tmpdata = {}
                    for i in self.dataparam:
                        tmpdata[i] = row[i] #проход по полям
                    self.datas[row["id"]] = tmpdata#сохраняем в данных
                    self.datarow += [row["id"]]
        except Exception,e:
            print e