#!/usr/bin/python
# -*- coding: utf-8  -*-
from PyQt4 import QtGui, QtCore
from spr.control import controltable
from spr.view import viewcombo, view

class brigadacombo(viewcombo):
    def __init__(self, parent = None, mysql = None):
        self.initSQL()
        viewcombo.__init__(self,parent,mysql)
    
    def initSQL(self):
        self.dataparam = ['name']
        self.dataview = ['name']
        self.getDataSQL = '''
                SELECT id, name
                FROM brigada
                WHERE del = 0
                ORDER BY name
            '''
    
    def itemcombo(self,id):
        s = "%s"%(self.data[id]["name"])
        return s
        
class brigadalist(view):
    def __init__(self, parent = None, mysql = None):
        self.initSQL()
        viewcombo.__init__(self,parent,mysql)
    
    def initSQL(self):
        self.dataparam = ['name']
        self.dataview = ['name']
        self.getDataSQL = '''
                SELECT id, name
                FROM brigada
                WHERE del = 0
                ORDER BY name
            '''
    
    def itemlist(self,id):
        s = "%s"%(self.data[id]["name"])
        return s        
#-------------------------------------------------------------------------
class editWind(QtGui.QDialog):#вспомогательное диалоговое окно
    def __init__(self, parent = None, mysql = None):
        QtGui.QDialog.__init__(self, parent)
        self.setWindowTitle(u'Изменить бригаду')
        self.grid = QtGui.QGridLayout(self)
        self.btn1 = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok|
                                      QtGui.QDialogButtonBox.Cancel)
        self.connect(self.btn1 , QtCore.SIGNAL("accepted()"), self.accept)
        self.connect(self.btn1 , QtCore.SIGNAL("rejected()"), self.reject)
        self.initWidget()

    def initWidget(self):
        lbl1 = QtGui.QLabel(u'Бригада')        
        self.ed1 = QtGui.QTextEdit()
        self.grid.addWidget(lbl1,0,0)
        self.grid.addWidget(self.ed1,0,1)
        self.grid.addWidget(self.btn1,1,0,1,2)
#-------------------------------------------------------------------------
class sprbrigada(QtGui.QMdiSubWindow):
    def __init__(self, parent = None, mysql = None):
        QtGui.QMdiSubWindow.__init__(self, parent)
        self.setWindowTitle(u'Список карточек для графика')        
        self.list = brigadalist(parent, mysql)
        self.table.brigada = 1
        self.table.redraw()
        
        self.brigada = []
        for i in range(4):
            self.brigada += [str(i+1)]

        self.initWidget()
        self.initAction()        
        
            
    def initWidget(self):
        widg = QtGui.QWidget()
        grd = QtGui.QGridLayout()
        vh = QtGui.QVBoxLayout()
        self.brigadacombo = QtGui.QComboBox()        
        self.brigadacombo.addItems(self.brigada)
        tool = QtGui.QToolBar()
        tool.addAction(self.table.addDataAct)
        tool.addAction(self.table.editDataAct)
        tool.addAction(self.table.upDataAct)
        tool.addAction(self.table.downDataAct)
        tool.addWidget(self.brigadacombo)
        tool.addAction(self.table.delDataAct)
        tool.setIconSize(QtCore.QSize(48,48))
        grd.addWidget(tool)
        grd.addWidget(self.table)
        vh.addLayout(grd)
        widg.setLayout(vh)
        self.setWidget(widg)
    
    def initAction(self):
        self.connect(self.brigadacombo, QtCore.SIGNAL("currentIndexChanged(int)"),self.changeBrigada)
        
    def changeBrigada(self, i):
        self.table.brigada = i+1
        self.table.redraw()
