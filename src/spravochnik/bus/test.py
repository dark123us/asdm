#!/usr/bin/python
# -*- coding: utf-8  -*-
from control import buscontrol
from PyQt4 import QtGui, QtCore

class formbus(QtGui.QDialog):
    def __init__(self, parent = None, mysql = None):
        QtGui.QDialog.__init__(self, parent)
        self.busc = buscontrol(self, mysql)
        grid = QtGui.QGridLayout(self)
        tool = QtGui.QToolBar()
        tool.addAction(self.busc.addDataAct)
        tool.addAction(self.busc.editDataAct)
        tool.addAction(self.busc.delDataAct)
        grid.addWidget(tool)
        grid.addWidget(self.busc)