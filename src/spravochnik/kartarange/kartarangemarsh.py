#!/usr/bin/python
# -*- coding: utf-8  -*-
from spr.control import control
from spr.view import viewcombo
from PyQt4 import QtGui, QtCore
from datetime import timedelta, datetime
import copy

class _cmbMarsh(viewcombo):
    def __init__(self, parent = None, mysql = None):
        self.initSql()
        viewcombo.__init__(self, parent, mysql)
        self.dataview = ["name"]

    def initSql(self):
        self.dataparam = ["id", "name"]
        self.getDataSQL = '''SELECT id, name
                FROM marshrut
                WHERE del = 0
                ORDER BY name'''

class _cmbOstanov(viewcombo):
    def __init__(self, parent = None, mysql = None):
        self.initSql()
        viewcombo.__init__(self, parent, mysql)
        self.dataview = ["name"]

    def initSql(self):
        self.dataparam = ["id", "name","idostanov", "dtime"]
        self.getDataSQL = '''SELECT mo.id as id, o.name as name, o.id as idostanov, mo.dtime as dtime
            FROM ostanov as o, marshrut as m, marshostanov as mo
            WHERE o.id = mo.idostanov AND m.id = mo.idmarsh
                AND mo.obrat=%d AND m.id = %d AND m.del = 0
                AND o.del = 0
            ORDER BY mo.sort '''

class marshAddDialog(QtGui.QDialog):
    '''Диалоговая форма для добавление и изменения маршрутов в карточку'''
    def __init__(self, parent=None,mysql=None):
        QtGui.QDialog.__init__(self, parent)
        self.cmbMarsh = _cmbMarsh(self,mysql)
        self.cmbBeginOstanov = _cmbOstanov(self,mysql)
        self.cmbEndOstanov = _cmbOstanov(self,mysql)
        self.cmbMarsh.redraw()
        self.cmbBeginOstanov.redraw([0,self.cmbMarsh.datarow[0]])
        self.cmbEndOstanov.redraw([0,self.cmbMarsh.datarow[0]])
        self.cmbEndOstanov.setCurrentIndex(len(self.cmbEndOstanov.datarow)-1)#выбираем конечную остановку в комбо
        self.initWidget()
        
    def initWidget(self):
        self.resize(300,250)
        gl = QtGui.QGridLayout(self)
        buttonBox = QtGui.QDialogButtonBox()
        buttonBox.setOrientation(QtCore.Qt.Horizontal)
        buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Save)
        l1 = QtGui.QLabel(u'Название маршрута')
        l2 = QtGui.QLabel(u'Первая остановка')
        l3 = QtGui.QLabel(u'Конечная остановка')
        l4 = QtGui.QLabel(u'Время отправления')
        l5 = QtGui.QLabel(u'Время прибытия')
        self.te = QtGui.QTimeEdit()
        self.l55 = QtGui.QLabel(u'0 минут')
        self.chb = QtGui.QCheckBox(u'Прямой маршрут')
        self.chb2 = QtGui.QCheckBox(u'С обратным маршрутом')
        self.chbnight = QtGui.QCheckBox(u'после 24:00')
        self.chb2.setVisible(0)
        gl.addWidget(l1,0,0)
        gl.addWidget(self.cmbMarsh,0,1)
        gl.addWidget(l2,1,0)
        gl.addWidget(self.cmbBeginOstanov,1,1)
        gl.addWidget(l3,2,0)
        gl.addWidget(self.cmbEndOstanov,2,1)
        gl.addWidget(l4,3,0)
        gl.addWidget(self.te,3,1)
        gl.addWidget(l5,4,0)
        gl.addWidget(self.l55,4,1)
        gl.addWidget(self.chb,5,0)
        gl.addWidget(self.chb2,5,1)
        gl.addWidget(self.chbnight,6,0)
        gl.addWidget(buttonBox,7,0,1,2)
        self.chb2.setCheckState(2)
        self.connect(buttonBox, QtCore.SIGNAL("accepted()"), self.accept)
        self.connect(buttonBox, QtCore.SIGNAL("rejected()"), self.reject)

        self.connect(self.cmbBeginOstanov, QtCore.SIGNAL("currentIndexChanged(int)"),self.changeOstanov)
        self.connect(self.cmbEndOstanov, QtCore.SIGNAL("currentIndexChanged(int)"),self.changeOstanov)
        self.connect(self.cmbMarsh,QtCore.SIGNAL("currentIndexChanged(int)"),self.changeMarsh)
        self.connect(self.chb,QtCore.SIGNAL("stateChanged(int)"),self.changeState)

    def changeState(self):
        self.changeMarsh(self.cmbMarsh.currentIndex())

    def changeMarsh(self,index):
        obrat = 1-self.chb.checkState()/2 #получаем состояния чека - прямой или обратный маршрут
        self.cmbBeginOstanov.redraw([obrat,self.cmbMarsh.datarow[index]])#отрисовываем остановки в соответсвии с новыми параметрами
        self.cmbEndOstanov.redraw([obrat,self.cmbMarsh.datarow[index]])
        self.cmbEndOstanov.setCurrentIndex(len(self.cmbEndOstanov.datarow)-1)#выбираем конечную остановку в комбо

    def changeOstanov(self):#при изменении остановки просто считаем новое время
        ei = self.cmbEndOstanov.currentIndex()
        id1 = self.cmbEndOstanov.datarow[ei]
        bi = self.cmbBeginOstanov.currentIndex()
        id2 = self.cmbBeginOstanov.datarow[bi]
        ed = self.cmbEndOstanov.data[id1]["dtime"]
        bd = self.cmbBeginOstanov.data[id2]["dtime"]
        if (bi>=0) & (ei>=0):
            #if (len(bd)>0)&(len(ed)>0):
            self.l55.setText("%s %s"%(str(ed - bd),u"минут"))

class kartarangemarsh(control):
    def __init__(self, parent = None, mysql = None):
        self.initSql()
        control.__init__(self, parent, mysql)
        self.edit = marshAddDialog(self,mysql)
        self.connect(self.edit, QtCore.SIGNAL("accepted()"),self.save)
        self.dataview = ["mname","o1name","o2name","begintime","day","dtime", "endtime"]
        self.datacopy = []
        self.datarowcopy = []
        self.initAction()

    def initSql(self):
        self.dataparam = ["id", "begintime", "mname", "o1name", "o2name", "obrat", "day", "mid", "o1id", "o2id", "endtime"]
        self.getDataSQL = '''
            SELECT km.id, mo.id as o1id, mo2.id as o2id, m.name as mname, m.id as mid,
                o.name as o1name, o2.name as o2name, km.begintime, mo2.dtime-mo.dtime as endtime,
                mo.obrat as obrat, km.day
            FROM marshrut as m, kartarange as k, ostanov as o, ostanov as o2,
                kartamarsh as km, marshostanov as mo, marshostanov as mo2
            WHERE k.id =%d AND k.del = 0 AND m.del = 0 AND km.del=0 AND
                km.idkartarange = k.id AND km.idmarsh = m.id AND
                km.idbegin = mo.id AND km.idend = mo2.id AND
                mo.idostanov = o.id AND mo2.idostanov = o2.id
            ORDER BY km.day, km.begintime'''
        self.addDataSQL = '''INSERT INTO kartamarsh (idkartarange, idmarsh, begintime, idbegin, idend, day)
            VALUES (%d, %d, '%s', %d, %d, %d)'''
        self.editDataSQL = '''UPDATE kartamarsh SET idmarsh = %d, begintime = '%s',
            idbegin = %d, idend = %d, day =%d  WHERE id = %d'''
        self.delDataSQL ='''UPDATE kartamarsh  SET del=1 WHERE id = %d'''

    def initAction(self):
        self.copyDataAct = QtGui.QAction(u'Скопировать',self)
        self.copyDataAct.setIcon(QtGui.QIcon("../../ico/3floppy_unmount.png"))
        self.pasteDataAct = QtGui.QAction(u'Вставить',self)
        self.pasteDataAct.setIcon(QtGui.QIcon("../../ico/paste.png"))
        self.tool.addAction(self.copyDataAct)
        self.tool.addAction(self.pasteDataAct)
        self.connect(self.copyDataAct, QtCore.SIGNAL("triggered()"),self.copy)
        self.connect(self.pasteDataAct, QtCore.SIGNAL("triggered()"),self.paste)

    def redraw(self,data):#в data получаем список из одного элемента id
        self.idkartarange = data[0] #сохраняем его для добавления или редактирования
        self.list.clear()
        self._getData(data)
        for id in self.datarow:
            self.dataview = ["mname","o1name","o2name","begintime","dtime"]
            l = self.data[id]
            s = "%s [%s-%s][%s-%s]"%(l["mname"],str(l["begintime"]),str(l["begintime"]+timedelta(0,l["endtime"]*60)),l["o1name"],l["o2name"])
            self.list.addItem("%s"%s)
        #control.redraw(self,data)

    def addDataControl(self):
        if self.idkartarange > 0:
            if self.edit.cmbMarsh.currentIndex()<0:
                self.edit.cmbMarsh.setCurrentIndex(0)
            self.edit.cmbBeginOstanov.setCurrentIndex(0)
            self.edit.cmbEndOstanov.setCurrentIndex(len(self.edit.cmbEndOstanov.datarow)-1)#выбираем конечную остановку в комбо
            self.edit.te.setTime(QtCore.QTime(QtCore.QDateTime(datetime.now()).time()))
            self.edit.chb.setCheckState(2)
            self.edit.chbnight.setCheckState(0)
            control.addDataControl(self)

    def editDataControl(self):
        if self.list.count()>0:
            r = self.list.currentRow()
            id = self.datarow[r]
            mid = self.data[id]["mid"]
            o1id = self.data[id]["o1id"]
            o2id = self.data[id]["o2id"]
            obrat = self.data[id]["obrat"]
            te = self.data[id]["begintime"]
            day = self.data[id]["day"]
            self.edit.chb.setCheckState((1 - obrat)*2)
            self.edit.cmbMarsh.redraw([],mid)
            self.edit.cmbBeginOstanov.redraw([obrat,mid],o1id)
            self.edit.cmbEndOstanov.redraw([obrat,mid],o2id)
            self.edit.te.setTime(QtCore.QTime.fromString(str(te),"h:mm:ss"))
            self.edit.chbnight.setCheckState(int(day*2))
            control.editDataControl(self)

    def delDataControl(self):
        control.delDataControl(self)
        self.redraw([self.idkartarange])

    def save(self):
        r = self.list.currentRow()
        mid = self.edit.cmbMarsh.datarow[self.edit.cmbMarsh.currentIndex()]
        o1id = self.edit.cmbBeginOstanov.datarow[self.edit.cmbBeginOstanov.currentIndex()]
        o2id = self.edit.cmbEndOstanov.datarow[self.edit.cmbEndOstanov.currentIndex()]
        day = self.edit.chbnight.checkState()/2
        te = self.edit.te.time().toPyTime()
        if self.idkartarange>0:
            if self.mode == 1:
                self._addData([self.idkartarange,mid,str(te),o1id,o2id,day])
            elif self.mode == 2:
                id = self.datarow[r]
                self._editData([mid,str(te),o1id,o2id,day,id])
            self.redraw([self.idkartarange])

    def copy(self):
        self.datacopy = copy.deepcopy(self.data)
        self.datarowcopy = copy.deepcopy(self.datarow)

    def paste(self):
        for i in self.datarowcopy:
            data = self.datacopy[i]
            mid = data["mid"]
            o1id = data["o1id"]
            o2id = data["o2id"]
            te = data["begintime"]
            day = data["day"]
            self._addData([self.idkartarange,mid,str(te),o1id,o2id,day])
        self.redraw([self.idkartarange])

