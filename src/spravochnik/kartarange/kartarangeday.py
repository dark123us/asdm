#!/usr/bin/python
# -*- coding: utf-8  -*-
from spr.control import control
from PyQt4 import QtGui, QtCore
from datetime import date

class editWind(QtGui.QDialog):#вспомогательное диалоговое окно
    def __init__(self, parent = None):
        QtGui.QDialog.__init__(self, parent)
        grid = QtGui.QGridLayout(self)
        lb1 = QtGui.QLabel(u'Начало периода')
        lb2 = QtGui.QLabel(u'Конец периода')
        self.date1 = QtGui.QDateEdit()
        self.date1.setCalendarPopup(True)
        self.date2 = QtGui.QDateEdit()
        self.date2.setCalendarPopup(True)

        self.btn1 = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok|
                                      QtGui.QDialogButtonBox.Cancel)
        self.connect(self.btn1 , QtCore.SIGNAL("accepted()"), self.accept)
        self.connect(self.btn1 , QtCore.SIGNAL("rejected()"), self.reject)
        grid.addWidget(lb1,0,0)
        grid.addWidget(self.date1,0,1)
        grid.addWidget(lb2,1,0)
        grid.addWidget(self.date2,1,1)
        grid.addWidget(self.btn1,2,0,1,2)

class kartarangeday(control):
    def __init__(self, parent = None, mysql = None):
        self.initSql()
        control.__init__(self, parent, mysql)
        self.edit = editWind(self)
        self.connect(self.edit, QtCore.SIGNAL("accepted()"),self.save)
        self.dataview = ["begindate", "enddate"]

    def initSql(self):
        self.dataparam = ["id", "begindate", "enddate"]
        self.getDataSQL = '''SELECT id, begin as begindate, end as enddate
                FROM kartarangeday
                WHERE del = 0 AND idkartarange =%d
                ORDER BY begin'''
        self.addDataSQL = '''INSERT INTO kartarangeday  (idkartarange, begin, end)
            VALUES (%d,'%s','%s')'''
        self.editDataSQL = '''UPDATE kartarangeday SET begin = '%s', end = '%s'
            WHERE id=%d'''
        self.delDataSQL ='''UPDATE kartarangeday SET del=1 WHERE id = %d'''
        self.updateTempDataSQL = '''UPDATE kartarangeday SET idkartarange = %d WHERE idkartarange = 0'''

    def redraw(self,data):#в data получаем список из одного элемента id
        self.idkartarange = data[0] #сохраняем его для добавления или редактирования
        self.list.clear()
        self._getData(data)
        for i in self.datarow:
            s = []
            for j in self.dataview:
                s += [str(self.data[i][j])]
            self.list.addItem("[%s --- %s]"%(s[0],s[1]))

    def addDataControl(self):
        self.edit.date1.setDate(QtCore.QDate(date.today()))
        self.edit.date2.setDate(QtCore.QDate(date.today()))
        control.addDataControl(self)

    def editDataControl(self):
        r = self.list.currentRow()
        self.edit.date1.setDate(QtCore.QDate(self.data[self.datarow[r]][self.dataview[0]]))
        self.edit.date2.setDate(QtCore.QDate(self.data[self.datarow[r]][self.dataview[1]]))
        control.editDataControl(self)

    def updateTemp(self,id):#обновить все idkartarange =0 на id
        self.mysql.execsql(self.updateTempDataSQL%(id))

    def save(self):
        r = self.list.currentRow()
        date1 = self.edit.date1.date().toPyDate()
        date2 = self.edit.date2.date().toPyDate()
        if self.mode == 1:
            self._addData([self.idkartarange,str(date1),str(date2)])
        elif self.mode == 2:
            id = self.datarow[r]
            self._editData([str(date1),str(date2),id])
        self.redraw([self.idkartarange])
