#!/usr/bin/python
# -*- coding: utf-8  -*-
from view import view
from PyQt4 import QtGui, QtCore

class editWind(QtGui.QDialog):
    def __init__(self, parent = None):
        QtGui.QDialog.__init__(self, parent)
        grid = QtGui.QGridLayout(self)
        lbl1 = QtGui.QLabel(u'Название')
        self.ed1 = QtGui.QLineEdit()
        self.btn1 = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok|
                                      QtGui.QDialogButtonBox.Cancel)

        grid.addWidget(lbl1,0,0)
        grid.addWidget(self.ed1,0,1)
        grid.addWidget(self.btn1,1,0,1,2)

class control(view):
    def __init__(self, parent = None, mysql = None):
        view.__init__(self, parent, mysql)
        self.edit = editWind(self)
        self.mode = 0 #режим добавления или изменения
        self.initConnect()

    def initConnect(self):
        self.connect(self.addDataAct, QtCore.SIGNAL("triggered()"),self.addDataControl)
        self.connect(self.editDataAct, QtCore.SIGNAL("triggered()"),self.editDataControl)
        self.connect(self.delDataAct, QtCore.SIGNAL("triggered()"),self.delDataControl)
        self.connect(self.edit.btn1 , QtCore.SIGNAL("accepted()"),self.save)
        self.connect(self.edit.btn1 , QtCore.SIGNAL("rejected()"),self.edit.close)

    def addDataControl(self):
        self.edit.ed1.setText('')
        self.edit.setModal(1)
        self.edit.show()
        self.mode = 1

    def editDataControl(self):
        r = self.list.currentRow()
        self.edit.ed1.setText(self.data[self.datarow[r]])
        self.edit.setModal(1)
        self.edit.show()
        self.mode = 2

    def delDataControl(self):
        r = self.list.currentRow()
        id = self.datarow[r]
        dlg = QtGui.QMessageBox(self)
        dlg.setWindowTitle(u'Удаление записи')
        dlg.setText(u'''Вы действительно хотите удалить %s?'''%
            (self.data[id]))
        dlg.setStandardButtons(QtGui.QMessageBox.Cancel | QtGui.QMessageBox.Ok );
        ok = dlg.exec_()
        if ok==QtGui.QMessageBox.Ok :
            self.delData(id)
            self.redraw()

    def save(self):
        
        r = self.list.currentRow()
        name = self.edit.ed1.text()
        if self.mode == 1:
            self.addData(name)
        elif self.mode == 2:
            id = self.datarow[r]
            self.editData(id,name)
        self.redraw()
