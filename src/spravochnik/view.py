#!/usr/bin/python
# -*- coding: utf-8  -*-
from model import model
from PyQt4 import QtGui, QtCore

class view(QtGui.QWidget, model):
    def __init__(self, parent = None, mysql = None):
        QtGui.QWidget.__init__(self, parent)
        model.__init__(self, mysql)
        self.list = QtGui.QListWidget(self)
        grid = QtGui.QGridLayout(self)
        grid.addWidget(self.list)
        self.initAction()
        self.redraw()

    def redraw(self):
        self.list.clear()
        self.getData()
        for i in self.datarow:
            a = self.data[i]
            self.list.addItem("%s"%(a))

    def initAction(self):
        self.addDataAct = QtGui.QAction(u'Добавить',self)
        self.editDataAct = QtGui.QAction(u'Изменить',self)
        self.delDataAct = QtGui.QAction(u'Удалить',self)








