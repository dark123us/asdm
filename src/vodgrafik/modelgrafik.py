#!/usr/bin/python
# -*- coding: utf-8  -*-
from spr.model import model
from datetime import date
import calendar


class holiday(model):
    """
    Получаем данные по праздничным в указанном месяце. Используемые переменные:
        * datarow - список с ключами id
        * data - словарь с ключами id таблицы holiday и значениями в виде словаря с ключами:
            * 'date' - datetime.date дата праздника
    """
        
    def __init__(self, mysql = None):
        """
        Инициализация класса. Входные данные:
            * mysql - ссылка на db.db()"""
            
        model.__init__(self,mysql)
        self.dataparam = ['date']
        self.initSql()
    
    def initSql(self):
        "Инициализация запросов"
        
        self.getDataSQL = '''
            SELECT id, date FROM holiday
                    WHERE date>='%s' and date<='%s'
                    ORDER BY date
            '''
            
    def redraw(self, data = []):
        '''Пересчитываем данные
        
        Входные данные: 
            [год, месяц, последний день месяца]
            
        `--------`'''
            
        data = ["%04d-%02d-01"%(data[0],data[1]),"%04d-%02d-%02d"%(data[0],data[1],data[2])]
        self._getData(data)
    '-----'
#################################################################################
    
class dayFlag:
    '''
    Рассчитываем флаги выставляемые для дня (пн, вт ... вс, раб, вых, празд, предпр., кроме праздн., все дни)
    
        Переменные:
            * date - переменная хранящая рассчитываемую дату, если дата в redraw не будет указана, будет использоваться данная переменная 
            * hld - переменная хранящая ссылку на класс holiday
            * flag - список по количеству дней (0..день-1) в рассчитываемом месяце, хранящий сумму флагов типа дня, где:
                * 1 — понедельник
                * 2 — вторник
                * 4 — среда
                * 8 — четверг
                * 16 — пятница
                * 32 — суббота
                * 64 — воскресенье
                * 128 — рабочий день
                * 256 — выходной день
                * 512 — праздничный
                * 1024 — предпраздничный
                
            Пример 512+1+128 = 641 — понедельник, рабочий, праздничный
    '''
    
    def __init__(self, datemonth, hld):
        '''Инициализация класса 
        
            Входные данные: 
                * datemonth - datetime.date любой день рассчитываемого месяца
                * hld - ссылка на класс holiday, рассчитанный на выбранный месяц 
        '''
        self.date = datemonth
        self.hld = hld
        self.redraw()
    
    def redraw(self, datemonth = None):
        '''Перерасчет данных, выставление флага для каждого дня
        
            Входные данные :
                * datemonth - datetime.date - любой день рассчитываемого месяца, можно опустить, тогда используется переменная date
            Дополнительно:
                * В функции класс holday, представленный переменной hld заново рассчитывается, перед использованием.
        '''
        if datemonth != None:
            self.date = datemonth
        self.days = calendar.monthrange(self.date.year, self.date.month)[1]#количество дней в месяце
        self.hld.redraw([self.date.year, self.date.month,self.days])
        self.flag = []
        for day in range(self.days):
            self.dayweek = calendar.weekday(self.date.year, self.date.month, day+1)
            fl = 2**(self.dayweek)#флаг дня недели
            #рабочий или выходной день            
            if self.dayweek < 5: fl += 128 #2**7
            else: fl += 256#2**8
            #признак праздничного дня
            for hldid in self.hld.datarow:
                hldday = self.hld.data[hldid]['date'].day 
                if hldday == day+1:
                    fl = 512 #2**9 праздничный
                if hldday-1 == day+1: 
                    fl += 1024 #2**10 предпраздничный
            self.flag += [fl]
#################################################################################            
class getIdKartaRangeToMonth:
    '''Класс для получения idkartarange на каждый день выбранного месяца для idkarta,
    если карточка в этот день не будет работать, то значение будет по умолчанию iddop 
    
    Переменные:    
        * mysql - ссылка на класс :py:class:`db.db` — привязка к базе данных
        * hld - ссылка на класс :py:class:`holiday`
        * idkarta - идентификатор карточки для которой производится расчет
        * date - дата (любой день) datetime.date в месяце которого производится расчет
        * dayflag - ссылка на класс dayFlag
        * datarow - список ключей id таблицы kartarange 
        * data - список на каждый день словарей с ключами id таблицы kartarange и значением в виде словаря с ключами:
            * «name» - имя карточки
            * «put» - значения для путевых листов
            * «flag» - сумма флагов для текущей idkartarange
            * «begin» - начальная дата действия карточки (kartarange) (если будет значение, иначе пустое)
            * «end» - конечная дата действия (если будет значение, иначе пустое)'''
            
    def __init__(self, mysql = None):
        '''Инициализация класса
        
            Входные данные:
                * mysql - (ссылка на db.db) '''
        self.mysql = mysql
        self.hld = holiday(mysql)
        self.idkarta = 1
        self.date = date(2011,1,1)
        self.dayflag = dayFlag(self.date, self.hld) #если используется внешняя обработка, то перед выполнением redraw этого класса сделать redraw dayFlag класса
        self.initSql()

    def initSql(self):
        "Инициализация SQL запросов и параметров полей словарей"
        self.paramKartaRangeFlag = ["monday", "tuesday", "wednesday", "thursday",
                                    "friday", "saturday", "sunday", "workday", 
                                    "weekend", "holiday", "bholiday", "noholiday",
                                    "allday"]
        self.paramKartaRange = ["name", "put"]
        self.SqlGetKartaRange = '''
            SELECT kr.id as id, kr.monday, kr.tuesday, kr.wednesday, kr.thursday, 
                kr.friday, kr.saturday, kr.sunday, kr.workday, kr.weekend, 
                kr.holiday, kr.bholiday, kr.noholiday, kr.allday, k.name, k.put
            FROM karta as k, kartarange as kr
            WHERE k.id = kr.idkarta AND kr.idkarta = %d AND kr.del = 0
            '''
        #выборка периодов для карточки за выбранный промежуток
        # [начало периода, конец, начало, начало, idkarta]
        self.SqlGetKartaRangeDay = '''
            SELECT krd.id, krd.idkartarange, krd.begin, krd.end
            FROM kartarangeday as krd, kartarange as kr, karta as k
            WHERE k.id = %d 
                AND krd.idkartarange = kr.id AND kr.idkarta = k.id
                AND krd.del = 0 AND kr.del = 0 AND k.del = 0'''
            
    def redraw(self,data):
        '''Перерасчёт всех переменных
        
            Входные данные:
                * data — список вида:
                    * idkarta — идентификатор карточки
                    * datetime.date — день месяца, который будет рассчитываться'''
                    
        self.idkarta = data[0]
        self.date = data[1]
        days = calendar.monthrange(self.date.year, self.date.month)[1]#количество дней в месяце
        
        self.data = []#обнуляем данные для карточки
#        #структура data[день] = {} имя карты, idkartarange
        try:
            #получаем флаги для каждого периода карточки
            kartaRange, kartaRangeRow = self.getFlagKartaRange()
            self.datarow = kartaRangeRow 
            #получаем даты действия для каждого периода карточек
            kartaRange = self.getDayKartaRange(kartaRange)
            #место для расширения инфы, как-то время выезда, прибытия, ночных и др., выше приведены примеры по расширению функциональности
            #проходим по всем дням и заполняем idkartarange
            for day in range(days):
                #делаем проход по всем периодам карточек                
                dayfl = self.dayflag.flag[day] #флаги дня
                dateday = date(self.date.year, self.date.month,day+1)#полная датя для дня
                for id in kartaRangeRow:
                    er = 0#сбрасываем ошибки
                    #проверяем флаг
                    fl = kartaRange[id]["flag"]
                    #print fl, dayfl,  fl & dayfl, day
                    if ((fl & dayfl) and 1) and not((fl & 2048) and (dayfl & 1024)):#если день попадает под флаг и нет праздничный не попадает под "кроме праздничных   
                        #проверяем даты
                        try:#если даты не будет, сработает исключение
                            beg = kartaRange[id]["begin"]
                            end = kartaRange[id]["end"]
                            if beg > dateday or end < dateday: er = 1
                        except:
                            pass
                        if er == 0: #если ошибок нет - заносим значение, переходим к следующему дню
                            tmp = kartaRange[id]
                            tmp["id"] = id
                            self.data += [tmp]
                            break
                    else:
                        er = 1
                        #есть ошибки - переходим к другому значению периода карточек
                #периоды закончились с ошибкой, пишем 0, переходим к следующему дню,                 
                if er == 1:
                    tmp = {}
                    tmp["name"] = u'В'#если не найден элемент, считаем, что это выходной
                    tmp["id"] = 0
                    self.data += [tmp]
        except Exception,e:
            print "getIdKartaRangeToMonth ",e
    #функции расширения
    def getFlagKartaRange(self):
        '''Функция расширения (первая) для расчёта флагов для каждого периода карточки, данные берутся из таблицы kartarange.
        
            Возвращаемое значение:
                * kartaRange - словарь с ключем id - целое, его значения:
                    * словарь с ключами из переменной paramKartaRange:
                         * «name» - имя карточки
                         * «put» - значения для путевых листов
                    * «flag» - сумма флагов для текущей idkartarange
                * kartaRangeRow — список с ключами id для словаря
        '''
        self.mysql.execsql(self.SqlGetKartaRange%self.idkarta)
        result_set = self.mysql.cursor.fetchall()
        kartaRange = {}
        kartaRangeRow = []
        for row in result_set:
            fl = 0 #флаг для записи
            i = 0  #индес
            tmp = {}
            for j in self.paramKartaRangeFlag:#
                fl += row[j]/2 * 2 ** i
                i += 1
            tmp["flag"] = fl
            for j in self.paramKartaRange:#
                tmp[j] = row[j]
            kartaRange[row["id"]] = tmp
            kartaRangeRow += [row["id"]]
        return (kartaRange, kartaRangeRow)
    
    def getDayKartaRange(self,kartaRange):
        '''Функция расширения, для расчета работает ли карточка в заданный период (с какого по какое, если есть в базе данных)
        
            Входные данные:
                 * kartaRange — словарь с ключем id и словарем с ключами в виде значения — получаем только для расширения словаря значений
            Возвращаемое значение
                 * kartaRange — словарь с ключем id и словарем с дополненными ключами:
                    * «begin» - начальная дата действия карточки (kartarange)
                    * «end» - конечная дата действия
        '''
        self.mysql.execsql(self.SqlGetKartaRangeDay%(self.idkarta))
        result_set = self.mysql.cursor.fetchall()
        for row in result_set:
            try:
                tmp = kartaRange[row["idkartarange"]]
                tmp["begin"] = row["begin"]
                tmp["end"] = row["end"]
                kartaRange[row["idkartarange"]] = tmp
            except KeyError:
                print "KeyError: in kartarange not found key from kartarangeday id = %d"%row["id"]
        return kartaRange
#################################################################################
class getInfoKartaRange:
    '''Класс для получения информации о kartarange - начало, конец по путевому и др.
    
    Переменные:
        * data - словарь с ключами id таблицы kartarange и значением в виде словаря с ключами:
            * begintime - начало работы datetime.timedelta
            * endtime - конец работы datetime.timedelta
            * endday - день завершения работы
            * smena - смена
            * idkarta - id карты
        * datarow - список с ключами id таблицы kartarange
        * brigada - рассчитываемая бригада
        * mysql - ссылка на db.db'''
        
    def __init__(self, mysql, brigada = 1):
        '''Инициализация класса
        
            Входные данные:
                * mysql - ссылка на db.db
                * brigada - номер бригады'''
                
        self.brigada = brigada
        self.mysql = mysql
        self.initSql()
    
    def initSql(self):
        '''Инициализация запросов и их параметров'''
        #информация по маршрутам, на которых действует картрэндж
        self.dataInfoMarshParam = []
        self.getInfoMarshSQL = ''' SELECT '''
        #имя, ид, карты, сроки действия картрэнджа
        self.dataInfoKartaParam = []
        self.getInfoKartaSQL = ''' SELECT '''
        #время по путевому
        self.dataInfoPutParam = ["begintime", "endtime", "endday", "smena", "idkarta"]
        self.getInfoPutSQL = ''' 
            SELECT kr.id, krt.begintime, krt.endtime, krt.endday, k.smena, k.id as idkarta
            FROM kartarangetime as krt, vodgrkartasort as vks, kartarange as kr, karta as k
            WHERE vks.brigada = %d AND vks.idkarta = kr.idkarta 
                AND krt.idkartarange = kr.id and kr.idkarta = k.id'''
    
    def redraw(self):
        '''Перерасчёт всех переменных'''
        if self.brigada > 0:
            self.data, self.datarow = self._getData(self.getInfoPutSQL, self.dataInfoPutParam, [self.brigada])
    
    def _getData(self, sql, dataparam, data):
        ''' Внутренний метод для извлечения информации из базы данных
            
            Входные данные:
                * sql - запрос на языке SQL
                * dataparam - имена ключей для заполнения справочника
                * data - список параметров для запроса (подмена %s %d в строке)'''
        self.mysql.execsql(sql%tuple(data))
        result_set = self.mysql.cursor.fetchall()
        data = {} #словарь со всеми полями
        datarow = []
        for row in result_set:
            tmpdata = {}
            for i in dataparam:
                tmpdata[i] = row[i] #проход по полям
            data[row["id"]] = tmpdata#сохраняем в данных
            datarow += [row["id"]]
        return data, datarow
    
#################################################################################
class tableLink:
    ''' 
    Класс содержащий данные хранящиеся в ячейках таблицы. 
    
    Используемые переменные:
        * data - двумерный массив [row][col], где row - строка таблицы, col - столбец (день-1), хранящиеся значения:
            * 0 - ничего нет в ячейке
            * словарь с ключами (может быть только один из них):
                * "idkartarange" - id таблицы kartarange
                * "iddop" - id дополнительного параметра (выходной. резерв и др.)
        * iddopfull - имена дополнительных параметров, полное имя [u'Выходной',u'Резерв',u'Отпуск',u'Больничный',u'Без сохранения',u'Прогул',u'Льготный',u'Отсыпной']
        * iddop - имена дополнительных параметров, сокращенное имя [u'В',u'Р',u'О',u'Б',u'Бс',u'П',u'Л',u'Отс']
        * dataerr - словарь с ошибками при проверке таблицы ключ - строка, значения - список словарей:
            * ключ "col" - колонка, в которой ошибка, если меньше нуля - ошибка относится ко всей строке
            * значение - строка с описанием ошибки
        * getinfokartarange - ссылка на класс :py:class:`getInfoKartaRange`
        * dayflag - ссылка на класс :py:class:`dayFlag`
        * days - количество дней в таблице 
        * rowfio - количество фамилий - строк в таблице
        * datafio - словарь с информацией для строк (фамилий), ключ - номер строки
            * worktime - отработанное время в минутах
        '''
        
    def __init__(self, getinfokartarange, dayflag):
        '''Инициализация класса, 
            Входные данные: 
                getinfokartarange - ссылка на класс :py:class:`getInfoKartaRange`
                dayflag - ссылка на класс :py:class:`dayFlag`
                '''
                
        self.data =[[]]#таблица по строкам и столбцам с ссылками на idkartarange
        self.days = 31
        self.rowfio = 50
        self.iddopfull = [u'Выходной',u'Резерв',u'Отпуск',u'Больничный',u'Без сохранения',u'Прогул',u'Льготный',u'Отсыпной']
        self.iddop = [u'В',u'Р',u'О',u'Б',u'Бс',u'П',u'Л',u'Отс']
        self.getinfokartarange = getinfokartarange
        self.dayflag = dayflag
        self.dataerr = {}
    
    def clear(self):
        ''' Очистка таблицы'''
        self.data = []
        for row in range(self.rowfio):
            self.data += [[]]
            for day in range(self.days):            
                self.data[row] += [0]
    
    def _geterr(self, row, col, mess):
        tmp = {}
        tmp[col] = mess
        try:
            self.dataerr[row] += [tmp]
        except:
            self.dataerr[row] = [tmp]

    def calctime(self):
        "Расчет времени по карточкам"

        self.datafio = {}
        rows = len(self.data)
        if rows > 0:
            cols = len(self.data[0])
            #идем по строкам-фамилиям
            for row in range(rows):
                tmpdict = {}
                atime = 0
                for col in range(cols):#проход по дням
                    try:#проход только по дням где есть карточка
                        tmp = self.data[row][col]
                        if tmp != 0:
                            idkartarange = tmp["idkartarange"]
                            info = self.getinfokartarange.data[idkartarange]
                            worktime = info["endtime"].seconds / 60 + info["endday"] * 1440 - info["begintime"].seconds / 60
                            atime += worktime 
                    except KeyError:
                        pass
                tmpdict["worktime"] = atime
                self.datafio[row] = tmpdict
        
            
    def provdata(self):
        '''Проверка введенных данных для графика по правилам:
            
            * отработать не более 6-ти дней в одну смену подряд
            * При переходе с 2-й смены на 1-ю дается 2 выходных
            * Между сменами разрыв не менее 12 часов
            * Выходных сколько полных недель в месяце (В) + праздники 
            * Отсыпных сколько выходных - В
            * ОВ одно должно попасть на субботу – воскресенье (при переходе со 2-й смены на 1-ю)
            * Между водителями пересменку в обед предусмотреть в одном месте
                '''
                
        #TODO что такое полная неделя
        #проход по рядам
        self.dataerr = {}
        try:
            rows = len(self.data)
            cols = len(self.data[0])
            #подбиваем сколько полных недель в месяце, сколько праздничных
            week = 0
            hld = 0
            for day in range(self.dayflag.days):
                if self.dayflag.flag[day] & 64:#если попали в воскресенье
                    week += 1
                if self.dayflag.flag[day] & 1024:#праздничный день
                    hld += 1
            #идем по строкам-фамилиям
            for row in range(rows):
                smena = 0 #текущая смена
                smena1 = 0 #предыдущая смена
                smena2 = 0 #2 смены назад
                smena3 = 0 #3 смены назад
                kolsmena = 0 #количество одинаковых смен
                vih = 0 #насчитано выходных
                ots = 0 #насчитано отсыпных
                otsvih21 = 0#флаг попадания ОВ на субботу-воскресенье 1 - ots попал на субботу, 2 - условие выполенно
                idkartarange = 0
                idkartarange1 = 0#предыдущая метка
                iddop = 0
                iddop1 = 0
                for col in range(cols):#проход по дням
                    try:#текущий параметр это маршрут
                        idkartarange = self.data[row][col]["idkartarange"]
                        try:#проверка на наличие информации по карте - его не будет, если нет записи времени для путевок
                            smena = self.getinfokartarange.data[idkartarange]["smena"]
                            #print col, smena1, smena
                            if smena1 == smena:#если это уже не первый день одна и та же смена
                                kolsmena += 1
                                if kolsmena > 5:#если смены повтрились уже в 6-й раз 
                                    pass #TODO генерим ошибку о повторе смен
                                    self._geterr(row,col,u'Больше 6-ти дней подряд %d-я смена'%smena)
                            else:
                                kolsmena = 0
                            if (smena == 1# and col > 1#если текущая смена 1 и прошло 2 и более дней
                                    and ((smena3 == 2 and (smena2 !=0 or smena1 !=0))#если смена 2 дня назад 2-я, а после нее небыло 2 выходных
                                         or(smena2 == 2)  #или один день назад была 2-я смена (т.е. нет отсыпного-выходного при пересменке)
                                         or(smena1 == 2))): #или прошлый день 2-я смена (см.выше)
                                pass#TODO генерим ошибку - неверный переход со второй смены к первой
                                self._geterr(row,col,u'При переходе от второй смены к первой нет отсыпного + выходного')
#                            if (smena == 2 and col > 1#если текущая смена 2 и прошло 2 и более дней
#                                    and ((smena2 == 1 and smena1 !=0)#если смена 2 дня назад 1-я, а после нее небыло 1 выходного
#                                         or(smena1 == 1))): #или прошлый день 1-я смена (см.выше)
#                                pass#TODO генерим ошибку - неверный переход с первой смены во вторую
#                                self._geterr(row,col,u'При переходе от первой смены ко второй нет выходного')
                            if smena1 != 0:#если прошлый день выходной
                                tb = self.getinfokartarange.data[idkartarange]["begintime"].seconds + 86400 #для начала новой смены добавляем сутки
                                te = self.getinfokartarange.data[idkartarange1]["endtime"].seconds + self.getinfokartarange.data[idkartarange1]["endday"]*86400#узнаем во сколько завершилась прошлая смена
                                if tb - te < 43200: #если между завершением предыдущей и началом новой смены меньше 12 часов
                                    pass #TODO генерим ошибку - время между сменами меньше 12 часов
                                    self._geterr(row,col,u'Между сменами меньше 12 часов')
                        except KeyError:#если возникла ошибка - нет записи времени в таблице kartarangetime
                            print "ERROR (in row = %d, col = %d, idkartarange = %d) - not found time"%(row,col,idkartarange)
                            self._geterr(row,col,u'Время выезда и возвращения для карточки не установлено')
                            smena = 0
                        smena3 = smena2
                        smena2 = smena1
                        smena1 = smena      
                        idkartarange1 = idkartarange
                    except KeyError:# если ошибка, значит возможно это дополнительный картарендж
                        iddop = self.data[row][col]["iddop"]
                        if iddop == 7:#если отсыпной
                            ots += 1
                        elif iddop == 0:#если выходной
                            vih += 1
                            if self.dayflag.flag[col] & 64 and smena1 == 0:#если выходной в воскресенье, а предыдующий день отсыпной при этом проверяем, что смена равна 0
                                otsvih21 = 1
                        iddop1 = iddop
                        smena3 = smena2
                        smena2 = smena1
                        smena1 = 0
                        idkartarange1 = 0
                    except TypeError:# если ошибка, в этом поле вообще ничего нет
                        pass#TODO здесь реакция на пустую клетку
                        if iddop == 0:#если выходной
                            vih += 1
                            if self.dayflag.flag[col] & 64 and smena1 == 0:#если выходной в воскресенье, а предыдующий день отсыпной при этом проверяем, что смена равна 0
                                otsvih21 = 1                        
                        iddop1 = 0
                        smena3 = smena2
                        smena2 = smena1
                        smena1 = 0
                        idkartarange1 = 0
                #TODO после прохода цикла подбиваем количество выпавших выходных дней
                if otsvih21 == 0:
                    self._geterr(row, -1, u'Нет отсыпного-выходного попадающего на выходные')
                if vih < (week+hld):
                    self._geterr(row, -1, u'Выходных меньше, чем полных недель')
                if ots < (week - vih):
                    self._geterr(row, -1, u'Отсыпных меньше, чем выходных в месяце - В')
        except IndexError:
            print "not found rows or cols"
        
        #print self.getinfokartarange.data
        #print self.getinfokartarange.datarow
