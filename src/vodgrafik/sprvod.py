#!/usr/bin/python
# -*- coding: utf-8  -*-
from PyQt4 import QtGui, QtCore
from spr.control import controltable
from spr.view import viewcombo

class garnomcombo(viewcombo):
    def __init__(self, parent = None, mysql = None):
        self.initSQL()
        viewcombo.__init__(self,parent,mysql)
    
    def initSQL(self):
        self.dataparam = ['garnom', 'gosnom']
        self.dataview = ['garnom', 'gosnom']
        self.getDataSQL = '''
                SELECT id, garnom, gosnom
                FROM bus
                WHERE del = 0
                ORDER BY garnom
            '''
    
    def itemcombo(self,id):
        s = "%d (%s)"%(self.data[id]["garnom"],self.data[id]["gosnom"])
        return s

class editWind(QtGui.QDialog):#вспомогательное диалоговое окно
    def __init__(self, parent = None, mysql = None):
        QtGui.QDialog.__init__(self, parent)
        self.setWindowTitle(u'Добавить водителя')
        self.grid = QtGui.QGridLayout(self)
        self.btn1 = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok|
                                      QtGui.QDialogButtonBox.Cancel)
        self.connect(self.btn1 , QtCore.SIGNAL("accepted()"), self.accept)
        self.connect(self.btn1 , QtCore.SIGNAL("rejected()"), self.reject)
        self.brigada = []
        for i in range(4):
            self.brigada += [str(i+1)]
        self.garnomModel = garnomcombo(parent,mysql)
        self.garnomModel.redraw()
        self.initWidget()

    def initWidget(self):
        lbl1 = QtGui.QLabel(u'Фамилия')
        lbl2 = QtGui.QLabel(u'Имя')
        lbl3 = QtGui.QLabel(u'Отчество')
        lbl4 = QtGui.QLabel(u'Табельный номер')
        lbl5 = QtGui.QLabel(u'№ бригады')
        lbl6 = QtGui.QLabel(u'Гаражный номер')
        self.ed1 = QtGui.QLineEdit()
        self.ed2 = QtGui.QLineEdit()
        self.ed3 = QtGui.QLineEdit()
        self.ed4 = QtGui.QLineEdit()
        self.cmb5 = QtGui.QComboBox()
        self.cmb5.addItems(self.brigada)
        #self.cmb6 = QtGui.QComboBox()
        #self.cmb6.addItems(self.garnom)
        self.grid.addWidget(lbl1,0,0)
        self.grid.addWidget(self.ed1,0,1)
        self.grid.addWidget(lbl2,1,0)
        self.grid.addWidget(self.ed2,1,1)
        self.grid.addWidget(lbl3,2,0)
        self.grid.addWidget(self.ed3,2,1)
        self.grid.addWidget(lbl4,3,0)
        self.grid.addWidget(self.ed4,3,1)
        self.grid.addWidget(lbl5,4,0)
        self.grid.addWidget(self.cmb5,4,1)
        self.grid.addWidget(lbl6,5,0)
        self.grid.addWidget(self.garnomModel,5,1)
        self.grid.addWidget(self.btn1,6,0,1,2)

class sprvodtable(controltable):
    def __init__(self, parent = None, mysql = None):
        controltable.__init__(self, parent, mysql)
        self.initSql()        
        self.initConnect()                
        self.edit = editWind(self, mysql)
        self.connect(self.edit, QtCore.SIGNAL("accepted()"),self.save)
        self.dest = 1
        self.selcol = 0
    
    def initSql(self):
        self.dataview = ["family", "name", "surname", "tabelnmbr", "brigada", "garnom"]
        self.headername = [u"Фамилия", u"Имя", u"Отчество",u"Табельный номер",
            u"Бригада", u"Гаражный"]
        self.dataparam = ["family", "name", "surname", "tabelnmbr", "brigada", "garnom", "idbus"]
        self.getDataSQL = '''
            SELECT v.id, v.family, v.name, v.surname, v.tabelnmbr, v.brigada, 0 as garnom, 0 as idbus
            FROM voditel as v
            WHERE v.idbus = 0 %s
            UNION 
            SELECT v.id, v.family, v.name, v.surname, v.tabelnmbr, v.brigada, b.garnom, v.idbus
            FROM voditel as v, bus as b
            WHERE (v.idbus = b.id) AND b.del = 0 %s
            ORDER BY %s
            '''
        self.addDataSQL = '''
                INSERT INTO voditel  (family, name, surname, tabelnmbr, brigada, idbus)
                VALUES ('%s','%s','%s', %d, %d, %d)
                '''
        self.editDataSQL = '''
                UPDATE voditel SET family = '%s', name  = '%s', surname = '%s', 
                    tabelnmbr = %d, brigada = %d, idbus = %d
                WHERE id=%d
                '''
        self.delDataSQL = ''' DELETE FROM voditel WHERE id = %d '''
        self.connect(self.horizontalHeader(), QtCore.SIGNAL("sectionClicked(int)"),self.sortChanged)

    def redraw(self, data = []):
        "Перерисовка виджета, ожидаем строку 'AND v.brigada = 1' или пустую строку"
        
        andbr = ''
        if len(data) > 0:
            andbr = "AND v.brigada = %d "%data[0]
        if self.dest: s = ' ASC'
        else: s = ' DESC'
        d = "%d %s"%(self.selcol+2,s)
        controltable.redraw(self,[andbr,andbr,d])

    def sortChanged(self,i):
        if i == self.selcol:
            self.dest = 1 - self.dest
        else:
            self.selcol = i
            self.dest = 0
        self.redraw()
    
    def addDataControl(self):
        self.edit.setWindowTitle(u'Добавить водителя')
        self.edit.ed1.setText('')
        self.edit.ed2.setText('')
        self.edit.ed3.setText('')
        self.edit.ed4.setText('')
        self.edit.cmb5.setCurrentIndex(-1)
        self.edit.garnomModel.redraw([])
        controltable.addDataControl(self)
    
    def editDataControl(self):
        self.edit.setWindowTitle(u'Изменить данные водителя')
        id = self.datarow[self.currentRow()]
        self.edit.ed1.setText(self.data[id]['family'])
        self.edit.ed2.setText(self.data[id]['name'])
        self.edit.ed3.setText(self.data[id]['surname'])
        self.edit.ed4.setText(str(self.data[id]['tabelnmbr']))
        self.edit.cmb5.setCurrentIndex(self.data[id]['brigada']-1)
        self.edit.garnomModel.redraw([],self.data[id]['idbus'])
        controltable.editDataControl(self)
    
    def save(self):
        try: d = int(self.edit.ed4.text())
        except: d = 0
        if self.edit.garnomModel.currentIndex()<0: idbus = 0
        else: idbus = self.edit.garnomModel.datarow[self.edit.garnomModel.currentIndex()]
        if self.edit.cmb5.currentIndex()<0: d1 = 0
        else: d1 = self.edit.cmb5.currentIndex()+1
        self.datasave = [self.edit.ed1.text(),
                         self.edit.ed2.text(),
                         self.edit.ed3.text(),
                         d,
                         d1,
                         idbus]
        if self.mode == 2:
            id = self.datarow[self.currentRow()]
        controltable.save(self)
        if self.mode == 1:
            id = self._getLastId()
        self.redraw()
        k = 0
        for i in self.datarow:
            if i == id:
                self.setCurrentCell(k, 0)
                break
            k += 1

    def delDataControl(self):
        controltable.delDataControl(self)
        self.redraw()

class sprvod(QtGui.QMdiSubWindow):
    def __init__(self, parent = None, mysql = None):
        QtGui.QMdiSubWindow.__init__(self, parent)
        self.setWindowTitle(u'Список водителей')        
        self.table = sprvodtable(parent, mysql)
        self.initWidget()
        self.table.redraw()
            
    def initWidget(self):
        widg = QtGui.QWidget()
        grd = QtGui.QGridLayout()
        vh = QtGui.QVBoxLayout()
        brigada = QtGui.QComboBox()
        br = [u'Все бригады']
        for i in range(10):
            br += [str(i+1)]
        brigada.addItems(br)
        brigada.setCurrentIndex(0)
        tool = QtGui.QToolBar()
        tool.addAction(self.table.addDataAct)
        tool.addAction(self.table.editDataAct)
        tool.addAction(self.table.delDataAct)
        tool.addWidget(brigada)
        tool.setIconSize(QtCore.QSize(48,48))
        grd.addWidget(tool)
        grd.addWidget(self.table)
        vh.addLayout(grd)
        widg.setLayout(vh)
        self.setWidget(widg)
        self.connect(brigada,QtCore.SIGNAL("currentIndexChanged (int)"),self.brigadaChanged)

    def brigadaChanged(self, i):
        "Реакция на изменение отображаемой бригады"
        
        if i == 0:
            self.table.redraw()
        else:
            self.table.redraw([i])
