#!/usr/bin/python
# -*- coding: utf-8  -*-
from PyQt4 import QtGui, QtCore
from freezetable import tblvodgrafik, kartaShow
import calendar
from datetime import date

class kartaTable(QtGui.QTableWidget):
    '''Виджет отображения количества действующих карточек за день. Инициализация класса:
        
    :param mysql: ссылка на класс
    :type mysql: :py:class:`db.db`
    
    Используемые переменные:
        
    :param dict data: ключ - idkarta, значения -
        * countid - количество 
        * date - дата
        * name - имя карточки
    :param list datarow: список ключей idkarta для data
        
    '''
    def __init__(self, parent = None, mysql = None):
        QtGui.QTableWidget.__init__(self, parent)
        self.mysql = mysql
        self._dataview = ["idkarta", "namekarta"]#первые столбцы без дней, в redraw столбцы добавяться в конец
        self.year = 2011
        self.month = 1
        self.brigada = 1
        self.initSql()

    def initSql(self):
            self.getKartaSQL = '''SELECT k.id, k.name, k.vihod, k.smena, k.put
                                 FROM vodgrkartasort as vgks, karta as k
                                 WHERE vgks.idkarta = k.id AND k.del = 0 AND vgks.brigada = %d
                                 ORDER BY k.name
                              '''
            self.getDataSQL = '''
                    SELECT vg.id as id,  kr.idkarta as idkarta, count(kr.idkarta) as countid, vg.date
                    FROM vodgrafik as vg, kartarange as kr
                    WHERE vg.date>='%s' AND vg.date<='%s' AND
                        vg.idkartarange = kr.id AND vg.brigada = %d
                    GROUP BY vg.date, kr.idkarta'''
        
    def redraw(self, data):#
        '''Отрисовка, с перечитыванием данных'''
        self.brigada = data[0]
        self.year = data[1]
        self.month = data[2]
        day = calendar.monthrange(self.year, self.month)[1] 
        (f,dr,d) = self.mysql.getData(self.getKartaSQL%tuple([self.brigada]))
        #обнуляем таблицу
        tbl = []
        for row in range(len(dr)):
            tbl += [[]]
            for j in range(day):
                tbl[row] += [0]
        self.setRowCount(0)
        self.setRowCount(len(tbl))
        #заполняем залоговки
        self._headername = [u'Карточка', u'Код']
        self.headername = self._headername[:] 
        for i in range(day):
            self.headername += [str(i+1)] 
        self.setColumnCount(day + len(self._headername))
        self.setHorizontalHeaderLabels(self.headername)
        row = 0
        rowidkarta = {}#id карта в какой строке
        for id in dr:
            s = d[id]['name']
            self.itemtable(row, 0, s)
            s = d[id]['put']
            self.itemtable(row, 1, s)
            rowidkarta [id] = row
            row += 1
        #заполняем данные
        day1 = str(date(self.year, self.month, 1))
        day2 = str(date(self.year, self.month, day))
        s = self.getDataSQL%tuple([day1, day2, self.brigada])
        (f,dr,d) = self.mysql.getData(s)
        for id in dr:
            row = rowidkarta[d[id]['idkarta']]
            col = d[id]['date'].day + len(self._headername) - 1
            s = str(d[id]['countid'])
            self.itemtable(row, col, s)
        self.resizeColumnsToContents()
        self.resizeRowsToContents ()
        self.setCurrentCell(0,0)

    def itemtable(self,row,col,s):
        item = QtGui.QTableWidgetItem("%s"%s)
        self.setItem(row,col,item)
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class listError(QtGui.QListWidget):
    "Виджет со списком для вывода ошибок текстом"
    def __init__(self,parent):
        QtGui.QListWidget.__init__(self,parent)
        
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class vodgrafik(QtGui.QMdiSubWindow):
    '''Окно вывода графика и его ошибок
    
    :param mysql: ссылка на класс :py:class:`db.db`
    :param tblgrafik: таблица с замороженными колонками
    :type tblgrafik: :py:class:`freezetable.tblvodgrafik`
    :param marshShow: диалоговое окно, для добавления карточки
    :type marshShow: :py:class:`freezetable.kartaShow`  
        '''
    
    def __init__(self, parent = None, mysql = None):
        QtGui.QMdiSubWindow.__init__(self, parent)
        self.setWindowTitle(u'График водителей')        
        self.tblgrafik = tblvodgrafik(parent, mysql)
        self.listerror = listError(self)
        self.kartatbl = kartaTable(parent,mysql)
        
        
        self.marshShow = kartaShow(parent, mysql)
        self.marshShow.setModal (True)
        self.initWidget()
        self.initAction()
        self.initMenu()
        self.redraw()
    
    def initWidget(self):
        '''Инициализация виджетов, расстановка по диалоговому окну

        Виджеты:
            * self.btn = QtGui.QPushButton(u'Обновить')- обновление данных
            * self.cmbmonth = QtGui.QComboBox() - месяц
            * self.cmbyear = QtGui.QComboBox() - год
            * self.cmbbrigada = QtGui.QComboBox() - бригада
            '''
            
        widg = QtGui.QWidget()
        lh1 = QtGui.QHBoxLayout()
        grd = QtGui.QGridLayout()
        lv1 = QtGui.QVBoxLayout()
        self.btn = QtGui.QPushButton(u'Обновить')#обновить график
        self.btnKarta = QtGui.QPushButton(u'Карточки')#показать окно совпадения или нехватки карточек
        self.btnError = QtGui.QPushButton(u'Ошибки')#показать список ошибок
        self.btnKodChange = QtGui.QCheckBox(u'Наименование/Код маршрута')#переключатель как отоброжать данные на графике - в виде кодов или названия
        self.btnKodChange.setCheckState(2)
        lbl1 = QtGui.QLabel(u'Месяц')
        lbl2 = QtGui.QLabel(u'Год')
        lbl3 = QtGui.QLabel(u'Бригада')
        self.cmbmonth = QtGui.QComboBox()
        months = [u'январь', u'февраль', u'март', u'апрель', u'май', u'июнь',
                  u'июль', u'август', u'сентябрь', u'октябрь', u'ноябрь', u'декабрь']
        self.cmbmonth.addItems(months)
        self.cmbmonth.setCurrentIndex(0)
        self.cmbyear = QtGui.QComboBox()
        years = ['2011', '2012', '2013', '2014', '2015']
        self.cmbyear.addItems(years)
        self.cmbyear.setCurrentIndex(0)
        self.cmbbrigada = QtGui.QComboBox()
        brigada = []
        for i in range(10):
            brigada += [str(i+1)]
        self.cmbbrigada.addItems(brigada)
        self.cmbbrigada.setCurrentIndex(0)
        grd.addWidget(lbl1,0,0)
        grd.addWidget(lbl2,0,1)
        grd.addWidget(self.btnKodChange,0,2)
        grd.addWidget(self.cmbmonth,1,0)
        grd.addWidget(self.cmbyear,1,1)
        grd.addWidget(self.btn,1,2)
        grd.addWidget(lbl3,0,3)
        grd.addWidget(self.cmbbrigada,1,3)
        grd.addWidget(self.btnKarta,0,4)
        grd.addWidget(self.btnError,1,4)
        lh1.addLayout(grd)
        lh1.addStretch()     

        lv1.addLayout(lh1)

        spl1 = QtGui.QSplitter(QtCore.Qt.Horizontal)        
        spl1.addWidget(self.tblgrafik)
        spl2 = QtGui.QSplitter(QtCore.Qt.Vertical)
        spl2.addWidget(self.kartatbl)
        spl2.addWidget(self.listerror)
        spl1.addWidget(spl2)
        
        lv1.addWidget(spl1)
        gr = QtGui.QGridLayout()
        gr.addLayout(lv1,0,0)
        
        widg.setLayout(gr)
        self.setWidget(widg)
        self.timer = QtCore.QTimer()#для костылей
        
    def initAction(self):
        '''Инициализация событий'''
        
        self.connect(self.cmbmonth, QtCore.SIGNAL("currentIndexChanged(int)"),self.redraw)
        self.connect(self.cmbyear, QtCore.SIGNAL("currentIndexChanged(int)"),self.redraw)
        self.connect(self.cmbbrigada, QtCore.SIGNAL("currentIndexChanged(int)"),self.redraw)
        self.connect(self.btn, QtCore.SIGNAL("clicked()"),self.redraw)
        
        self.connect(self.btnError, QtCore.SIGNAL("clicked()"),self.showError)
        self.connect(self.btnKarta, QtCore.SIGNAL("clicked()"),self.showKarta)
        
        self.addAct = QtGui.QAction(u'Добавить карточку...', self)
        self.connect(self.addAct, QtCore.SIGNAL("triggered()"), self._addMarshShow)
        self.rmAct = QtGui.QAction(u'Очистить данные', self)
        self.connect(self.rmAct, QtCore.SIGNAL("triggered()"), self._clearmarsh)
        self.vihodnoyAct = QtGui.QAction(u'Выходной',self)
        self.connect(self.vihodnoyAct, QtCore.SIGNAL("triggered()"), self._addVihodnoy)
        self.rezervAct = QtGui.QAction(u'Резерв',self)
        self.connect(self.rezervAct, QtCore.SIGNAL("triggered()"), self._addRezerv)
        self.otpuskAct = QtGui.QAction(u'Отпуск',self)
        self.connect(self.otpuskAct, QtCore.SIGNAL("triggered()"), self._addOtpusk)
        self.bolnAct = QtGui.QAction(u'Больничный',self)
        self.connect(self.bolnAct, QtCore.SIGNAL("triggered()"), self._addBoln)
        self.bezsohrAct = QtGui.QAction(u'Без сохранения',self)
        self.connect(self.bezsohrAct, QtCore.SIGNAL("triggered()"), self._addBezSohr)
        self.progulAct = QtGui.QAction(u'Прогул',self)
        self.connect(self.progulAct , QtCore.SIGNAL("triggered()"), self._addProgul)
        self.lgotnAct = QtGui.QAction(u'Льготный',self)
        self.connect(self.lgotnAct , QtCore.SIGNAL("triggered()"), self._addLgotn)
        self.otsypnAct = QtGui.QAction(u'Отсыпной',self)
        self.connect(self.otsypnAct , QtCore.SIGNAL("triggered()"), self._addOtsypn)
        
        
        self.connect(self.btnKodChange, QtCore.SIGNAL("stateChanged(int)"), self.kodChange)
        
        
        self.connect(self.marshShow, QtCore.SIGNAL("accepted()"), self._addMarshs)
        self.connect(self.timer, QtCore.SIGNAL("timeout()"), self._scrollTable)#для костылей

    def printerr(self):
        '''Реакция на ошибки'''
        
        self.listerror.clear()
        err = 0        
        self.tblgrafik.provdata()
        for row in self.tblgrafik.tablelink.dataerr.keys():
            for tmp in self.tblgrafik.tablelink.dataerr[row]:
                for col, v in tmp.iteritems():
                    if col >= 0:
                        self.listerror.addItem(u"%s [строка %d, день %d]"%(v,row+1,col+1))
                        err += 1
                    else:
                        self.listerror.addItem(u"%s [cтрока %d]"%(v,row+1))
                        err += 1
        self.listerror.addItem("")
        self.listerror.addItem(u"Найдено %d ошибок"%err)
        
        month = self.cmbmonth.currentIndex() + 1
        year = self.cmbyear.currentIndex() + 1 + 2010
        brigada = self.cmbbrigada.currentIndex()+1
        self.kartatbl.redraw([brigada, year, month])

    def kodChange(self, state):
        self.tblgrafik.statePrintName = state/2
        self.redraw()
    
    def redraw(self):
        "Перерисовка данных"
        month = self.cmbmonth.currentIndex() + 1
        year = self.cmbyear.currentIndex() + 1 + 2010
        brigada = self.cmbbrigada.currentIndex()+1
        self.tblgrafik.redraw([brigada, year, month])
        self.printerr()
        self.kartatbl.redraw([brigada, year, month])

    def showKarta(self):
        "Показываем или скрываем виджет совпадения или нехватки карточек за день"
        
        self.kartatbl.setVisible(not self.kartatbl.isVisible())
    
    def showError(self):
        "Показываем или скрываем виджет сообщений об ошибках"
        self.listerror.setVisible(not self.listerror.isVisible())
        
    def contextMenuEvent(self, event):
        "Вызов контекстного меню"
        
        self.cmenu.exec_(event.globalPos())
        
    def initMenu(self):
        "Инициализация контекстного меню"
        
        self.cmenu = QtGui.QMenu(self)
        self.cmenu.addAction(self.addAct)
        self.cmenu.addAction(self.rmAct)
        self.cmenu.addSeparator()
        self.cmenu.addAction(self.vihodnoyAct)
        self.cmenu.addAction(self.otsypnAct)
        self.cmenu.addAction(self.rezervAct )
        self.cmenu.addAction(self.otpuskAct)
        self.cmenu.addAction(self.bolnAct)
        self.cmenu.addAction(self.bezsohrAct)
        self.cmenu.addAction(self.progulAct)
        self.cmenu.addAction(self.lgotnAct)
    
    def _addMarshShow(self):
        self.a = self.tblgrafik.verticalScrollBar().value()
        brigada = self.cmbbrigada.currentIndex()+1
        self.marshShow.table.redraw([brigada,brigada])
        self.marshShow.show()

    def _addMarshs(self):
        tab = self.marshShow.table
        idkarta = tab.data[tab.datarow[tab.currentRow()]]["idkarta"]
        self.tblgrafik.addKarta(self.tblgrafik.selectionModel().selectedIndexes(),idkarta)
        self.printerr()
        self.timer.start(10)#костыль
        
    def _scrollTable(self):#костыль против скролинга после появления диалогового окна и заполнения данных
        if self.tblgrafik.verticalScrollBar().value() != self.a:
            self.timer.stop()        
            self.tblgrafik.verticalScrollBar().setValue(self.a)
        
    def _clearmarsh(self):
        self.tblgrafik.clearKarta(self.tblgrafik.selectionModel().selectedIndexes())
        self.printerr()

    def _addVihodnoy(self):
        self.tblgrafik.addKarta(self.tblgrafik.selectionModel().selectedIndexes(),0,1)
        self.printerr()

    def _addRezerv(self):
        self.tblgrafik.addKarta(self.tblgrafik.selectionModel().selectedIndexes(),1,1)
        self.printerr()

    def _addOtpusk(self):
        self.tblgrafik.addKarta(self.tblgrafik.selectionModel().selectedIndexes(),2,1)
        self.printerr()

    def _addBoln(self):
        self.tblgrafik.addKarta(self.tblgrafik.selectionModel().selectedIndexes(),3,1)
        self.printerr()

    def _addBezSohr(self):
        self.tblgrafik.addKarta(self.tblgrafik.selectionModel().selectedIndexes(),4,1)
        self.printerr()

    def _addProgul(self):
        self.tblgrafik.addKarta(self.tblgrafik.selectionModel().selectedIndexes(),5,1)
        self.printerr()

    def _addLgotn(self):
        self.tblgrafik.addKarta(self.tblgrafik.selectionModel().selectedIndexes(),6,1)
        self.printerr()

    def _addOtsypn(self):
        self.tblgrafik.addKarta(self.tblgrafik.selectionModel().selectedIndexes(),7,1)
        self.printerr()