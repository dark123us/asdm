#!/usr/bin/python
# -*- coding: utf-8  -*-
from PyQt4 import QtGui, QtCore
from spr.control import controltable
from spr.view import viewcombo

class dopcombo(QtGui.QComboBox):
    def __init__(self, parent = None, mysql = None):
        QtGui.QComboBox.__init__(self, parent)
        self.mysql = mysql
        self.sql = '''SELECT id, name FROM dopoln'''
        
    def redraw(self):
        self.clear()
        (f, self.datarow, self.data) = self.mysql.getData(self.sql)
        s = []
        for id in self.datarow:
            s += [self.data[id]['name']]
        self.addItems(s)
    
    def setSelect(self,iddop):
        k = 0
        for i in self.datarow:
            if i == iddop:
                self.setCurrentIndex(k)
            k += 1
    
#-------------------------------------------------------------------------
class kartacombo(QtGui.QComboBox):
    def __init__(self, parent = None, mysql = None):
        QtGui.QComboBox.__init__(self,parent)
        self.mysql = mysql
        self.sql = '''
                SELECT id, name, smena, put
                FROM karta
                WHERE del = 0
                ORDER BY name
            '''
    def redraw(self):
        self.clear()
        (f, self.datarow, self.data) = self.mysql.getData(self.sql)
        s = []
        for id in self.datarow:
            put = str(self.data[id]["put"])
            s += ["%s (%s-%s)"%(self.data[id]["name"],put[0:4],put[4:])]
        self.addItems(s)
    
    def setSelect(self,idkarta):
        k = 0
        for i in self.datarow:
            if i == idkarta:
                self.setCurrentIndex(k)
            k += 1

#-------------------------------------------------------------------------
class editWind(QtGui.QDialog):#вспомогательное диалоговое окно
    def __init__(self, parent = None, mysql = None):
        QtGui.QDialog.__init__(self, parent)
        self.setWindowTitle(u'Добавить карточку')
        self.grid = QtGui.QGridLayout(self)
        self.btn1 = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok|
                                      QtGui.QDialogButtonBox.Cancel)
        self.connect(self.btn1 , QtCore.SIGNAL("accepted()"), self.accept)
        self.connect(self.btn1 , QtCore.SIGNAL("rejected()"), self.reject)
        self.brigada = []
        for i in range(10):
            self.brigada += [str(i+1)]
        
        self.karta = kartacombo(parent,mysql)
        self.dop = dopcombo(parent,mysql)
        self.karta.redraw()
        self.dop.redraw() 
        self.initWidget()

    def initWidget(self):
        lbl1 = QtGui.QLabel(u'Бригада')        
        self.rbtn1 = QtGui.QRadioButton(u'Карточка')
        self.rbtn2 = QtGui.QRadioButton(u'Дополнительно')
        self.rbtn1.setChecked(2)
        self.connect(self.rbtn1, QtCore.SIGNAL("toggled(bool)"), self.chEnabled)

        self.dop.setEnabled(False)
        
        self.cmb1 = QtGui.QComboBox()
        self.cmb1.addItems(self.brigada)
        self.grid.addWidget(lbl1,0,0)
        self.grid.addWidget(self.cmb1,0,1)
        self.grid.addWidget(self.rbtn1,1,0)
        self.grid.addWidget(self.rbtn2,2,0)
        self.grid.addWidget(self.karta,1,1)
        self.grid.addWidget(self.dop,2,1)
        self.grid.addWidget(self.btn1,3,0,1,2)
        
    def chEnabled(self, state=0):
        st = state
        self.dop.setEnabled(not st)
        self.karta.setEnabled(st)
#-------------------------------------------------------------------------
class sprkartatable(controltable):
    def __init__(self, parent = None, mysql = None):
        controltable.__init__(self, parent, mysql)
        self.initSql()        
        self.initConnect()                
        self.edit = editWind(self, mysql)
        self.connect(self.edit, QtCore.SIGNAL("accepted()"),self.save)
        self.dest = 1
        self.selcol = 1
        self.brigada = 1
    
    def initSql(self):
        self.dataview = ["karta", "sort", "put"]
        self.headername = [u"Карточка", u"Сортировка", u"По путевым"]
        self.dataparam = ["karta", "brigada", "sort", "idkarta", "put", "t"]
        self.getDataSQL = '''
            SELECT v.id, k.name as karta, v.sort, k.put, v.brigada, v.idkarta, v.t
            FROM vodgrkartasort as v, karta as k
            WHERE v.brigada = %d AND v.idkarta = k.id AND v.t = 1
            UNION
            SELECT v.id, k.name as karta, v.sort, 0 as put, v.brigada, v.idkarta, v.t
            FROM vodgrkartasort as v, dopoln as k
            WHERE v.brigada = %d AND v.idkarta = k.id AND v.t = 0
            ORDER BY %s            '''

        self.addKartaSQL = '''
            INSERT INTO vodgrkartasort (idkarta, brigada, sort, t)
            VALUES (%d, %d, %d, %d) '''        
        
        self.editKartaSQL = '''
                UPDATE vodgrkartasort SET idkarta = %d, brigada = %d, sort = %d, t = %d
                WHERE id = %d'''
        self.upKartaSQL = ''' UPDATE vodgrkartasort SET sort = sort + 1
                WHERE sort = %d AND brigada = %d'''
        self.downKartaSQL = ''' UPDATE vodgrkartasort SET sort = sort - 1
                WHERE sort = %d AND brigada = %d'''
        self.changeKartaSQL = ''' UPDATE vodgrkartasort SET sort = %d WHERE id = %d'''
                
        self.delKartaSQL = '''DELETE FROM vodgrkartasort WHERE id = %d '''
        self.delDownKartaSQL = ''' UPDATE vodgrkartasort SET sort = sort - 1
                WHERE sort > %d AND brigada = %d'''
        
        self.connect(self.horizontalHeader(), QtCore.SIGNAL("sectionClicked(int)"),self.sortChanged)

    def initConnect(self):
        self.upDataAct = QtGui.QAction(u'Выше',self)
        self.upDataAct.setIcon(QtGui.QIcon("../ico/1uparrow.png"))
        self.downDataAct = QtGui.QAction(u'Ниже',self)
        self.downDataAct.setIcon(QtGui.QIcon("../ico/1downarrow.png"))
        self.connect(self.upDataAct, QtCore.SIGNAL("triggered()"),self.upDataControl)
        self.connect(self.downDataAct, QtCore.SIGNAL("triggered()"),self.downDataControl)
        controltable.initConnect(self)
    
    def upDataControl(self):
        try:
            id = self.datarow[self.currentRow()]
            sort = self.data[id]["sort"]
            if sort>0:
                self.mysql.execsql(self.upKartaSQL%(sort-1,self.brigada))
                self.mysql.execsql(self.changeKartaSQL%(sort-1,id))
                self.redraw()
                k = 0
                for i in self.datarow:
                    if i == id:
                        self.setCurrentCell(k, 0)
                        break
                    k += 1
        except Exception,e:
            print "nothing selected"
            print e
            raise
    
    def downDataControl(self):
        try:        
            id = self.datarow[self.currentRow()]
            sort = self.data[id]["sort"]
            if self.rowCount() > sort+1:
                self.mysql.execsql(self.downKartaSQL%(sort+1,self.brigada))
                self.mysql.execsql(self.changeKartaSQL%(sort+1,id))
                self.redraw()
                k = 0
                for i in self.datarow:
                    if i == id:
                        self.setCurrentCell(k, 0)
                        break
                    k += 1
        except Exception,e:
            print "nothing selected"
            print e

    def redraw(self, data = []):
        if self.dest: s = ' ASC'
        else: s = ' DESC'
        d = "%d %s"%(self.selcol+2,s)
        controltable.redraw(self,[self.brigada, self.brigada, d])
    
    def itemtable(self,row,col,s):
        if col == 2:
            if len(s)>4:
                s = "%s-%s"%(s[:4],s[4:])
        item = QtGui.QTableWidgetItem("%s"%s)
        self.setItem(row,col,item)

    def sortChanged(self,i):
        if i == self.selcol:
            self.dest = 1 - self.dest
        else:
            self.selcol = i
            self.dest = 0
        self.redraw()
    
    def addDataControl(self):
        self.edit.setWindowTitle(u'Добавить данные')
        self.edit.cmb1.setCurrentIndex(self.brigada-1)
        self.edit.karta.redraw()
        self.edit.dop.redraw()
        self.edit.rbtn1.setChecked(2)
        controltable.addDataControl(self)
    
    def editDataControl(self):
        self.edit.setWindowTitle(u'Изменить данные')
        try:
            id = self.datarow[self.currentRow()]
            self.edit.cmb1.setCurrentIndex(self.data[id]['brigada']-1)
            self.edit.karta.redraw()
            self.edit.dop.redraw()
            t = self.data[id]['t']
            if  t:
                self.edit.karta.setSelect(self.data[id]['idkarta'])
                self.edit.rbtn1.setChecked(2)
            else:
                self.edit.dop.setSelect(self.data[id]['idkarta'])
                self.edit.rbtn2.setChecked(2)
            controltable.editDataControl(self)
        except:
            self.addDataControl()


    def _ins(self, t1):
        sort = self.rowCount()
        brigada = self.edit.cmb1.currentIndex()+1
        if t1:#карточка вставка
            idkarta = self.edit.karta.datarow[self.edit.karta.currentIndex()]
            t = 1
        else:#дополнительный параметр вставка
            idkarta = self.edit.dop.datarow[self.edit.dop.currentIndex()]
            t = 0
        data = [idkarta, brigada, sort, t]
        return (data)
    
    def _upd(self, t1, id):
        sort = self.data[self.datarow[self.currentRow()]]["sort"]#получаем место сортировки где находился элемент
        brigada = self.edit.cmb1.currentIndex()+1
        if t1:#карточка вставка
            idkarta = self.edit.karta.datarow[self.edit.karta.currentIndex()]
            t = 1
        else:#дополнительный параметр вставка
            idkarta = self.edit.dop.datarow[self.edit.dop.currentIndex()]
            t = 0
        data = [idkarta, brigada, sort, t, id]
        return (data)

    def save(self):
        t1 = self.edit.rbtn1.isChecked()#получаем новый тип (1 - карточка 0 - дополнительно)
        sql = ''        
        if self.mode == 1:
            data = self._ins(t1)
            sql = self.addKartaSQL%tuple(data)
        else:
            id = self.datarow[self.currentRow()]#получаем ид элемента
            data = self._upd(t1, id)
            sql = self.editKartaSQL%tuple(data)
        if len(sql)>0:
            self.mysql.execsql(sql)
            self.redraw()

    def delDataControl(self):
        id = self.datarow[self.currentRow()]#получаем ид элемента

        sort = self.data[id]["sort"]
        if id > 0:
            self.mysql.execsql(self.delKartaSQL%tuple([id]))
            self.mysql.execsql(self.delDownKartaSQL%(sort,self.brigada))
            self.redraw()
#-------------------------------------------------------------------------
class sprkarta(QtGui.QMdiSubWindow):
    def __init__(self, parent = None, mysql = None):
        QtGui.QMdiSubWindow.__init__(self, parent)
        self.setWindowTitle(u'Список карточек для графика')        
        self.table = sprkartatable(parent, mysql)
        self.table.brigada = 1
        self.table.redraw()
        
        self.brigada = []
        for i in range(10):
            self.brigada += [str(i+1)]

        self.initWidget()
        self.initAction()        
        
            
    def initWidget(self):
        widg = QtGui.QWidget()
        grd = QtGui.QGridLayout()
        vh = QtGui.QVBoxLayout()
        self.brigadacombo = QtGui.QComboBox()        
        self.brigadacombo.addItems(self.brigada)
        tool = QtGui.QToolBar()
        tool.addAction(self.table.addDataAct)
        tool.addAction(self.table.editDataAct)
        tool.addAction(self.table.upDataAct)
        tool.addAction(self.table.downDataAct)
        tool.addWidget(self.brigadacombo)
        tool.addAction(self.table.delDataAct)
        tool.setIconSize(QtCore.QSize(48,48))
        grd.addWidget(tool)
        grd.addWidget(self.table)
        vh.addLayout(grd)
        widg.setLayout(vh)
        self.setWidget(widg)
    
    def initAction(self):
        self.connect(self.brigadacombo, QtCore.SIGNAL("currentIndexChanged(int)"),self.changeBrigada)
        
    def changeBrigada(self, i):
        self.table.brigada = i+1
        self.table.redraw()
