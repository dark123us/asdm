#!/usr/bin/python
# -*- coding: utf-8 -*-
from PyQt4 import QtGui, QtCore
from datetime import date, timedelta
#import pprint
import xlwt
from xlwt import easyxf

class getPribor:
    def __init__(self, mysql):
        self._mysql = mysql
        self.initSql()
    
    def initSql(self):
        self._getDataSql = '''SELECT id, nmbr, idbus FROM pribor ORDER BY nmbr'''
    
    def getData(self):
        f, dr, d = self._mysql.getData(self._getDataSql)
        return f, dr, d 
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class getBus:
    def __init__(self, mysql):
        self._mysql = mysql
        self.initSql()
    
    def initSql(self):
        self._getDataSql = '''SELECT id, garnom, gosnom, marka FROM bus ORDER BY garnom'''
    
    def getData(self):
        f, dr, d = self._mysql.getData(self._getDataSql)
        return f, dr, d 
        
    def getDataId(self):
        data = {}
        f, dr, d = self.getData()
        for idb in d:
            tmp = {}
            tmp["gosnom"] = d[idb]["gosnom"]
            tmp["garnom"] = d[idb]["garnom"]
            tmp["marka"] = d[idb]["marka"]
            data [d[idb]["id"]] = tmp
        return data
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class getError:
    def __init__(self, mysql):
        self._mysql = mysql
        self.initSql()
    
    def initSql(self):
        self._getDataSql = '''SELECT id, lat, lon, nmbr, datetimepribor, 
            datetimeprogramm, sluj, sluj2, koderror, mess  
            FROM error_%s
            WHERE nmbr = %d'''
        
        self._getCountData = '''SELECT id, count(id) as cid, nmbr
            FROM error_%s
            WHERE datetimeprogramm>= '%s' AND datetimeprogramm<= '%s'
            GROUP BY nmbr'''
        
    def getData(self, year, nmbr):
        f, dr, d = self._mysql.getData(self._getDataSql%(str(year), nmbr))
        return f, dr, d 

    def getCountData(self, datebegin, dateend):
        beginmonth = "%04d%02d"%(datebegin.year, datebegin.month)
        data = {}
        try:
            f, dr, d = self._mysql.getData(self._getCountData%(beginmonth, str(datebegin), str(dateend)))
        except:
            dr = {}
            d = []
        for i in dr:
            data[d[i]["nmbr"]] = d[i]["cid"]
        return data
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++        
class getPoint:
    def __init__(self, mysql):
        self._mysql = mysql
        self.initSql()
    
    def initSql(self):
        self._getDataSql = '''SELECT id, lat, lon, nmbr, datetimepribor, 
            datetimeprogramm
            FROM point_%s
            WHERE nmbr = %d'''
        
        self._getCountData = '''SELECT id, count(id) as cid, nmbr
            FROM point_%s
            WHERE datetimeprogramm>= '%s' AND datetimeprogramm<= '%s'
            GROUP BY nmbr'''
        
    def getData(self, year, nmbr):
        f, dr, d = self._mysql.getData(self._getDataSql%(str(year), nmbr))
        return f, dr, d 

    def getCountData(self, datebegin, dateend):
        beginmonth = "%04d%02d"%(datebegin.year, datebegin.month)
        data = {}
        try:
            sql = self._getCountData%(beginmonth, str(datebegin), str(dateend))
            f, dr, d = self._mysql.getData(sql)
        except:
            dr = {}
            d = []
        for i in dr:
            data[d[i]["nmbr"]] = d[i]["cid"]
        return data
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++        
class getCountDataPribor:
    def __init__(self, mysql):
        self._mysql = mysql
        self._pribor = getPribor(mysql)
        self._bus = getBus(mysql)
        self._error = getError(mysql)
        self._point = getPoint(mysql)
        self._data = []
        self._cols = ['nmbr', 'garnom', 'gosnom', 'point' , 'error']
    
    def getData(self, datebegin, dateend):
        data = []
        databus = self._bus.getDataId()
        #берем данные по ошибкам
        dataerror = self._error.getCountData(datebegin, dateend)
        for key in dataerror.keys():
            cid = dataerror[key]
            dataerror[key] = [cid,0]
        #берем данные по координатам
        datapoint = self._point.getCountData(datebegin, dateend)
        for key in datapoint.keys():
            cid = datapoint[key]
            datapoint[key] = [cid,0]
        
        f, dr, d = self._pribor.getData()
        for idp in d:
            tmp = {}
            tmp["nmbr"] = d[idp]["nmbr"]
            idbus = d[idp]["idbus"]
            try:
                tmp["garnom"] = databus[idbus]["garnom"]
            except KeyError:
                tmp["garnom"] = 0
            try:
                tmp["gosnom"] = databus[idbus]["gosnom"]
            except KeyError:
                tmp["gosnom"] = 0
            try:
                tmp["point"] = datapoint[tmp["nmbr"]][0]
                datapoint[key][1] = 1
            except:
                tmp["point"] = 0
            try:
                tmp["error"] = dataerror[tmp["nmbr"]][0]
                dataerror[key][1] = 1
            except:
                tmp["error"] = 0
            data += [tmp]
        self._data = data
        return self._data 

    def qSort(self, data, col, dest):
        cols = self._cols 
        t = True
        while t:
            t = False
            for j in range(len(data)-1):
                ch = False
                if dest == 0:
                    if data[j][cols[col]] > data[j+1][cols[col]]:
                        ch = True
                else:
                    if data[j][cols[col]] < data[j+1][cols[col]]:
                        ch = True
                if ch:
                    tmp = data[j]
                    data[j] = data[j+1]
                    data[j+1] = tmp
                    t = True
        #pp = pprint.PrettyPrinter(indent=4)
        #pp.pprint(data)
        return data
    
    def sortData(self, sort, dest):
        self._data = self.qSort(self._data, sort, dest)
        return self._data
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++        
class printCountDataPribor(getCountDataPribor):
    def __init__(self, mysql):
        getCountDataPribor.__init__(self, mysql)
    
    def printXls(self, filename = 'countdata.xls'):
        wb = xlwt.Workbook()
        ws = wb.add_sheet(u'Данные по приборам')

        #подготавливаем стили, шрифты
        font1 = xlwt.Font()
        font1.name = 'Times New Roman'
        font1.bold = True
        font1.height = 14*20
        #font0.colour_index = 2

        style0 = easyxf(
            'font:name Times New Roman, height 200;'
            'alignment:horizontal left, vertical top;'
            )
        
#        ws.write_merge(nlist*self.nrowlist +smesh, nlist*self.nrowlist +smesh,
#                0, dmonth+ncol, "%s %s %s %s %s %s %s"%(u'Утверждаю: ',22*'_',
#                u' Директор филиала "Автобусный парк №1"',90*' '
#                ,u'Согласовано: ',22*'_',
#                u' Профсоюзный  комитет'), style0)
        #ws.row(nlist*self.nrowlist +smesh).height = 900
        #ws.col(0).width = 1500
        ws.write(0, 0, u'Номер прибора',style0)
        ws.write(0, 1, u'Гаражный номер',style0)
        ws.write(0, 2, u'Гос.номер',style0)
        ws.write(0, 3, u'Пакетов без ошибок',style0)
        ws.write(0, 4, u'Пакетов с ошибками',style0)
        ['nmbr', 'garnom', 'point' , 'error']
        row = 1
        for i in self._data:
            col = 0
            for j in self._cols:
                ws.write(row, col, i[j], style0)
                col += 1
            row += 1

        ws.portrait = False#альбомная печать
        ws.print_scaling = 100#масштаб печати
        ws.top_margin = 0/2.54#поля печати
        ws.bottom_margin = 0/2.54#поля печати
        ws.left_margin = 0#поля печати
        ws.right_margin = 0#поля печати

        ws.header_str=''
        ws.footer_str=''
        #ws.header_margin = False#печать колонтитулов
        #ws.footer_margin = False

        ws.page_preview = True
        wb.save(filename)

        dlg = QtGui.QMessageBox()
        dlg.setWindowTitle(u'Создание отчета')
        dlg.setText(u'Файл отчета создан успешно')
        #dlg.setInformativeText(u'Вы действительно хотите удалить запись?')
        dlg.setStandardButtons(QtGui.QMessageBox.Ok );
        #ok = dlg.exec_()
        dlg.exec_()
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++        
class tableCounDataPribor(QtGui.QTableWidget):
    def __init__(self, mysql, parent = None):
        QtGui.QTableWidget.__init__(self,parent)
        self.setEditTriggers(QtGui.QAbstractItemView.EditTriggers(0))
        self._cdpr = printCountDataPribor(mysql)
        self._headername = [u'Номер прибора', u'Гаражный', u'Гос.номер', u'Верных данных', u'Ошибок данных']
        self._datebegin = date(2011,1,1)
        self._dateend = date(2011,1,1)
        self._datarow = []
        self._sortcol = 0
        self._sortdest = 0
    
    def _initTable(self):
        self.setRowCount(0)
        self.setRowCount(len(self._data))
        self.setColumnCount(len(self._headername))
        self.setHorizontalHeaderLabels(self._headername)
    
    def _resizeTable(self):
        self.resizeColumnsToContents()
        self.resizeRowsToContents ()
        self.setCurrentCell(0,0)
    
    def addRow(self, data, row):
        d = []
        d += [data["nmbr"]]
        d += [data["garnom"]]
        d += [data["gosnom"]]
        d += [data["point"]]
        d += [data["error"]]
        col = 0
        for i in d:
            self.itemDraw(row, col, i)
            col += 1
        
    def redraw(self, redr = True):
        if redr:
            self._cdpr.getData(self._datebegin, self._dateend)
        self._data = self._cdpr.sortData(self._sortcol, self._sortdest)
        self._initTable()
        row = 0
        for i in self._data:
            self.addRow(i, row)
            row += 1 
        self._resizeTable()
            
    def itemDraw(self,row,col,data):
        if type(data) == str or type(data) == unicode:
            item = QtGui.QTableWidgetItem("%s"%data)
            self.setItem(row,col,item)    
        elif type(data) == int or type(data) == long:
            s = str(data)            
            item = QtGui.QTableWidgetItem(s)
            self.setItem(row,col,item)

    def setDate(self, datebegin, dateend):
        self._datebegin = datebegin
        self._dateend = dateend
        self.redraw()
    
    def setDateBegin(self, datebegin):
        datebegin = datebegin.toPyDate()
        self._datebegin = datebegin
        self.redraw()
    
    def setDateEnd(self, dateend):
        dateend = dateend.toPyDate()
        self._dateend = dateend
        self.redraw()
        
    def setSort(self, idsort):
        if idsort == self._sortcol:
            self._sortdest = 1 - self._sortdest
        else:
            self._sortcol = idsort
            self._sortdest = 0
        self.redraw(False)
    
    def printXls(self):
        self._cdpr.printXls()
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++    
class wCountDataPribor(QtGui.QWidget):
    def __init__(self, mysql, parent= None):
        QtGui.QWidget.__init__(self, parent)
        self._table = tableCounDataPribor(mysql,parent)
        self.initWidget()
        self.initConnect()        
        
    def initWidget(self):
        grd = QtGui.QGridLayout()
        vh = QtGui.QVBoxLayout()
        vh2 = QtGui.QHBoxLayout()
        grd1 = QtGui.QGridLayout()
        lb1 = QtGui.QLabel(u'Начальная дата')
        lb2 = QtGui.QLabel(u'Конечная дата')
        self._btn1 = QtGui.QPushButton(u'Обновить')
        self._btn2 = QtGui.QPushButton(u'Отчет')
        self._dt1 = QtGui.QDateEdit(QtCore.QDate(date.today()))
        self._dt1.setCalendarPopup(True)
        self._dt2 = QtGui.QDateEdit(QtCore.QDate(date.today() + timedelta(1,0)))
        self._dt2.setCalendarPopup(True)
        self._table.setDate(self._dt1.date().toPyDate(), self._dt2.date().toPyDate())
        grd1.addWidget(lb1, 0, 0)        
        grd1.addWidget(lb2, 0, 1)        
        grd1.addWidget(self._btn1, 0, 2)        
        grd1.addWidget(self._dt1, 1, 0)        
        grd1.addWidget(self._dt2, 1, 1)        
        grd1.addWidget(self._btn2, 1, 2)                
        vh2.addLayout(grd1)
        vh2.addStretch()
        
        grd.addLayout(vh2,0,0)
        grd.addWidget(self._table)
        vh.addLayout(grd)
        self.setLayout(vh)
    
    def initConnect(self):
        self.connect(self._dt1, QtCore.SIGNAL("dateChanged(QDate)"),self._table.setDateBegin)
        self.connect(self._dt2, QtCore.SIGNAL("dateChanged(QDate)"),self._table.setDateEnd)
        self.connect(self._btn1, QtCore.SIGNAL("clicked()"),self._table.redraw)
        self.connect(self._btn2, QtCore.SIGNAL("clicked()"),self._table.printXls)
        self.connect(self._table.horizontalHeader(), QtCore.SIGNAL("sectionClicked(int)"),self._table.setSort)
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class sprRemont(QtGui.QMdiSubWindow):
    def __init__(self, mysql, parent = None):
        QtGui.QMdiSubWindow.__init__(self, parent)
        self.setWindowTitle(u'Список данных от приборов')        
        self._widg = wCountDataPribor(mysql, parent)
        self.setWidget(self._widg)
