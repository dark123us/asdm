#!/usr/bin/python
# -*- coding: utf-8  -*-
import re
import socket, string, datetime
import time



# 16-bit CRCs should detect 65535/65536 or 99.998% of all errors in
# data blocks up to 4096 bytes
MASK_CCITT = 0x1021 # CRC-CCITT mask (ISO 3309, used in X25, HDLC)
MASK_CRC16 = 0xA001 # CRC16 mask (used in ARC files)

class decodedata():
    def __init__(self,type=0, st = ''):
        self.st = self.ords(st)
        self.type = type
        self.minlat = 51.14
        self.maxlat = 56.4
        self.minlon = 23.0
        self.maxlon = 33.0
        self.maxspeed = 150
        self.alt = 300
        self.timepojas = 60*60*3#сдвиг на 3 часа


    def ords(self,x):#из байт стпроки в 16-ричную систему
        s1 = ''
        for s in x:
            s1 = "%s%02x"%(s1,ord(s))
        return s1

    def sord(self,ord):#из 16-ричной системы в байт строку
        s1 = ''
        for i in range(0,len(ord),2):
            s1 = "%s%s"%(s1,chr(self.hextoint(ord[i:i+2])))
        return s1

    def delslash(self,s1):
        #удаляем экранирующие символы если есть 1712, то оставляем только 17
        s = ''
        fl = 0
        for i in range(0,len(s1),2):
            if fl == 1:
                fl = 0
                if s1[i:i+2] != '12':
                    s = "%s%s"%(s,s1[i:i+2])
            else:
                s = "%s%s"%(s,s1[i:i+2])
            if s1[i:i+2] == '17':
                fl = 1
        return s


    def updcrc(self, data, crc=0xffff, mask=MASK_CCITT):
        for char in data:
            c = ord(char)
            c = c << 8

            for j in xrange(8):
                if (crc ^ c) & 0x8000:
                    crc = (crc << 1) ^ mask
                else:
                    crc = crc << 1
                c = c << 1
        return crc & 0xffff

    def hextoint(self,st):
        j = 1
        sum = 0
        for i in range(len(st)-1,-1,-1):
            st1 = st[i]
            if st1 == 'a': st1 = 10
            if st1 == 'b': st1 = 11
            if st1 == 'c': st1 = 12
            if st1 == 'd': st1 = 13
            if st1 == 'e': st1 = 14
            if st1 == 'f': st1 = 15
            st1 = int(st1)
            sum += st1 * j
            j *= 16
        return sum

    def inttohex(self,st):
        return "%x"%st

    def hextofloat(self,st):
        j = 1 / 16.0
        sum = 0
        for i in range(0,len(st)):
            st1 = st[i]
            if st1 == 'a': st1 = 10
            if st1 == 'b': st1 = 11
            if st1 == 'c': st1 = 12
            if st1 == 'd': st1 = 13
            if st1 == 'e': st1 = 14
            if st1 == 'f': st1 = 15
            st1 = int(st1)
            sum += st1 * j
            j /= 16.0
        return sum

    def decode(self,st='',typest = 0):#typest - 0-байт строка, 1 - в 16-ричном видении
        #минимально-максимальные координаты

        if typest == 0:
            if len(st)==0:
                self.st = '1713080cea08100b0c291901e3540000fbcc1e04d80f6e1f000035309800720666401705'
            else:
                self.st = self.ords(st)
        if typest:
            self.st = st

        o = re.finditer('(1713)(.*?)(1705)',self.st)
        k = 0
        ret = []
        dh = self.hextoint
        dt = datetime.datetime.now()
        for i in o:
            error = []
            ss = i.groups()[1]
            ss = self.delslash(ss)
            if self.type ==0:
                sluj = self.hextoint(ss[0:2])
                nprib = self.hextoint(ss[2:6])
                try:
                    dt = datetime.datetime(2000+dh(ss[10:12]),dh(ss[6:8]),dh(ss[8:10]),dh(ss[12:14]),dh(ss[14:16]),dh(ss[16:18])  )
                    dt += datetime.timedelta(0,self.timepojas)#смещение времени по текущему часовому поясу
                except Exception, e:
                    error += [[1,'Datetime - error']]
                    print "decodedata.decodedata.decode ",e
                lat = self.hextoint(ss[18:28])/153600000.0
                lon = self.hextoint(ss[28:34])/600000.0
                if lat > self.maxlat or lat < self.minlat or lon > self.maxlon or lon < self.minlon:
                    error += [[2,'lat or lon - error']]
                speed = self.hextoint(ss[34:38])/100.0
                if speed > self.maxspeed:
                    error += [[3,'speed - error']]
                angle = self.hextoint(ss[38:42])/100.0
                if angle > 360:
                    error += [[4,'angle - error']]
                nepotnyatno = ss[42:]
                #if len(nepotnyatno)>0:
                #    error += [[7,'musor detected']]

                sluj2 = 0
                alt = 0#self.hextoint(ss[42:44])
                crc16 = 0#self.hextoint(ss[-4:])
                crc16prov = 0
            if self.type ==1:
                sluj = self.hextoint(ss[0:2])
                sluj2 = self.hextoint(ss[2:4])
                nprib = self.hextoint(ss[4:8])
                try:
                    dt = datetime.datetime(2000+dh(ss[12:14]),dh(ss[8:10]),dh(ss[10:12]),dh(ss[14:16]),dh(ss[16:18]),dh(ss[18:20]))
                    dt += datetime.timedelta(0,self.timepojas)#смещение времени по текущему часовому поясу
                except Exception, e:
                    error += [[1,'Datetime - error']]
                    print "decodedata.decodedata.decode ",e
                lat = self.hextoint(ss[20:30])/153600000.0
                lon = self.hextoint(ss[30:36])/600000.0
                if lat > self.maxlat or lat < self.minlat or lon > self.maxlon or lon < self.minlon:
                    error += [[2,'lat or lon - error']]
                speed = self.hextoint(ss[36:40])/100.0
                if speed > self.maxspeed:
                    error += [[3,'speed - error']]
                angle = self.hextoint(ss[40:44])/100.0
                if angle > 360:
                    error += [[4,'angle - error']]
                alt = self.hextoint(ss[44:48])
                if alt > self.alt:
                    error += [[5,'alt - error']]
                crc16 = self.hextoint(ss[-4:])
                crc16prov = int(self.updcrc(self.sord(ss[0:-4])))
                if int(crc16) - int(crc16prov) != 0:
                    error += [[6,'crc16 - error']]
                nepotnyatno = ss[48:-4]
                #if len(nepotnyatno)>0:
                #    error += [[7,'musor detected']]

            
            d = {}
            d ['datetime'] = dt
            d ['sluj'] = sluj
            d ['sluj2'] = sluj2
            d ['nprib'] = nprib
            d ['lat'] = lat
            d ['lon'] = lon
            d ['angle'] = angle
            d ['speed'] = speed
            d ['alt'] = alt
            d ['crc16'] = crc16
            d ['crc16prov'] = crc16prov
            d ['nepotnyatno'] = nepotnyatno
            k += 1
            if len(error)>0:
                ret +=[[d,[error,ss]]]
            else:
                ret +=[[d]]
        if k>1:
            print "---------------====================--------------------",k
        return ret

if __name__ == "__main__":
    a = decodedata(1)
    d = '17130b60189f08500b0c320e01e36ffb00fc75970e6267fb0097095a0100000000120200171202170517130b60189f08100b0c321901e3705f00fc714910b56e420099095a010000000000db21170517130b60189f08100b0c322301e370aa00fc6cea128f685e0099095a0100000000001dde170517130b60189f08100b0c322d01e3700900fc684813ba61cc0098095a0100000000004029170517130b60189f08100b0c323401e36f0400fc6550138d5bcf0096095a010000000000bbf5170517130b60189f08100b0c330701e36ccf00fc61be040a557b0095095a010000000000ddf5170517130b60189f08100b0c330d01e36cbd00fc61b300285b630095095a0100000000009d71170517130b60189f08100b0c330e01e36cbd00fc61b3000d5b630095095a0100000000005fca170517130b60189f08100b0c331501e36c7c00fc6185064d51730095095a0100000000004905170517130b60189f08100b0c340d01e3612d00fc54ec13b4579a0092095a0100000000009b0c170517130b60189f08100b0c341901e35efb00fc4ff613a15e4d008a095a0100000000005b4f170517130b60189f08100b0c351901e356f000fc359c0dde5eb7008f0983010000000000fd5e170517130b60189f08100b0c351e01e356a800fc34a4068968ad0092088301000000000044d2170517130b60189f08100b0c351f01e356a800fc348804af6f430092088301000000000068cf170517130b60189f08100b0c352201e3569700fc346001ed634e00930983010000000000383a170517130b60189f08100b0c352701e3568600fc344f000b67bb009408830100000000008330170517130b60189f08100b0c360901e3565a00fc33f903f05b4500970883010000000000f03a170517130b60189f08100b0c360f01e3560200fc332907f861f7009809830100000000005433170517130b60189f08100b0c362c01e353d000fc2c0901ba5b4b00940983010000000000dfb1170517130b60189f08100b0c363101e353d400fc2c0500215b6700940983010000000000a72b170517130b60189f08100b0c371d01e353b500fc2b4f04774af200a3095a0100000000004b34170517130b60189f08100b0c372001e3537200fc2b5e057141be00a1095a010000000000f001170517130b60189f08100b0c372301e3531e00fc2bbb083837d500a1095a01000000000032a11705'
    #d = '1713030013c108100b0c260501e3847600fc5e59000e7cb9009c006117051713030013c108100b0c270501e3848b00fc5e6300037cb9009e1a0617051713030013c108100b0d1b3801e387df00fc5e57062413c0009a8be31705'
    #d = '17130700124b08100b0d032fffffffffffffffffffffffffffff5e0d1705'
    #d = '171300095208100b0c242601e333a600fc356904214a881f000035547101021550101705171308095208100b0c242701e3339500fc355a052150d21f000035547101021550101705171300095208100b0c242901e3337000fc3521070258161f000035547101021550101705'
    #d = '1713000cea08100b0c243601e3541d00fbf8db0ac75fb41f0000353098007206664017051713000cea08100b0c250901e3530d00fbf4e20a20663a1f0000353098007206664017051713000cea08100b0c250b01e3531300fbf45b0aff6cb61f0000353098007206664017051713000cea08100b0c250f01e3531c00fbf33a0b9366b21f000035309800720666401705'
    dd = '0b60189f08100b0c320e01e36ffb00fc75970e6267fb0097095a0100000000120200'
    
    #dd = '17130b60189f08100b0c321901e3705f00fc714910b56e420099095a010000000000db211705'
    #dd = '0b60189f08100b0c321901e3705f00fc714910b56e420099095a010000000000'
    dd = '000cea08100b0c243601e3541d00fbf8db'
    dd = a.sord(dd)
    #print dd
    #print a.ords(dd)
    f = a.updcrc(dd)
    print "%x"%f
    #print a.sord(d)
    #d = '1713000cea08100b0c243601e3541d00fbf8db0ac75fb41f000035309800720666401705'
    f = a.decode(d, 1)
    x = 0
    for i in f:
        x = 0
        print len(i)
        for j in i:
            if x == 0:
                #for j in i:
                print  "sluj = %x,%x"%(j['sluj'],j['sluj2'])
                print  "n prib = %d"%j ['nprib']
                print  "date = ",j['datetime']
                print  "lat = %f lon = %f"%(j['lat'],j['lon'])
                print  "speed = %.2f angle = %.2f alt = %d"%(j['speed'],j['angle'], j['alt'])
                print  "crc16 = %x prov = %x"%(j['crc16'],j ['crc16prov'])
                print  "ostatok = %s"%j['nepotnyatno']
            else:
                print j
            x = 1-x
        print

    