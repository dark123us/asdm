#!/usr/bin/python
# -*- coding: utf-8  -*-
import re
import socket, string, datetime
import time


# 16-bit CRCs should detect 65535/65536 or 99.998% of all errors in
# data blocks up to 4096 bytes
MASK_CCITT = 0x1021 # CRC-CCITT mask (ISO 3309, used in X25, HDLC)
MASK_CRC16 = 0xA001 # CRC16 mask (used in ARC files)

#----------------------------------------------------------------------------
# Calculate and return an incremental CRC value based on the current value
# and the data bytes passed in as a string.
#
def updcrc(crc, data, mask=MASK_CCITT):

# data_length = len(data)
# unpackFormat = '%db' % data_length
# unpackedData = struct.unpack(unpackFormat, data)

    for char in data:
        c = ord(char)
        c = c << 8

        for j in xrange(8):
            if (crc ^ c) & 0x8000:
                crc = (crc << 1) ^ mask
            else:
                crc = crc << 1
            c = c << 1

    return crc & 0xffff

def hextoint(st):
    j = 1
    sum = 0
    for i in range(len(st)-1,-1,-1):
        st1 = st[i]
        if st1 == 'a': st1 = 10
        if st1 == 'b': st1 = 11
        if st1 == 'c': st1 = 12
        if st1 == 'd': st1 = 13
        if st1 == 'e': st1 = 14
        if st1 == 'f': st1 = 15
        st1 = int(st1)
        sum += st1 * j
        j *= 16
    return sum

def hextofloat(st):
    j = 1 / 16.0
    sum = 0
    for i in range(0,len(st)):
        st1 = st[i]
        if st1 == 'a': st1 = 10
        if st1 == 'b': st1 = 11
        if st1 == 'c': st1 = 12
        if st1 == 'd': st1 = 13
        if st1 == 'e': st1 = 14
        if st1 == 'f': st1 = 15
        st1 = int(st1)
        sum += st1 * j
        j /= 16.0
    return sum

def readlog():
    path = '/home/dark123us/project/asdm/src/shkiper/'
    fil = file(path+'log.sct','r')
    for st in fil:
        restr = "[[(']{3}([0-9\.]+)[',\ ]{3}([0-9]+)[)\][]{3}([0-9-]+) ([0-9:]+)[0-9\.]+]([0-9a-f]+)"
        result = re.finditer(restr, st )
        for match in result:
            #print match.groups()
            s = "[%s:%s][%s %s]"%(match.groups()[0],match.groups()[1],match.groups()[2],match.groups()[3])
            #print s
            rr = '''(1713)(.*?)(1705)'''
            res2 = re.finditer(rr,match.groups()[4])
            #print match.groups()[4]
            for i in res2:
                #print i.groups()
                #if i.groups()[0] == '1713':
                    #print '    Начало 1713 = %d(%d,%d)'%(hextoint('1713'),hextoint('17'),hextoint('13'))
                st = i.groups()[1]
                if hextoint(st[4:8])==4694:
                #print "        Тип (%s) = %d (%d %d)"%(st[0:4],hextoint(st[0:4]),hextoint(st[0:2]),hextoint(st[2:4]))
                    print "        № прибора (%s) = %d"%(st[4:8],hextoint(st[4:8]))
                #print "        дата (%s) = %02d-%02d-20%02d"%(st[8:14],hextoint(st[10:12]),hextoint(st[8:10]),hextoint(st[12:14]))
                    print "        время (%s) = %02d:%02d:%02d"%(st[14:20],hextoint(st[14:16]),hextoint(st[16:18]),hextoint(st[18:20]))

                    s1 = ''
                    for j in range(20,48,2):
                        s1 = "%s %d"%(s1,hextoint(st[j:j+2]))
                    print "        1 координата (%s) %s"%(st[20:48],s1)
                #print "        1 координата (%s) = %02d.%02e"%(st[20:34],hextoint(st[20:22]),hextofloat(st[22:34]))


                #if i.groups()[2] == '1705':
                    #print '    Конец 1705 = %d(%d,%d)'%(hextoint('1705'),hextoint('17'),hextoint('05'))
def sendpacket(host1,port1,data1):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((host1,port1))
    sock.send(data1)
    #time.sleep(20)
    sock.close()

HOST = "10.1.12.2"                 # localhost
PORT = 21003

'0300 13cb 04050b 083419 01e3858100 fc5bbb00aa 39f9 0096 8e76'

begin = '1713'
end = '1705'
sluj = '0300'
npribor = '13be' #5054
date = '04020b'# 1 апреля 2011
time1 = '040002'#10:10:10
koord1 = '01e3858100'#5 байтпредположим, что это 1-ая координата
koord2 = 'fc0000'#5 байт, т.к необходима была экранизация
speed = '10ff'
azim = '3458' #угол от 0 до 360 необходимо деление на 100
alt = '0205'#высота над уровнем
data = sluj + npribor + date + time1 + koord1 + koord2 + speed + azim + alt
strdata = ''
for i in range(0,len(data),2):
    strdata = "%s%s"%(strdata,chr(hextoint(data[i:i+2])))
crc16ccitt = updcrc(0xffff,strdata,0x1021)
data = "%s%s%x%s"%(begin,data,crc16ccitt,end)

print data
strdata = ''
for i in range(0,len(data),2):
    strdata = "%s%s"%(strdata,chr(hextoint(data[i:i+2])))
print strdata
print "%f "%(hextoint('10ff')/100.0)
print "%10.10e"%(hextoint('01e3858100')/153600000.0000)
print "%f"%(hextoint('fc0000')/600000.0)

print "%f"%(hextoint('ff')/100.0)

#sendpacket(HOST,PORT,strdata)

"""
txt = '''
{number section}
num=1
{text section}
txt="2"
'''
result = re.finditer( ur"({[^}\n]+})|(?:([^=\n]+)=([^\n]+))", txt )
for match in result :
  print match.groups()
print 123
object = re.compile( ur"(?P<section>{[^}\n]+})|(?:(?P<name>[^=\n]+)=(?P<value>[^\n]+))", re.M | re.S | re.U )
result = object.finditer( txt )
group_name_by_index = dict( [ (v, k) for k, v in object.groupindex.items() ] )
print group_name_by_index
for match in result :
  for group_index, group in enumerate( match.groups() ) :
    if group :
      print "text: %s" % group
      print "group: %s" % group_name_by_index[ group_index + 1 ]
      #print "position: %d, %d" % match.span( group_index + 1 )
"""
