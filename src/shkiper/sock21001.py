#!/usr/bin/python
import SocketServer
import socket
import datetime
import time
import threading

def ords(x):
    s1 = ''
    for s in x:
        s1 = "%s%02x"%(s1,ord(s))
    return s1

alln = 0
HOST = ['10.1.12.2','10.1.12.5','bysel.by']
PORT = 21001
class mythread(threading.Thread):
    def __init__(self, channel, details,nthread):
        global alln 
        alln += 1
        self.channel = channel
        self.details = details
        self.nthread = nthread
        self.conn = []
        for i in HOST:
            self.conn += [0]
        print "\nBegin thread [%s %d][%s]"%(datetime.datetime.now(),self.nthread,self.details)
        j = 0
        for i in HOST:
            try:
                self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                self.sock.connect((i,PORT))
                self.conn[j] = 1
                j += 1
            except:
                print "\nNot connected %s:%d\n"%(i,PORT)
        threading.Thread.__init__(self)

    def run(self):
        global alln 
        rr = True
        self.channel.settimeout(120)
        while rr:
            try:
                data = self.channel.recv(1024)
                if not data: 
                    rr = False
                    alln -= 1
                    print "\nEnd thread %d(%d)\n"%(self.nthread,alln)
                    self.channel.close()
                else:
                    print "[%s %d(%d)][%s]%s"%(datetime.datetime.now(),self.nthread,alln,self.details,ords(data))
                    j = 0
                    for i in HOST:
                        try:
                            if self.conn[j]:
                                self.sock.send(data)
                            else:
                                try:
                                    self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                                    self.sock.connect((i,PORT))
                                    self.conn[j] = 1
                                except:
                                    print "Not connected again %s:%d"%(i,PORT)
                                    self.conn[j] = 0
                        except:
                            self.conn[j] = 0
                            print "\nDisconnect to %s:%d\n"%(i,PORT)
                        j+=1
                    
            except:
                rr = False
                alln -= 1
                print "\nEnd thread (timeout) %d(%d)\n"%(self.nthread,alln)
                self.channel.close()

#server host is a tuple ('host', port)
print "start"
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
k = 0
n = 0
rr = 1
while rr:
    try:
        server.bind(('10.1.12.5', 21001))
        rr = False
        print "Listen"
    except:
        n += 1
        print "Socket denied %d"%n
        time.sleep(1)

server.listen(100)

while 1:
    try:
        channel, details = server.accept()
        mythread(channel, details, k).start()
        k += 1
    except:
        n += 1
