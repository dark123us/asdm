#!/usr/bin/python
# -*- coding: utf-8  -*-
def crc16(Str, Params ):
    '''
      Name            : CRC-16 CCITT
      Poly (default)  : 0x1021    x^16 + x^12 + x^5 + 1
      Init (default)  : 0xFFFF
      XorOut (default): 0x0000
      Revert          : false
      Check           : 0x29B1 ("123456789")
      MaxLen          : 4095 байт (32767 бит) - обнаружение
        одинарных, двойных, тройных и всех нечетных ошибок
    '''

    # устанавливаем значения по умолчанию у незаданных параметров
    Defaults = {
      "polynome": 0x1021,
      "init" : 0xFFFF,
      "xor_out" : 0,}
    print Defaults

 '''
   foreach ($aDefaults as $key => $val){
      if (!isset($aParams[$key])){
         $aParams[$key] = $val;
      }
   }

   //-- инициализируем переменные
   $sStr .= "";
   $crc = $aParams['init'];
   $len = strlen($sStr);
   $i = 0;

   //-- считаем
   while ($len--){
      $crc ^= ord($sStr[$i++]) << 8;
      $crc &= 0xffff;

      for ($j = 0; $j < 8; $j++){
         $crc = ($crc & 0x8000) ? ($crc << 1) ^ $aParams['polynome'] : $crc << 1;
         $crc &= 0xffff;
      }
   }

   $crc ^= $aParams['xor_out'];

   return $crc;
}
'''
