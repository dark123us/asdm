# -*- coding: utf-8 -*-
def formatstr(week):#data соответстиве self.dataview и данными
        b = 0 #флаг для объединения дней
        c = 0
        rab = '' #строка для рабочих дней
        vih = '' #строка для выходных дней
        st = ''
        if week[0] == 2:
            b = 1
            rab = u'Пн'
        if week[1] == 2:
            if b == 1:
                rab += u', вт'
            else:
                rab = u'Вт'
                b = 1
        if week[2] == 2:
            if b == 1:
                rab += u', ср'
            else:
                rab = u'Ср'
                b = 1
        if week[3] == 2:
            if b == 1:
                rab += u', чт'
            else:
                rab = u'Чт'
                b = 1
        if week[4] == 2:
            if b == 1:
                rab += u', пт'
            else:
                rab = u'Пт'
                b = 1
        if week[5] == 2:
            if b == 1:
                vih += u', сб'
            else:
                vih = u'Сб'
                c = 1
        if week[6] == 2:
            if b == 1:
                vih += u', вс'
            else:
                vih = u'Вс'
                c = 1
        if week[7] == 2:
            b = 1
            st = u'По рабочим'
            rab = ''
        if week[8] == 2:
            if b == 1:
                st += u', выходным'
                vih = ''
            else:
                st = u'По выходным'
                vih = ''
                b = 1
        st = rab + st + vih
        if week[9] == 2:
            if b == 1:
                st += u', праздничным'
            else:
                st = u'По праздничным'
                b = 1
        if week[10] == 2:
            if b == 1:
                st += u', предпраздничным'
            else:
                st = u'По предпраздничным'
        if week[12] == 2:
            if b == 1:
                st += u', кроме праздничных'
            else:
                st = u'Кроме праздничных'
        #если все дни, то все дни, что ж тут сделаешь
        if week[11] == 2:
            st = u'Все дни'
        #print st
        return st